import robot_foot_model as robot_foot_model
import FootPlot as FootPlot
import QLearning as QL
import gait as gait
import UI.excuResultUI_real as erUI
import UI.dynamicdataUI as dyUI
import UI.gaitplotUI as gaUI
from onefoot_pbrl_gait import TrainingConfiguration
from learning_communication.data_package import TrainigData
import socket, os, sys, pickle
import threading
import multiprocessing as mp
from multiprocessing import Queue
from PyQt5 import QtWidgets, QtCore, QtGui
import time
from collections import namedtuple
import numpy as np
import datetime
import traceback
import LearningUtility as LUtil

from gait import Obstacle

class App(object):

    def __init__(self):
        if socket.gethostname() == 'ERIC-PERSONAL':
            os.environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
            app = QtWidgets.QApplication(sys.argv)
            app.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling)
        else:
            app = QtWidgets.QApplication(sys.argv)

        self.GUI = ExecutorUI()
        self.dynaGUI = DynamicLogUI()
        self.gaitGUI = GaitUI()

        model_list = list()
        for name in os.listdir("./model"):
            if name.endswith('.model'): model_list.append(name[:-6])
        self.GUI.model_select_box.addItems(model_list)

        ### Loading trace
        self.main_trace_folder = "./excuResult/trainingTrace"
        trace_list = list()
        for name in os.listdir(self.main_trace_folder):
            if os.path.isdir("{0}/{1}".format(self.main_trace_folder, name)):
                trace_list.append(name)
        self.GUI.trace_SelectBox.addItems(trace_list)
        self.p_trace_timer = QtCore.QTimer()
        self.p_trace_timer.timeout.connect(self.plotProcessTrace)
        self.GUI.progress_SLBar.sliderPressed.connect(self.p_trace_timer.stop)
        self.loadTrainingTrace()
        self.selectTrace()

        # App Status
        self.bool_real_foot_plot            = False
        self.bool_foot_too_far_from_target  = False
        self.bool_activate_real_motor       = False
        self.bool_with_offset               = False
        self.bool_real_time_mode            = False
        self.bool_possiable_for_realtime    = False
        self.bool_config_loaded             = False

        #App attripute
        self.switch_half_mode = False
        self.switch_trt_mode = False
        self.switch_real_foot = False
        self.switch_emergency_stop = False

        #Matplot Lib Setting
        self.PYQTGRAPH_ON = True
        self.NEEDOBSTACLE = False
        self.dynamic_log_on = False
        self.counter = 0

        self.real_foot  = robot_foot_model.Foot14_14_7_Real()
        self.dummy_foot   = robot_foot_model.Foot14_14_7_Real()
        self.foot_widget = FootPlot.FootWidget(self.GUI.MPLib, PYQTGRAPH_ON=self.PYQTGRAPH_ON)
        self._dynamic_ax, self.plot_widgets = self.foot_widget.returnPlotWidget()
        self.foot_diagram = FootPlot.Foot(
            self.GUI.MPLib,
            dynamic_ax=self._dynamic_ax,
            plot_widgets=self.plot_widgets,
            need_trace=True,
            need_ground=True,
            need_obstacle=True,
            need_target=True,
            PYQTGRAPH_ON=self.PYQTGRAPH_ON
        )
        self.dyna_diagram = FootPlot.Dynamic(0.1, foot_model=self.dummy_foot ,dynaGUI=self.dynaGUI, PYQTGRAPH_ON=self.PYQTGRAPH_ON)
        self.gait_diagram = FootPlot.Gait(self.gaitGUI.gait_plot_Lay)

        self.data = TrainigData()
        #Function Maping
        self.GUI.load_model_Btn.clicked.connect(self.updateLearningConfiguration)
        self.GUI.QuitBTN.clicked.connect(self.ExitApp)
        self.GUI.auto_run_Btn.clicked.connect(self.modelAutoRun)
        self.GUI.load_q_Btn.clicked.connect(self.loadQNetwork)
        self.GUI.DynamicLogBtn.clicked.connect(self.showDynamicWindow)
        self.GUI.save_2_mat_btn.clicked.connect(lambda:self.saveData("mat"))
        self.GUI.btn_save_to_xlsx.clicked.connect(lambda:self.saveData("xlsx"))
        self.GUI.clear_data_Btn.clicked.connect(self.clearData)
        self.GUI.trace_SelectBox.currentIndexChanged.connect(self.loadTrainingTrace)
        self.GUI.progress_SLBar.sliderReleased.connect(self.selectTrace)
        self.GUI.start_traceBtn.clicked.connect(self.startPlottingPTrace)
        self.GUI.activateRM_Btn.clicked.connect(self.activateRealMotor)
        self.GUI.with_offset_rb.clicked.connect(self.switchOffset)
        self.GUI.real_time_Rb.toggled.connect(self.realTimeMode)

        self.GUI.device_cpu_rBtn.clicked.connect(self.initiateAgent)
        self.GUI.device_gpu_rBtn.clicked.connect(self.initiateAgent)
        self.GUI.float_normal_rBtn.clicked.connect(self.initiateAgent)
        self.GUI.float_half_rBtn.clicked.connect(self.initiateAgent)
        self.GUI.trt_activate_rBtn.clicked.connect(self.initiateAgent)
        self.GUI.trt_inactivate_rBtn.clicked.connect(self.initiateAgent)

        self.gait_plot_show_ground = False
        self.global_gait_graph_density_factor = 3
        self.GUI.gait_plot_Btn.clicked.connect(lambda: self.gaitGUI.window.show())
        self.gaitGUI.start_gait_Btn.clicked.connect(self.plotGlobalGaitGrapth)
        self.gaitGUI.btn_show_ground.clicked.connect(self.plotGlobalGaitGrapthShowGround)
        self.gaitGUI.PlotTraceBtn.clicked.connect(self.gait_diagram.plotTrace)
        self.gaitGUI.DensityInput.valueChanged.connect(self.plotGlobalGaitGrapthDensityFactor)

        self.dynaGUI.tabWidget.currentChanged.connect(self.updateDynamicData)

        self.modelTimer = QtCore.QTimer()
        self.modelTimer.timeout.connect(self.modelRunner)

        #Posible thread
        self.gait_thread = None

        #QLearning StandBy
        self.initiateAgent()

        #GUI Start
        self.GUI.excu_window.show()
        sys.exit(app.exec_())

    def activateRealMotor(self):
        if self.bool_config_loaded:
            if robot_foot_model.HAS_DXL_SDK and not self.real_foot.real_motor_mode:
                self.real_foot.activateRealMotor(self.GUI.device_name_lned.text())
                if self.real_foot.real_motor_mode:
                    self.real_foot.disarmAllMotor()
                    for motor in self.real_foot.motors:
                        motor.real_motor.switchMode('position')
                    if not self.PYQTGRAPH_ON:self.foot_diagram.UpdatePlot(self.real_foot, True)
                    else:self.foot_diagram.plotFoot(foot_model=self.real_foot, expect_foot=None)
                    self.activateRealTable()
                    self.GUI.activateRM_Btn.setStyleSheet("background-color: rgb(50, 225, 50);")
                    self.bool_real_time_mode = True
                    self.realTimeMode()
                    self.GUI.real_time_Rb.setChecked(True)
                else:
                    self.real_foot.disableAllMotor()
                    self.GUI.statusbar.showMessage("Device Error", 2000)
                    self.GUI.activateRM_Btn.setStyleSheet("background-color: rgb(221, 221, 221);")
            elif not robot_foot_model.HAS_DXL_SDK:
                self.GUI.statusbar.showMessage("SDK Not Installed!!!", 2000)
                self.GUI.activateRM_Btn.setStyleSheet("background-color: rgb(221, 221, 221);")
                print("SDK Not Installed!!!")
            
            elif self.real_foot.real_motor_mode:
                self.real_foot.disarmAllMotor()
                self.real_foot.disableAllMotor()
                if not self.real_foot.real_motor_mode:
                    self.GUI.activateRM_Btn.setStyleSheet("background-color: rgb(221, 221, 221);")
        else:
            print("Load a config file first before activate")

    def activateRealTable(self):
        self.real_motor_talbe_itemlist = list()
        for index, motor in enumerate(self.real_foot.motors):
            item_enable = QtWidgets.QTableWidgetItem(
                "On" if motor.real_motor.TORQUE_ENABLE_value == 1 else "Off"
            )
            item_position   = QtWidgets.QTableWidgetItem(str(round(motor.GetPosition(Unit='deg'),2)))
            item_velocity   = QtWidgets.QTableWidgetItem(str(round(motor.GetVelocity(Unit='rad'),2)))
            item_current    = QtWidgets.QTableWidgetItem(str(round(motor.GetCurrent(),2)))
            item_temperture = QtWidgets.QTableWidgetItem(str(motor.GetTemperture()))
            self.GUI.real_motor_table.setItem(0, index, item_enable)
            self.GUI.real_motor_table.setItem(1, index, item_position)
            self.GUI.real_motor_table.setItem(2, index, item_velocity)
            self.GUI.real_motor_table.setItem(3, index, item_current)
            self.GUI.real_motor_table.setItem(4, index, item_temperture)
            self.real_motor_talbe_itemlist.append([item_enable, item_position, item_velocity, item_current, item_temperture])

    def armRealMotor(self):
        if self.real_foot.real_motor_mode:
            if not self.real_foot.joint_motor_armed:
                self.real_foot.updateRealMotorsInfo()
                self.real_foot.armAllMotor()
                self.GUI.arm_motor_Btn.setStyleSheet("background-color: rgb(50, 221, 50);")
            else:
                self.real_foot.disarmAllMotor()
                self.GUI.arm_motor_Btn.setStyleSheet("background-color: rgb(221, 221, 221);")
            self.updateRealTable()

    def checkCurrrentCycle(self):
        '''
            Check gait execute cycle times. Stop executing if cycle times reach the value of "cycle" box.
        '''
        if self.target[1][2]:
            self.cycle_passed += 1
            if self.cycle_passed >= self.GUI.cycle_Sb.value():
                    self.modelTimer.stop()

    def clearData(self):
        for motor in self.dummy_foot.motors:
            motor.position_log.fill(0)
            motor.velocity_log.fill(0)
            motor.acc_log.fill(0)
            motor.torque_log.fill(0)
        for motor in self.real_foot.motors:
            motor.position_log.fill(0)
            motor.velocity_log.fill(0)
            motor.acc_log.fill(0)
            motor.torque_log.fill(0)

    def ExitApp(self):
        print("Program Closed")
        sys.exit()

    def initiateAgent(self):
        self.modelTimer.stop()
        if self.GUI.device_gpu_rBtn.isChecked():
            device_name = QL.initialQLearning(designated_gpu=1)
            if QL.device == 'cpu':
                self.GUI.device_cpu_rBtn.setChecked(True)
                self.GUI.statusbar.showMessage("GPU activation FAILED", 2000)
        else:
            device_name = QL.initialQLearning(force_cpu=True)
        self.GUI.device_name_Lb.setText(device_name)

        if self.GUI.float_half_rBtn.isChecked():
            self.switch_half_mode = True
        else:
            self.switch_half_mode = False

        cuda = QL.torch.device('cuda')
        if self.GUI.trt_activate_rBtn.isChecked():
            if QL.device == cuda and QL.HAS_TensorRT:
                self.switch_trt_mode = True
            else:
                self.switch_trt_mode = False
                if not QL.HAS_TensorRT:
                    self.GUI.statusbar.showMessage("TensorRT import Error", 2000)
                elif not QL.device == cuda:
                    self.GUI.statusbar.showMessage("GPU not activate, current device is {0}".format(QL.device), 2000)
                self.GUI.trt_inactivate_rBtn.setChecked(True)
        else:
            self.switch_trt_mode = False
        self.network_loaded = False

    def loadTrainingTrace(self):
        self.p_trace_timer.stop()
        self.p_trace_counter = 0
        trace_folder = "{0}/{1}".format(self.main_trace_folder, self.GUI.trace_SelectBox.currentText())
        self.process_trace = list()
        for trace_name in os.listdir(trace_folder):
            if trace_name.endswith(".trace"):
                file_name = "{0}/{1}".format(trace_folder,trace_name)
                with open(file_name, 'rb') as f:
                    self.process_trace.append(pickle.load(f))
        self.GUI.progress_SLBar.setMaximum(len(self.process_trace))

    def loadQNetwork(self):
        self.network_loaded = False
        if hasattr(self, "learning_conf"):
            result = QL.InitialQNetwork(
                self.learning_conf.network_shape,
                designative_mode = True,
                model_name = self.GUI.model_select_box.currentText(),
                )

            if self.switch_half_mode:
                QL.changeModeltoExcuMode(half_mode=self.switch_half_mode)

            if self.switch_trt_mode == True:
                QL.loadModeltoTensorRT(half_mode=self.switch_half_mode)
                self.choseAction = QL.choseActionTRT
            else:
                QL.changeModeltoExcuMode(half_mode=self.switch_half_mode)
                self.choseAction = QL.choseAction

            self.network_loaded = result
            if result is True:
                self.GUI.statusbar.showMessage("Model Loaded!!!", 1000)
            else:
                self.GUI.statusbar.showMessage("Model Loaded FAILED!!!", 3000)
                return
        else:
            self.GUI.statusbar.showMessage("Load Config First!!!", 1000)
            return

        if result:
            self.total_desitions = 0
            self.dummy_foot = LUtil.selectFoot(self.learning_conf)
            if not self.real_foot.real_motor_mode:
                self.real_foot = LUtil.selectFoot(self.learning_conf)
                self.dyna_diagram.foot_model = self.dummy_foot
            else:
                print("Real foot is activated, not allow to change model")
                self.dyna_diagram.foot_model = self.real_foot
            self.gait = LUtil.selectGait(self.learning_conf)
            if self.gait is None:
                self.GUI.statusbar.showMessage("Gait Error", 2000)
                self.network_loaded = False
                return
            else:
                self.n_action = self.learning_conf.action_resolution**3
                #Make model alive
                target = [False, [*self.gait.GaitLookUp(0)]]
                current_state = LUtil.statelize(self.dummy_foot, target[1])
                # self.choseAction(current_state, self.n_action, self.total_desitions, in_learning=False, half_mode=self.switch_half_mode)

                self.GUI.op_freq.setText("Evaluating Model")
                old_time = time.time()
                for _ in range(50):
                    target = [False, [*self.gait.GaitLookUp(0)]]
                    current_state = LUtil.statelize(self.dummy_foot, target[1])
                    self.choseAction(current_state, self.n_action, self.total_desitions, in_learning=False, half_mode=self.switch_half_mode)
                self.time_out = (time.time() - old_time)/50

                self.maximum_freq = 1 / self.time_out
                self.GUI.op_freq.setText("{0:2f} Hz".format(self.maximum_freq))
                self.bool_possiable_for_realtime = True if self.maximum_freq > self.learning_conf.operating_frequency else False
                if self.gait.__class__.__name__ == 'ObstacleGait_Line':
                    import RewardFunc as RF
                    self.checkObs = RF.RwByObstacle()
                    self.NEEDOBSTACLE = False


    def modelAutoRun(self):
        '''Get ready to execute learning model and initialize plot'''
        if self.network_loaded:
            if self.modelTimer.isActive():
                print("Stop AutoRun mode")
                self.modelTimer.stop()
                return 0
            else:
                print("AutoRun mode is activated")

            self.data.ground_info = self.gait.ground_info
            self.data.obstacle_info = self.gait.obstacle_info
            self.ground_x_recorder = []
            self.ground_y_recorder = []
            self.plot_threads = []
            self.n_action = self.learning_conf.action_resolution**3

            #Excu Counter
            self.total_desitions = 0
            self.target_passed   = 0
            self.cycle_passed    = -1 # since start point is viewed as pass,
                                      # thus the cycle_passed at start will be 0
            self.sys_dt = 1/self.learning_conf.operating_frequency
            self.dyna_diagram.dt = self.sys_dt
            self.each_target_times = round(self.gait.dt / self.sys_dt)
            self.last_target_time = time.time()
            self.target = [False, [*self.gait.GaitLookUp(self.target_passed)]]
            self.initial_point = self.target.copy()
            self.target_log = list()

            if self.real_foot.real_motor_mode:
                self.real_foot.switchMotorMode("velocity")
                self.real_foot.InverseKinematic(*self.gait.GaitLookUp(0)[0:2], needLog=False)
                self.real_foot.switchMotorMode("position")
                self.real_foot.armAllMotor()

            self.dummy_foot.InverseKinematic(*self.gait.GaitLookUp(0)[0:2], needLog = False)

            plot_thread = FootPlot.FootDiagramThread(
                self.foot_diagram,
                self.real_foot if self.real_foot.real_motor_mode else self.dummy_foot,
                data=self.data,
                v_cmd=[0, 0, 0],
                PYQTGRAPH_ON=self.PYQTGRAPH_ON
            )
            plot_thread.start()


            #Start Running
            self.MOTOR_ACTION_SPACE = LUtil.setActionSpace(self.learning_conf)


            # Make robot_foot_mode history motor log record total cycle data.
            self.real_foot.resetLoglength(
                self.gait.numberOfPoint * self.GUI.cycle_Sb.value() * self.each_target_times
            )
            self.dummy_foot.resetLoglength(
                self.gait.numberOfPoint * self.GUI.cycle_Sb.value() * self.each_target_times
            )

            self.modelTimer.start()


    def modelPloter(self, foot_model, list_velocity_command=[0, 0, 0]):
        if self.plotThreadIsNotRuning():
            # if foot_model.real_motor_mode:
            #     foot_model.updateRealMotorsInfo()
            self.data.target = self.target
            self.data.recv_count = self.total_desitions
            self.data.ground_info = self.gait.ground_info
            self.data.obstacle_info = self.gait.obstacle_info
            self.plot_threads = list()

            self.plot_threads.append(FootPlot.FootDiagramThread(
                self.foot_diagram, foot_model,
                data=self.data,
                v_cmd=list_velocity_command,
                need_repaint=False,
                PYQTGRAPH_ON=self.PYQTGRAPH_ON
            ))

            if self.dynamic_log_on and self.dynaGUI.tabWidget.isVisible():
                self.plot_threads.append(FootPlot.DynamicLogThread(
                        self.dyna_diagram, foot_model, self.gait.numberOfPoint,
                        PYQTGRAPH_ON=self.PYQTGRAPH_ON
                    ))


            #### Uncomment code below if custom time delay for plot is needed
            #### Change the time.sleep value
            self.plot_threads.append(threading.Thread(
                target=lambda:time.sleep(0.01),
                daemon=True,
            ))

            if self.PYQTGRAPH_ON:
                if self.gait.__class__.__name__ == 'ObstacleGait_Line':
                    self.checkObs.detactHit(foot_model, self.gait)

                if self.NEEDOBSTACLE:
                    self.gait.obstacle_info = self.data.obstacle_info = Obstacle('circle')

            for thread in self.plot_threads:
                thread.start()


    def modelRunner(self):
        if self.real_foot.real_motor_mode:
            self.real_foot.updateRealMotorsInfo()
            current_state = LUtil.statelize(self.real_foot, self.target[1])
            action, _, _ = self.choseAction(
                current_state, 
                self.n_action,
                self.total_desitions,
                in_learning=False,
                half_mode=self.switch_half_mode
            )
            if self.target[1][1] == self.learning_conf.ground_level:
                loading = self.learning_conf.on_ground_loading
            else:
                loading = self.learning_conf.off_ground_loading
            list_velocity_command = LUtil.processAction(action, self.MOTOR_ACTION_SPACE)

            list_position_command = [
                self.real_foot.Upper_Motor.GetPosition("deg") + np.rad2deg(list_velocity_command[0])*self.sys_dt,
                self.real_foot.Middle_Motor.GetPosition("deg") + np.rad2deg(list_velocity_command[1])*self.sys_dt,
                self.real_foot.Lower_Motor.GetPosition("deg") + np.rad2deg(list_velocity_command[2])*self.sys_dt
            ]

            self.real_foot.excu_position_cmd(
                list_position_command,
                self.sys_dt,
                needLog=True,
            )
            if self.real_foot.InterferenceFlag:
                self.dummy_foot.InverseKinematic(*self.initial_point[1], needLog=False)
                list_position_command = [
                    self.dummy_foot.Upper_Motor.GetPosition("deg"),
                    self.dummy_foot.Middle_Motor.GetPosition("deg"),
                    self.dummy_foot.Lower_Motor.GetPosition("deg")
                ]
                self.real_foot.switchMotorMode("velocity")
                self.real_foot.InverseKinematic(*self.initial_point[1], needLog=False)
                self.real_foot.switchMotorMode("position")
                self.real_foot.armAllMotor()
                self.real_foot.excu_position_cmd(
                    list_position_command,
                    self.sys_dt,
                    needLog=True,
                )
            self.modelPloter(self.real_foot, list_velocity_command)


        else:
            current_state = LUtil.statelize(self.dummy_foot, self.target[1])
            action, _, _ = self.choseAction(
                current_state, 
                self.n_action,
                self.total_desitions,
                in_learning=False,
                half_mode=self.switch_half_mode
            )
        
            if self.target[1][1] == self.learning_conf.ground_level:
                loading = self.learning_conf.on_ground_loading
            else:
                loading = self.learning_conf.off_ground_loading

            list_velocity_command = LUtil.processAction(action, self.MOTOR_ACTION_SPACE)

            self.dummy_foot.excu_velocity_cmd(
                list_velocity_command,
                self.sys_dt,
                needLog=True,
                f_cmd=loading
            )
            self.modelPloter(self.dummy_foot, list_velocity_command)

        if self.total_desitions % self.each_target_times == 0:
            self.target = [False, self.gait.GaitLookUp(self.target_passed)]
            self.data.ground_info = self.gait.ground_info.getGroundInfo() if self.gait.ground_info is not None else None
            self.target_passed += 1
            self.checkCurrrentCycle()

        self.total_desitions += 1
        self.target_log.append(self.target_passed)
        self.data.tsne_info = ((current_state, action, current_state, 0), False)
        self.data.motor_info = self.dummy_foot.getMotorInfo()
        self.data.target = self.target

        if self.gait.ground_info is not None:
            ground_x_index = np.argmin(abs(self.gait.ground_info.ground_x - self.target[1][0]))
            self.ground_x_recorder.append(self.target[1][0] + self.target_passed*self.gait.Ground_Step)
            self.ground_y_recorder.append(self.gait.ground_info.ground_y[ground_x_index])



    def modelStepRun(self):
        pass


    def plotProcessTrace(self):
        info, time, index =  self.process_trace[self.trace_selection-1][self.p_trace_counter]
        self.dummy_foot.excuByInfo(info)
        self.foot_diagram.update_data(
            self.dummy_foot, TrainigData(), v_cmd=[motor.GetVelocity() for motor in self.dummy_foot.motors]
        )
        # self.foot_diagram.plotting2()
        self.p_trace_counter += 1
        if self.p_trace_counter == len(self.process_trace[self.trace_selection-1]):
            self.p_trace_timer.stop()


    def plotThreadIsNotRuning(self):
        '''
            Check if plot thread is running,
            return "True" is no thread is running
        '''
        if self.plot_threads == []:
            return True
        else:
            running = False
            for thread in self.plot_threads:
                # print(thread.is_alive())
                running = running or thread.is_alive()
                if running:
                    return False
            return not(running)


    def plotGlobalGaitGrapthShowGround(self):
        '''Toggle to show ground in GaitPlotGUI'''
        self.gait_plot_show_ground = not self.gait_plot_show_ground
        if self.gait_plot_show_ground:
            self.gaitGUI.btn_show_ground.setStyleSheet("background-color: rgb(0, 255, 59);")
        else:
            self.gaitGUI.btn_show_ground.setStyleSheet("background-color: rgb(225, 225, 225);")


    def plotGlobalGaitGrapthDensityFactor(self):
        self.global_gait_graph_density_factor = self.gaitGUI.DensityInput.value()


    def plotGlobalGaitGrapth(self):
        '''Start FootPlot.GaitPlotingThread'''
        if self.gait_thread is None or not self.gait_thread.is_alive():
            self.gait_thread = FootPlot.GaitPlotingThread(
                self.gait_diagram,
                self.real_foot if self.real_foot.real_motor_mode else self.dummy_foot,
                self.gait, self.each_target_times,
                self.target_log,
                self.global_gait_graph_density_factor,
                # self.PYQTGRAPH_ON,
                False,
                self.gait_plot_show_ground,
                self.ground_x_recorder,
                self.ground_y_recorder
            )
            self.gait_thread.start()


    def realTimeMode(self):
        if self.real_foot.real_motor_mode:
            if self.bool_real_time_mode:
                print("Reset Real Time timer")
            else:
                self.bool_real_time_mode = True
            self.modelTimer.setInterval(round(1000/self.learning_conf.operating_frequency))
        else:
            if self.GUI.real_time_Rb.isChecked() and self.bool_possiable_for_realtime:
                self.modelTimer.setInterval(round(1000/self.learning_conf.operating_frequency))
                self.bool_real_time_mode = True
            else:
                print(self.time_out)
                self.modelTimer.setInterval(self.time_out*1000)
                self.bool_real_time_mode = False


    def saveData(self, form):
        now = datetime.datetime.now()
        folder_name = "{0}@{1:02d}{2:02d}-{3}{4}".format(
            self.learning_conf.name,
            now.month, now.day, now.hour, now.minute
        )
        folder_name = "./excuResult/"+folder_name
        print(folder_name)
        os.mkdir(folder_name)
        type_list = ["pos"        , "vel"        , "acc"        , "toq"]
        attr_list = ["position_log", "velocity_log", "acc_log", "torque_log"]
        data = dict()

        foot_model = self.real_foot if self.real_foot.real_motor_mode else self.dummy_foot

        try:
            if form == "mat":
                import scipy.io as sio
                for motor in foot_model.motors:
                    motor_name = motor.name
                    motor_data = dict()
                    for d_type, attr in zip(type_list, attr_list):
                        motor_data[d_type] = getattr(motor, attr)
                    data[motor_name] = motor_data
                timeBase = np.arange(-foot_model.log_len,0)
                timeBase = timeBase * self.sys_dt
                data['timeBase'] = timeBase
                sio.savemat("{0}/data.mat".format(folder_name),data)
            elif form == "xlsx":
                from pandas import DataFrame
                for motor in foot_model.motors:
                    data[motor.name] = np.array([None]*foot_model.log_len)
                    for d_type, attr in zip(type_list, attr_list):
                        data[d_type + " " + motor.name] = getattr(motor, attr)
                timeBase = np.arange(-foot_model.log_len, 0)
                timeBase = timeBase * self.sys_dt
                data["timeBase"] = np.array([None]*foot_model.log_len)
                data['time'] = timeBase

                data_excel = DataFrame(data)
                data_excel.to_excel("{0}/data.xlsx".format(folder_name))
            print(f"Save with {form}")
        except:
            traceback.print_exc()
            print("Unable to save")


    def selectTrace(self):
        self.trace_selection = self.GUI.progress_SLBar.value()
        self.GUI.trace_progress_Lb.setText(str(self.trace_selection))


    def showDynamicWindow(self):
        self.dynamic_log_on = True
        self.updateDynamicData()
        self.dynaGUI.window.show()


    def startPlottingPTrace(self):
        self.p_trace_counter = 0
        self.p_trace_timer.setInterval(100)
        self.p_trace_timer.start()


    def switchOffset(self):
        if self.GUI.with_offset_rb.isChecked():
            self.with_offset = True
        else:
            self.with_offset = False


    def updateDynamicData(self):
        '''
            If not in PYQTGRAPH plot mode, only update plot
            when clicking the tab in Dynamic_Log_Window for fear
            of enormous lag when us matplotlib.
        '''
        foot_model = self.real_foot if self.real_foot.real_motor_mode else self.dummy_foot
        if hasattr(self,'foot_model'):
            if not self.PYQTGRAPH_ON:
                self.dyna_diagram.UpdatePlot(foot_model)
        else:
            self.GUI.statusbar.showMessage("Run Model First!!!", 1000)


    def updateLearningConfiguration(self):
        config_name = "./model/{0}.lrconf".format(self.GUI.model_select_box.currentText())
        try:
            with open(config_name,'rb') as f:
                self.learning_conf = pickle.load(f)
        except FileNotFoundError:
            self.GUI.statusbar.showMessage("No Configuration File", 1000)
            return
        except Exception:
            traceback.print_exc()
            return

        attr_list = list()
        for attr in dir(self.learning_conf):
            if not attr.startswith("__"):
                attr_list.append((attr, getattr(self.learning_conf,attr)))
        self.GUI.config_Tb.setRowCount(len(attr_list))
        for index,value in enumerate(attr_list):
            self.GUI.config_Tb.setItem(index,0,QtWidgets.QTableWidgetItem(value[0]))
            self.GUI.config_Tb.setItem(index,1,QtWidgets.QTableWidgetItem(str(value[1])))
        self.GUI.config_Tb.resizeRowsToContents()
        self.bool_config_loaded = True


    def updateRealModel(self):
        self.real_foot.updateRealMotorsInfo()
        self.foot_diagram.UpdatePlot(self.real_foot, True)
        self.updateRealTable()


    def updateRealTable(self):
        for motor_index, sub_list in enumerate(self.real_motor_talbe_itemlist):
            motor = self.real_foot.motors[motor_index]
            sub_list[0].setText("On" if self.real_foot.joint_motor_armed else "Off")
            sub_list[1].setText(str(round(motor.GetPosition(Unit='deg',with_offset=self.with_offset),2)))
            sub_list[2].setText(str(round(motor.GetVelocity(Unit='rad'),2)))
            sub_list[3].setText(str(round(motor.GetCurrent(),2)))
            sub_list[4].setText(str(motor.GetTemperture()))
        self.GUI.real_motor_table.repaint()



class ExecutorUI(erUI.Ui_MainWindow):
    def __init__(self):
        self.excu_window = QtWidgets.QMainWindow()
        self.setupUi(self.excu_window)

        #Modify radio button group
        self.device_rBtn_group = QtWidgets.QButtonGroup()
        self.device_rBtn_group.addButton(self.device_gpu_rBtn)
        self.device_rBtn_group.addButton(self.device_cpu_rBtn)
        self.device_cpu_rBtn.setChecked(True)
        self.float_rBtn_group = QtWidgets.QButtonGroup()
        self.float_rBtn_group.addButton(self.float_normal_rBtn)
        self.float_rBtn_group.addButton(self.float_half_rBtn)
        self.float_normal_rBtn.setChecked(True)
        self.trt_rBtn_group = QtWidgets.QButtonGroup()
        self.trt_rBtn_group.addButton(self.trt_activate_rBtn)
        self.trt_rBtn_group.addButton(self.trt_inactivate_rBtn)
        self.trt_inactivate_rBtn.setChecked(True)

class DynamicLogUI(dyUI.Ui_MainWindow):
    def __init__(self):
        self.window = QtWidgets.QMainWindow()
        self.setupUi(self.window)

class GaitUI(gaUI.Ui_MainWindow):
    def __init__(self):
        self.window = QtWidgets.QMainWindow()
        self.setupUi(self.window)

class FootCyclePlot(threading.Thread):

    def __init__(self,foot_model,cycle):
        threading.Thread.__init__(self)
        self.foot_model = foot_model
        self.cycle = cycle



def configGenerator():
    print("Using This means YOU KNOW WHAT YOU'R DOING!!!!")
    model_list = list()
    print("Exist Model:")
    for name in os.listdir("./model"):
        if name.endswith('.model'): 
            print("    "+name[:-6])
            model_list.append(name[:-6])
    print("==================================================")
    
    config_name = input("Name:")
    config_file = "./model/{0}.lrconf".format(config_name)
    model_file  = "./model/{0}.model".format(config_name)
    shape_file  = "./model/{0}.shape".format(config_name)

    no_modify = [
        "configuration_discription", 
        "configuration_version",
        "object_type",
        "NETWORK_SHAPE"
        ]
    if os.path.exists(model_file) and not os.path.exists(config_file):
        conf = TrainingConfiguration()
        with open(shape_file,'rb') as f:
            shape = pickle.load(f)
        print("model shape is: {0}".format(shape))
        for attr in dir(conf):
            if not attr.startswith("__") and (attr not in no_modify):
                msg = "The defalt value of conf.{0} is :: {1}".format(attr, getattr(conf,attr))
                print(msg)
                input_value = input("New Value?:")
                if input_value != "":
                    if input_value.startswith("[") and input_value.endswith("]"):
                        value = list()
                        input_value = input_value[1:-1].split(",")
                        for sub_value in input_value:
                            if "." in sub_value:
                                value.append(float(sub_value))
                            else:
                                value.append(int(sub_value))
                        setattr(conf,attr,value)
                    else:
                        try:
                            setattr(conf, attr, int(input_value))
                        except ValueError:
                            setattr(conf, attr, input_value)
                else:
                    #Nothing Inputed
                    pass
        with open(config_file, "wb+") as f:
            pickle.dump(conf,f)
    
    elif os.path.exists(config_file):
        print("Configuration File Already Exists !!!")
        sys.exit()
    elif not os.path.exists(model_file):
        print("No Such Model !!!")
        sys.exit()

if __name__ == "__main__":
    # try:
    #     if sys.argv[1] == 'ConfGen':
    #         print("Config Generator Mode")  
    #         configGenerator()
    # except IndexError:
    #     App()
    App()