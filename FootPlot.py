from matplotlib.backends.backend_qt5agg import FigureCanvas #pylint: disable=no-name-in-module
from matplotlib.figure import Figure
from matplotlib import pyplot
import numpy as np
import random
import time
import threading
import robot_leg_model
import LearningUtility

import pyqtgraph
from PyQt5 import QtCore
import traceback

class Pens:
    def __init__(self, color_id=0):
        various_color = random.randint(color_id, 200) if color_id > 0 else 0

        self.ground_pen = pyqtgraph.mkPen(color=(various_color, various_color, various_color), width=3)
        self.trace_pen = pyqtgraph.mkPen(color=(0, 255, various_color, 95), width=2, style=QtCore.Qt.SolidLine)
        self.motor_position_pen = pyqtgraph.mkPen(color=(various_color, various_color, 255), width=3, style=QtCore.Qt.SolidLine)
        self.obstacle_pen = pyqtgraph.mkPen(color=(various_color, various_color, various_color), width=2)


    def back_pen(self, width=2, style=QtCore.Qt.SolidLine):
        return pyqtgraph.mkPen(color='k', width=width, style=style)
    def green_pen(self, width=3, style=QtCore.Qt.SolidLine):
        return pyqtgraph.mkPen(color='g', width=width, style=style)



class FootWidget:
    ######## Modified by Clement ########
    def __init__(self, layout):
    #####################################
    # def __init__(self, layout, need_trace = False, need_text=True ,need_ground=False):
        self.dynamic_canvas = FigureCanvas(Figure())

        self.counter = 0
        self.counter_fail = 0
        self.start_time = 0
        self.plot_count = 0
        self.plot_update_rate = 0
        self.last_plot_count = 0
        self.fail      = False
        self._dynamic_ax = None
        self.plot_widgets = None
        self.data = None

        self.plot_widgets = pyqtgraph.PlotWidget(background='w')
        self.plot_widgets.setMouseEnabled(False, False)
        self.plot_widgets.setXRange(-25, 25)
        self.plot_widgets.setYRange(-43, 3)
        self.text_plot = pyqtgraph.TextItem("plot count: " + str(self.counter), color=(0,0,0))
        self.text_plot.setPos(-15, 4)
        self.draw_plot = pyqtgraph.TextItem("update rate: "+ str(self.plot_update_rate), color=(0,0,0))
        self.draw_plot.setPos(-15, 2.5)
        self.status_text = pyqtgraph.TextItem("", color=(255,0,0))
        self.status_text.setPos(-15, 1)
        self.plot_widgets.addItem(self.text_plot)
        self.plot_widgets.addItem(self.draw_plot)
        self.plot_widgets.addItem(self.status_text)
        layout.addWidget(self.plot_widgets)


    def updateData(self, plot_count, data):
        self.plot_count = plot_count
        self.data       = data

    def updatePlotWidget(self):
        self.counter += 1

        if self.data is not None:
            try:
                if self.data.tsne_info[0][2] is None:
                    self.fail = True
                else:
                    self.fail = False
            except TypeError:
                self.fail = True


            self.text_plot.setText("plot count: " + str(self.plot_count))
            self.draw_plot.setText("plot update rate: " + str(round(self.plot_update_rate, 0)))
            if self.fail:
                self.status_text.setText("FAIL")
                self.status_text.setColor((255,0,0))
                self.counter_fail = self.plot_count
            elif self.plot_count - self.counter_fail > 150:
                self.status_text.setText("Normal")
                self.status_text.setColor((0,255,0))

            if self.data.target[0]:
                self.status_text.setText("manual mode")
            if not self.data.under_learning:
                self.status_text.setText("Off learning")
            if self.data.real_time:
                self.status_text.setText("Real time")




            if self.counter == 1:
                self.start_time = time.time()
                self.last_plot_count = self.plot_count
            if time.time() - self.start_time >= 1:
                self.plot_update_rate = self.plot_count - self.last_plot_count
                self.counter = 0


    def returnPlotWidget(self):
        return self.plot_widgets

class Foot:
    '''
        This is used to plot new foot on Foot plot
    '''
    def __init__(self, plot_widgets=None,
        need_trace=False, need_text=True ,need_ground=False, 
        need_obstacle=False, need_target=False, color_id=0
    ):
        self.need_text = need_text
        self.need_ground = need_ground
        self.need_trace = need_trace
        self.need_obstacle = need_obstacle
        self.need_target = need_target
        self.plot_widgets = plot_widgets if plot_widgets is not None else None
        self.trace_len = 12
        self.trace = [list(), list()]
        self.target = [False, [0, -35]]
        self.fail      = False
        self.ploting_function = list()
        self.Pens = Pens(color_id)

        if need_text:
            self.motor1_state_text = pyqtgraph.TextItem("", color=(0,0,0))
            self.motor2_state_text = pyqtgraph.TextItem("", color=(0,0,0))
            self.motor3_state_text = pyqtgraph.TextItem("", color=(0,0,0))
            self.end_point_text = pyqtgraph.TextItem("", color=(0,0,0))
            self.plot_widgets.addItem(self.motor1_state_text)
            self.plot_widgets.addItem(self.motor2_state_text)
            self.plot_widgets.addItem(self.motor3_state_text)
            self.plot_widgets.addItem(self.end_point_text)
            self.ploting_function.append(self.plotText)
        if need_ground:
            self.ground_plot = self.plot_widgets.plot(pen=self.Pens.ground_pen)
            self.ploting_function.append(self.plotGround)
        if need_trace:
            self.trace_plot = self.plot_widgets.plot(pen=self.Pens.trace_pen)
            self.ploting_function.append(self.plotTrace)
        if need_obstacle:
            self.obstacle_plot = self.plot_widgets.plot(pen=self.Pens.obstacle_pen)
            self.ploting_function.append(self.plotObstacle)

        if need_target:
            self.target_point_plot = self.plot_widgets.plot(symbol='x', symbolSize=20, symbolBrush=(255,0,0))
            self.ploting_function.append(self.plotTarget)


        self.motor_position_plot = self.plot_widgets.plot(pen=self.Pens.motor_position_pen)
        self.ploting_function.append(self.plotFoot)

    def update_data(self, foot_model, data, v_cmd, need_repaint=False):
        self.foot_model = foot_model
        self.data = data
        self.need_repaint = need_repaint
        self.target = self.data.target
        self.ground_info = self.data.ground_info
        self.obstacle_info = self.data.obstacle_info
        self.v_cmd = v_cmd
        try:
            if data.tsne_info[0][2] is None:
                self.fail = True
                # print(self.data.tsne_info[0][2])
            else:
                self.fail = False
        except TypeError:
            self.fail = True

    


    def plotTarget(self, target):
        self.target_point_plot.setData([target[1][0]], [target[1][1]])

    def plotFoot(self, **kwargs):
        foot_model = kwargs['foot_model']
        expect_foot = kwargs['expect_foot']

        LineX = np.array([foot_model.upper_motor_pos[0],
            # foot_model.Mid_Moter_Pos[0],
            foot_model.lower_motor_pos[0],
            foot_model.EndPoint[0]])
        LineY = np.array([foot_model.upper_motor_pos[1],
            # foot_model.Mid_Moter_Pos[1],
            foot_model.lower_motor_pos[1],
            foot_model.EndPoint[1]])

        if foot_model.InterferenceFlag == True:
            self.motor_position_plot.setPen(pyqtgraph.mkPen(color='r', width=3))
            self.motor_position_plot.setData(LineX, LineY)


        else:
            self.motor_position_plot.setPen(self.Pens.motor_position_pen)
            self.motor_position_plot.setData(LineX, LineY)


        if expect_foot:
            real_LineX = np.array([expect_foot.upper_motor_pos[0],
                # expect_foot.Mid_Moter_Pos[0],
                expect_foot.lower_motor_pos[0],
                expect_foot.EndPoint[0]])
            real_LineY = np.array([expect_foot.upper_motor_pos[1],
                # expect_foot.Mid_Moter_Pos[1],
                expect_foot.lower_motor_pos[1],
                expect_foot.EndPoint[1]])


    def plotText(self, **kwargs):
        ''' 
            Plot some info on plot, 
            PLEASE_NOTE!!! If PYQTGRAPH_ON, 
            this method takes a bit long time to execute
        '''
        try:
            self.motor1_state_text.setPos(self.foot_model.upper_motor_pos[0] + 2, self.foot_model.upper_motor_pos[1])
            # self.motor2_state_text.setPos(self.foot_model.Mid_Moter_Pos[0] + 2, self.foot_model.Mid_Moter_Pos[1])
            self.motor3_state_text.setPos(self.foot_model.lower_motor_pos[0] + 2, self.foot_model.lower_motor_pos[1])
            self.end_point_text.setPos(self.foot_model.EndPoint[0] + 2, self.foot_model.EndPoint[1])
            if not self.fail:
                self.motor1_state_text.setText("S: {} , A: {}".format(self.data.tsne_info[0][0][2], self.v_cmd[0]))
                self.motor2_state_text.setText("S: {} , A: {}".format(self.data.tsne_info[0][0][3], self.v_cmd[1]))
                self.motor3_state_text.setText("S: {} , A: {}".format(self.data.tsne_info[0][0][4], self.v_cmd[2]))
                self.end_point_text.setText(
                    str(round(self.data.tsne_info[0][0][0], 2)) + " , " + 
                    str(round(self.data.tsne_info[0][0][1], 2))
                    )
        except:
            pass



    def plotTrace(self, **kwargs):
        foot_model = kwargs['foot_model']
        if foot_model:
            self.trace[0].append(foot_model.EndPoint[0])
            self.trace[1].append(foot_model.EndPoint[1])
            if len(self.trace[0]) > self.trace_len:
                del self.trace[0][0]
                del self.trace[1][0]
            self.trace_plot.setData(self.trace[0], self.trace[1])



    def plotGround(self, **kwargs):
        ground_info = kwargs['ground_info']
        if ground_info.__class__.__name__ == 'tuple':
            self.ground_plot.setData(*ground_info)
        elif ground_info.__class__.__name__ == 'Ground':
            self.ground_plot.setData(*ground_info.getGroundInfo())
        elif ground_info is not None:
            self.ground_plot.setData(*ground_info.getGroundInfo())
        else:
            pass

    def plotObstacle(self, **kwargs):
        obstacle_info = kwargs['obstacle_info']
        if obstacle_info.__class__.__name__ == 'tuple':
            self.obstacle_plot.setData(*obstacle_info)
        elif obstacle_info.__class__.__name__ == 'Obstacle':
            self.obstacle_plot.setData(*obstacle_info.getObstacleInfo())
        else:
            pass

    def repaint(self):
        pass

    def clearTrace(self):
        self.trace = [list(), list()]


class ActionInfo:
    def __init__(self, GUI, status_info, 
            data              = None, 
            learningServer    = str(), 
            PORT              = 0, 
            _run_flag         = False,
            PYQTGRAPH_ON      = True,
            action_plot_buffer_size = 3000,

        ):

        self.GUI = GUI
        self.status_info = status_info
        self.data = data
        self.learninigServer = learningServer
        self.PORT = PORT
        self._run_flag = _run_flag
        self.PYQTGRAPH_ON = PYQTGRAPH_ON
        self.action_plot_buffer_size = action_plot_buffer_size
        self.action_resolution = 41
        self.each_motor_action_summarize = np.zeros((3, self.action_resolution))
        self.action_space = np.linspace(0, self.action_resolution)
        self.counter = 0
        if self.PYQTGRAPH_ON:
            self.action_plot_widgets_list = [pyqtgraph.PlotWidget(background='w', title="Motor {} action".format(i)) for i in range(3)]
            self.each_action_statistic_data_plot = [pyqtgraph.BarGraphItem(x=range(self.action_resolution), height=range(self.action_resolution), width=0.6, brush='b') for i in range(3)]
            for i, j in zip(self.action_plot_widgets_list, self.each_action_statistic_data_plot):
                i.addItem(j)
                # i.setYRange(0, round(action_plot_buffer_size/2, 0))
                self.GUI.StateInfo_layout1.addWidget(i)



  
        self.learning_frequency_mode, self.status_info.status_cmd.state_monitor[0] = False, False
        self.slowly_update_mode = False
        self.single_step_mode = False
        self.data_receive_count = 0



        self.GUI.StateInfo_frequency_start_stop_Btn.clicked.connect(self.frequencyStartStopButtonClick)
        self.GUI.state_info_frequency_spinBox.valueChanged.connect(self.frequrncyChange)
        self.GUI.StateInfo_slowly_start_stop_Btn.clicked.connect(self.slowlyStartStopButtonClick)
        self.GUI.state_info_slowly_spinBox.valueChanged.connect(self.slowlyChange)



    def updateStatus(self, GUI, status_info, 
            data, 
            learningServer, 
            PORT, 
            _run_flag,
            plotTimer,
            each_motor_action,
            action_resolution,
            data_receive_count,
        ):
        self.GUI = GUI
        self.status_info = status_info
        self.data = data
        self.learninigServer = learningServer
        self.PORT = PORT
        self._run_flag = _run_flag
        self.each_motor_action = each_motor_action
        self.data_receive_count = data_receive_count

        if action_resolution != self.action_resolution: # Reset if action resolution change
            self.each_motor_action_summarize = np.zeros((3, action_resolution))
            self.action_resolution = action_resolution
            for i, j in zip(self.each_action_statistic_data_plot, range(3)):
                i.setOpts(x=range(self.action_resolution))

        if self.data_receive_count < self.action_plot_buffer_size:
            for i in range(3):
                self.each_motor_action_summarize[i][each_motor_action[i]] += 1
        elif self.data_receive_count >= self.action_plot_buffer_size:
            for i in range(3):
                self.each_motor_action_summarize[i][each_motor_action[i]] += 2
            if self.data_receive_count%self.action_resolution == 0:
                self.each_motor_action_summarize -= 1
            self.each_motor_action_summarize = np.clip(self.each_motor_action_summarize, a_min=0, a_max=None) # if a_min or a_max is None, no limitation on min or max


    def updatePlot(self):
        if self.GUI.tabWidget.currentIndex() == 2 and self.PYQTGRAPH_ON:
            if self.learning_frequency_mode and not self.status_info.status_cmd.state_monitor[0]:
                self.status_info.status_cmd.state_monitor[0] = True
                self.status_info.StatusSend(self.learninigServer, self.PORT)
            # self.GUI.lcd_current_eoa.display(round(self.data.tsne_info[0][0][0], 2))
            # self.GUI.lcd_current_eor.display(round(self.data.tsne_info[0][0][1], 2))
            # if self.data.tsne_info[0][2] is not None:
            #     self.GUI.lcd_new_eoa.display(round(self.data.tsne_info[0][2][0], 2))
            #     self.GUI.lcd_new_eor.display(round(self.data.tsne_info[0][2][1], 2))
            #     try:
            #         ### Better show new first, since currrent has more than 5 elements 
            #         ### with unknown reason
            #         # self.GUI.lcd_current_v.display(round(self.data.tsne_info[0][0][5], 2))
            #         self.GUI.lcd_new_v.display(round(self.data.tsne_info[0][2][5], 2)) 
            #         self.GUI.lcd_current_v.display(round(self.data.tsne_info[0][0][5], 2))
            #     except IndexError:
            #         # print("IndexError {}".format(self.data.tsne_info))
            #         pass
            self.updateEachMotorActionPlot(
                self.each_motor_action,
                self.action_resolution,
                self.data_receive_count
            )
        else:
            if self.learning_frequency_mode:
                self.status_info.status_cmd.state_monitor[0] = False
                self.status_info.StatusSend(self.learninigServer, self.PORT)


    def slowlyChange(self):
        if self._run_flag:
            pass

    def slowlyStartStopButtonClick(self):
        if self._run_flag:
            if self.single_step_mode:
                self.single_step_mode = False

            if self.status_info.status_cmd.state_monitor[0] or self.learning_frequency_mode:
                self.learning_frequrncy_mode, self.status_info.status_cmd.state_monitor[0] = False, False
                self.status_info.StatusSend(self.learninigServer, self.PORT)
                self.GUI.StateInfo_frequency_start_stop_Btn.setText("Frequency Start")

            if not self.slowly_update_mode:
                self.GUI.StateInfo_slowly_start_stop_Btn.setText("Slowly update data Stop")
                # self.GUI.StateInfo_start_stop_Btn.repaint()
                self.slowly_update_mode = True

            else:
                self.GUI.StateInfo_slowly_start_stop_Btn.setText("Slowly update data Start")
                self.slowly_update_mode = False
        else:
            self.GUI.statusbar.showMessage("Server is not connect !!!!!!!!!! ",2000)

    def frequrncyChange(self):
        if self._run_flag:
            self.status_info.status_cmd.state_monitor[1] = self.GUI.state_info_frequency_spinBox.value()
            self.status_info.StatusSend(self.learninigServer, self.PORT)    

    def frequencyStartStopButtonClick(self):
        if self._run_flag:
            if self.single_step_mode:
                self.single_step_mode = False
            if self.slowly_update_mode:
                self.GUI.StateInfo_slowly_start_stop_Btn.setText("Slowly update data Start")
                self.slowly_update_mode = False


            if not self.status_info.status_cmd.state_monitor[0]:
                self.learning_frequency_mode = True
                self.GUI.StateInfo_frequency_start_stop_Btn.setText("Frequrncy Stop")
                # self.GUI.StateInfo_start_stop_Btn.repaint()
                self.status_info.status_cmd.state_monitor = [True, self.GUI.state_info_frequency_spinBox.value()]
                self.status_info.StatusSend(self.learninigServer, self.PORT)
            else:
                self.learning_frequency_mode = False
                self.GUI.StateInfo_frequency_start_stop_Btn.setText("Frequency Start")
                self.status_info.status_cmd.state_monitor = [False, self.GUI.state_info_frequency_spinBox.value()]
                self.status_info.StatusSend(self.learninigServer, self.PORT)
        else:
            self.GUI.statusbar.showMessage("Server is not connect !!!!!!!!!! ",2000)



    def updateEachMotorActionPlot(self, each_motor_action, action_resolution, data_receive_count):
        '''each_motor_action = [v1, v2, v3]
            action_resolution = axis-x for BarGraph'''
        # if action_resolution != self.action_resolution: # Reset if action resolution change
        #     self.each_motor_action_summarize = np.zeros((3, action_resolution))
        #     self.action_resolution = action_resolution
        #     for i, j in zip(self.each_action_statistic_data_plot, range(3)):
        #         i.setOpts(x=range(self.action_resolution))
        # if data_receive_count < self.action_plot_buffer_size:
        #     for i in range(3):
        #         self.each_motor_action_summarize[i][each_motor_action[i]] += 1
        # elif data_receive_count >= self.action_plot_buffer_size:
        #     for i in range(3):
        #         self.each_motor_action_summarize[i][each_motor_action[i]] += 2
        #     self.each_motor_action_summarize -= 1
        #     self.each_motor_action_summarize = np.clip(self.each_motor_action_summarize, a_min=0, a_max=None) # if a_min or a_max is None, no limitation on min or max
        for i, j in zip(self.each_action_statistic_data_plot, range(3)):
            i.setOpts(height=self.each_motor_action_summarize[j])

    def change_action_plot_buffer_size(self, buffer_size):
        self.action_plot_buffer_size = buffer_size
        # for i in self.action_plot_widgets_list:
            # i.setYRange(0, round(buffer_size/2, 0))

    def reset_action_plot(self):
        self.counter = 0
        self.action_resolution = 21
        self.each_motor_action_summarize = np.zeros((3, self.action_resolution))


class Dynamic():
    def __init__(self, dt ,dynaGUI, PYQTGRAPH_ON=False, foot_model=None, numberOfPoint=0):
        self.PYQTGRAPH_ON = PYQTGRAPH_ON
        self.dt = dt
        self.UI = dynaGUI
        self.timeBase = np.arange(-200, 0)
        self.timeBase = self.timeBase * dt

        self.Up_AV_figure = FigureCanvas(Figure())
        self.Up_AA_figure = FigureCanvas(Figure())
        # self.Mi_AV_figure = FigureCanvas(Figure())
        # self.Mi_AA_figure = FigureCanvas(Figure())
        self.Lo_AV_figure = FigureCanvas(Figure())
        self.Lo_AA_figure = FigureCanvas(Figure())
        self.upper_tq_fig = FigureCanvas(Figure())
        # self.midde_tq_fig = FigureCanvas(Figure())
        self.lower_tq_fig = FigureCanvas(Figure())
        self.Up_AP_figure = FigureCanvas(Figure())
        # self.Mi_AP_figure = FigureCanvas(Figure())
        self.Lo_AP_figure = FigureCanvas(Figure())

        if self.PYQTGRAPH_ON:
            self.plot_widgets_list = [pyqtgraph.PlotWidget(background='w') for i in range(8)]
            dynaGUI.Up_AV.addWidget(self.plot_widgets_list[0])
            dynaGUI.Up_AA.addWidget(self.plot_widgets_list[1])
            # dynaGUI.Mi_AV.addWidget(self.plot_widgets_list[2])
            # dynaGUI.Mi_AA.addWidget(self.plot_widgets_list[3])
            dynaGUI.Lo_AV.addWidget(self.plot_widgets_list[2])
            dynaGUI.Lo_AA.addWidget(self.plot_widgets_list[3])
            dynaGUI.up_tq.addWidget(self.plot_widgets_list[4])
            # dynaGUI.mi_tq.addWidget(self.plot_widgets_list[7])
            dynaGUI.lo_tq.addWidget(self.plot_widgets_list[5])
            dynaGUI.upper_posi_Lay.addWidget(self.plot_widgets_list[6])
            # dynaGUI.middl_posi_Lay.addWidget(self.plot_widgets_list[10])
            dynaGUI.lower_posi_Lay.addWidget(self.plot_widgets_list[7])
            self.qtgraph_lines = [i.plot(pen='b') for i in self.plot_widgets_list]
            for i in self.plot_widgets_list: i.showGrid(x=True, y=True, alpha=0.95)
            # for i in range(9,12) : self.plot_widgets_list[i].setYRange(0, 360)

            self.show_force = False
            self.numberOfPoint = numberOfPoint
            self.foot_model = foot_model
            self.Data_list = [
                    foot_model.upper_motor.velocity_log,
                    foot_model.upper_motor.acc_log,
                    # foot_model.Middle_Motor.velocity_log,
                    # foot_model.Middle_Motor.acc_log,
                    foot_model.lower_motor.velocity_log,
                    foot_model.lower_motor.acc_log,
                    foot_model.upper_motor.torque_log,
                    # foot_model.Middle_Motor.torque_log,
                    foot_model.lower_motor.torque_log,
                    np.rad2deg(foot_model.upper_motor.position_log)%360,
                    # np.rad2deg(foot_model.Middle_Motor.position_log)%360,
                    np.rad2deg(foot_model.lower_motor.position_log)%360
                ]

            self._labs_list = [
                [self.UI.u_maxv, self.UI.u_minv],
                [self.UI.u_maxa, self.UI.u_mina],
                # [self.UI.m_maxv, self.UI.m_minv],
                # [self.UI.m_maxa, self.UI.m_mina],
                [self.UI.l_maxv, self.UI.l_minv],
                [self.UI.l_maxa, self.UI.l_mina],
                [self.UI.u_maxt, self.UI.u_mint],
                # [self.UI.m_maxt, self.UI.m_mint],
                [self.UI.l_maxt, self.UI.l_mint],
                [self.UI.u_maxp, self.UI.u_minp],
                # [self.UI.m_maxp, self.UI.m_minp],
                [self.UI.l_maxp, self.UI.l_minp],
                ]
            self.tab_indexs = [
                1, 1,
                1, 1,
                2, 2,
                0, 0
                ]
            self.data_types = [
                'v', 'a',
                'v', 'a',
                't', 't',
                'p', 'p',
                ]


        else:
            dynaGUI.Up_AV.addWidget(self.Up_AV_figure)
            dynaGUI.Up_AA.addWidget(self.Up_AA_figure)
            # dynaGUI.Mi_AV.addWidget(self.Mi_AV_figure)
            # dynaGUI.Mi_AA.addWidget(self.Mi_AA_figure)
            dynaGUI.Lo_AV.addWidget(self.Lo_AV_figure)
            dynaGUI.Lo_AA.addWidget(self.Lo_AA_figure)
            dynaGUI.up_tq.addWidget(self.upper_tq_fig)
            # dynaGUI.mi_tq.addWidget(self.midde_tq_fig)
            dynaGUI.lo_tq.addWidget(self.lower_tq_fig)
            dynaGUI.upper_posi_Lay.addWidget(self.Up_AP_figure)
            # dynaGUI.middl_posi_Lay.addWidget(self.Mi_AP_figure)
            dynaGUI.lower_posi_Lay.addWidget(self.Lo_AP_figure)

            self._Up_AV = self.Up_AV_figure.figure.subplots()
            self._Up_AA = self.Up_AA_figure.figure.subplots()
            # self._Mi_AV = self.Mi_AV_figure.figure.subplots()
            # self._Mi_AA = self.Mi_AA_figure.figure.subplots()
            self._Lo_AV = self.Lo_AV_figure.figure.subplots()
            self._Lo_AA = self.Lo_AA_figure.figure.subplots()
            self._up_tq_ax = self.upper_tq_fig.figure.subplots()
            # self._mi_tq_ax = self.midde_tq_fig.figure.subplots()
            self._lo_tq_ax = self.lower_tq_fig.figure.subplots()
            self._Up_AP_ax = self.Up_AP_figure.figure.subplots()
            # self._Mi_AP_ax = self.Mi_AP_figure.figure.subplots()
            self._Lo_AP_ax = self.Lo_AP_figure.figure.subplots()


    def qtgraph_update(self):
        for index, data, data_type, lab, qtgraph in zip(self.tab_indexs, self.Data_list, self.data_types, self._labs_list, self.qtgraph_lines):
            if self.UI.tabWidget.currentIndex() == index or self.show_force:
                qtgraph.setData(data)
                # i.plot(data, clear=True)
                # temp_new_data = np.array([i for i in data if not i in old_data])
                if data_type in ['p','v','t']:
                    lab[0].setText(
                        "max = {0:4f}".format(max(data))
                    )
                    lab[1].setText(
                        "min = {0:4f}".format(min(data))
                    )
                elif data_type in ['a']:
                    if (sum(data[data>0]) == 0) :
                        lab[0].setText("pos_ave = {0:4f}".format(0.00))
                    else :
                        lab[0].setText(
                            "pos_ave = {0:4f}".format(np.average(data[data>0]))
                        )
                    if (sum(data[data<0]) == 0) :
                        lab[1].setText("nav_ave = {0:4f}".format(0.00))
                    else:
                        lab[1].setText(
                            "nav_ave = {0:4f}".format(np.average(data[data<0]))
                        )

    def update_data(self, foot_model, numberOfPoint=0, show_force=False, cycle_finished=False):
        self.foot_model = foot_model
        self.numberOfPoint = numberOfPoint
        self.show_force = show_force
        self.Data_list = [
                foot_model.upper_motor.velocity_log,
                foot_model.upper_motor.acc_log,
                # foot_model.Middle_Motor.velocity_log,
                # foot_model.Middle_Motor.acc_log,
                foot_model.lower_motor.velocity_log,
                foot_model.lower_motor.acc_log,
                foot_model.upper_motor.torque_log,
                # foot_model.Middle_Motor.torque_log,
                foot_model.lower_motor.torque_log,
                np.rad2deg(foot_model.upper_motor.position_log),
                # np.rad2deg(foot_model.Middle_Motor.position_log),
                np.rad2deg(foot_model.lower_motor.position_log)
                # np.rad2deg(foot_model.Middle_Motor.position_log) - 180,
                # np.rad2deg(foot_model.lower_motor.position_log) - 180     # Leave it just in case
            ]
        self.timeBase = np.arange(-foot_model.log_len, 0)
        self.timeBase = self.timeBase * self.dt




    # def UpdatePlot(self, foot_model, show_force=False):
        ########### Modified by Clement #########
    def UpdatePlot(self, foot_model, numberOfPoint=0, show_force=False, version=0, dynamic_plot_exec_times=0):

        if self.UI.window.isVisible():
        ###################################
        # if self.UI.window.isVisible():
            tab_indexs = [
                0, 1, 1,
                # 0, 1, 1,
                0, 1, 1,
                2, 2
            ]

            ax_list = [
                self._Up_AP_ax, self._Up_AV, self._Up_AA,
                # self._Mi_AP_ax, self._Mi_AV, self._Mi_AA,
                self._Lo_AP_ax, self._Lo_AV, self._Lo_AA,
                self._up_tq_ax, self._lo_tq_ax
            ]

            Data_list = [
                np.rad2deg(foot_model.upper_motor.position_log),
                foot_model.upper_motor.velocity_log,
                foot_model.upper_motor.acc_log,
                # np.rad2deg(foot_model.Middle_Motor.position_log) - 180,
                # foot_model.Middle_Motor.velocity_log,
                # foot_model.Middle_Motor.acc_log,
                np.rad2deg(foot_model.lower_motor.position_log) - 180,
                foot_model.lower_motor.velocity_log,
                foot_model.lower_motor.acc_log,
                foot_model.upper_motor.torque_log,
                # foot_model.Middle_Motor.torque_log,
                foot_model.lower_motor.torque_log
            ]

            data_types = [
                'p', 'v', 'a',
                'p', 'v', 'a',
                'p', 'v', 'a',
                't', 't', 't'
            ]

            labs = [
                [self.UI.u_maxp, self.UI.u_minp],
                [self.UI.u_maxv, self.UI.u_minv, self.UI.u_absv_min],
                [self.UI.u_maxa, self.UI.u_mina],
                [self.UI.m_maxp, self.UI.m_minp],
                [self.UI.m_maxv, self.UI.m_minv, self.UI.m_absv_min],
                [self.UI.m_maxa, self.UI.m_mina],
                [self.UI.l_maxp, self.UI.l_minp],
                [self.UI.l_maxv, self.UI.l_minv, self.UI.l_absv_min],
                [self.UI.l_maxa, self.UI.l_mina],
                [self.UI.u_maxt, self.UI.u_mint],
                [self.UI.m_maxt, self.UI.m_mint],
                [self.UI.l_maxt, self.UI.l_mint],
            ]
            self.timeBase = np.arange(-foot_model.log_len,0)
            self.timeBase = self.timeBase * self.dt

            for index, ax, data, data_type, lab in zip(tab_indexs, ax_list, Data_list, data_types, labs):
                if self.UI.tabWidget.currentIndex() == index or show_force:
                    ax.clear()
                    ax.plot(self.timeBase[1:],data[1:])
                    ############### Modified by Clement ############
                    ax.axvline(x=(-dynamic_plot_exec_times * self.dt), color='r', linewidth=1)
                    # if version in [1, 2]:
                    #     # ax.axvline(x=(-dynamic_plot_exec_times)* self.dt, color='r', linewidth=1)
                    #     ax.axvline(x=(-dynamic_plot_exec_times), color='r', linewidth=1)
                    #     ax.axvline(x=-len(self.timeBase)* self.dt, color='r', linewidth=1)
                    ###############################################
                    ax.figure.canvas.draw()
                    if data_type in ['p','t']:
                        lab[0].setText(
                            "max = {0:4f}".format(max(data))
                        )
                        lab[1].setText(
                            "min = {0:4f}".format(min(data))
                        )
                    elif data_type in ['a']:
                        if (sum(data[data>0]) == 0) :
                            lab[0].setText("pos_ave = {0:4f}".format(0.00))
                        else :
                            lab[0].setText(
                                "pos_ave = {0:4f}".format(np.average(data[data>0]))
                            )
                        if (sum(data[data<0]) == 0) :
                            lab[1].setText("nav_ave = {0:4f}".format(0.00))
                        else:
                            lab[1].setText(
                                "nav_ave = {0:4f}".format(np.average(data[data<0]))
                            )
                    elif data_type in ['v']:
                        lab[0].setText(
                            "max = {0:4f}".format(max(data))
                        )
                        lab[1].setText(
                            "min = {0:4f}".format(min(data))
                        )
                        lab[2].setText(
                            f"abs min = {min(abs(data)):6f}"
                        )


    def changeTimeBase(self, dt):
        self.dt = dt


class TSNE():
    
    def __init__(self, layout):
        self.dynamic_canvas = FigureCanvas(Figure())
        self._dynamic_ax = self.dynamic_canvas.figure.subplots()
        layout.addWidget(self.dynamic_canvas)
    
    def UpdatePlot(self, data):
        if data is not None:
            self._dynamic_ax.clear()
            # self._dynamic_ax.set_xlim([-10,10])
            # self._dynamic_ax.set_ylim([-10,10])
            # self._dynamic_ax.set_aspect('equal', 'box')
            self._dynamic_ax.scatter(data.x, data.y, c = data.data, s = 10)
            self._dynamic_ax.figure.canvas.draw()

class Loss():

    def __init__(self, layout, PYQTGRAPH_ON=False):
        self.PYQTGRAPH_ON = PYQTGRAPH_ON
        if PYQTGRAPH_ON:
            self.Pens = Pens()
            self.loss_widget = pyqtgraph.PlotWidget(background="w")
            self.loss_plot = self.loss_widget.plot(pen=pyqtgraph.mkPen(color=[0, 0, 255], width=1.5))
            layout.addWidget(self.loss_widget)
        else:
            self.dynamic_canvas = FigureCanvas(Figure())
            self._dynamic_ax = self.dynamic_canvas.figure.subplots()
            layout.addWidget(self.dynamic_canvas)

    def UpdatePlot(self, data):
        if self.PYQTGRAPH_ON:
            self.loss_plot.setData(*data)
        else:
            self._dynamic_ax.clear()
            self._dynamic_ax.plot(*data)
            self._dynamic_ax.figure.canvas.draw()

class Gait():
    
    def __init__(self, layout, needTrace = False):
        self.static_canvas = FigureCanvas(Figure())
        self._static_ax = self.static_canvas.figure.subplots()
        self.ground_line, = self._static_ax.plot([], [])
        self.needTrace = needTrace
        self.middl_trace = [list(), list()]
        self.lower_trace = [list(), list()]
        layout.addWidget(self.static_canvas)
        self.layout = layout

    def poltGait(self, LineX, LineY, need_repaint=False):
        self._static_ax.set_aspect("equal")
        self._static_ax.plot(LineX, LineY, 'b')
        self._static_ax.figure.canvas.draw()
        if need_repaint:
            self.static_canvas.repaint()

    def plotGround(self, ground_x_recorder, ground_y_recorder, PYQTGRAPH_ON):
        if PYQTGRAPH_ON:
            pass
        else:
            self.ground_line = self._static_ax.plot(ground_x_recorder, ground_y_recorder, 'k')

    def plotTrace(self, need_repaint=False):
        if len(self.middl_trace[0]) != 0: # Has value
            self._static_ax.plot(self.middl_trace[0], self.middl_trace[1] ,'r')
            self._static_ax.plot(self.lower_trace[0], self.lower_trace[1] ,'r')
            self._static_ax.figure.canvas.draw()
            if need_repaint:
                self.static_canvas.repaint()

    def plotLable(self, gait_name, need_repaint = False):
        self._static_ax.set_title(gait_name)
        self._static_ax.set_xlabel('x position(cm)')
        self._static_ax.set_ylabel('y position(cm)')
        self._static_ax.figure.canvas.draw()
        if need_repaint:
            self.static_canvas.repaint()

    def clearOld(self):
        self._static_ax.clear()
        self.middl_trace = [list(), list()]
        self.lower_trace = [list(), list()]


class FootDiagramThread(threading.Thread):
    def __init__(self, diagram, foot_model, data, v_cmd, need_repaint=False, PYQTGRAPH_ON=False):
        super().__init__(daemon=True)
        self.diagram = diagram
        self.foot_model = foot_model
        self.data = data
        self.need_repaint = need_repaint
        self.PYQTGRAPH_ON = PYQTGRAPH_ON
        self.v_cmd = v_cmd


    def run(self):
        if self.PYQTGRAPH_ON:
            self.diagram.update_data(self.foot_model, self.data, self.v_cmd)
            if self.diagram.need_trace: self.diagram.plotTrace(foot_model=self.foot_model)
            if self.diagram.need_ground: self.diagram.plotGround(ground_info=self.data.ground_info)
            if self.diagram.need_obstacle: self.diagram.plotObstacle(obstacle_info=self.data.obstacle_info)
            if self.diagram.need_target: self.diagram.plotTarget(target=self.data.target)
            self.diagram.plotFoot(foot_model=self.foot_model, expect_foot=None)
        else:
            try:
                self.diagram.target = self.data.target
                self.diagram.under_learning = self.data.under_learning
                self.diagram.real_time = self.data.real_time
                try:
                    if self.data.tsne_info[0][2] is None:
                        self.diagram.fail = True
                    else:
                        self.diagram.fail = False
                except TypeError:
                    self.diagram.fail = False

                self.diagram.UpdatePlot(self.foot_model, ground_info=self.data.ground_info)

                if self.need_repaint:
                    self.diagram.dynamic_canvas.repaint()
            except:
                traceback.print_exc()
            # print(e)




class DynamicLogThread(threading.Thread):
    def __init__(self, diagram, foot_model, numberOfPoint, version=0, dynamic_plot_exec_times=0, PYQTGRAPH_ON=False):

        threading.Thread.__init__(self)
        self.diagram = diagram
        self.foot_model = foot_model
        self.numberOfPoint = numberOfPoint
        self.version = version
        self.dynamic_plot_exec_times = dynamic_plot_exec_times
        self.PYQTGRAPH_ON = PYQTGRAPH_ON

    def run(self):
        if self.PYQTGRAPH_ON:
            self.diagram.update_data(self.foot_model, self.numberOfPoint)
            self.diagram.qtgraph_update()
        else:
            self.diagram.UpdatePlot(self.foot_model, self.numberOfPoint,
                version=self.version, 
                dynamic_plot_exec_times=self.dynamic_plot_exec_times
            )


class TSNEPlotingThread(threading.Thread):
    def __init__(self, diagram, tsne_data):
        threading.Thread.__init__(self)
        self.diagram = diagram
        self.tsne_data = tsne_data
    
    def run(self):
        self.diagram.UpdatePlot(self.tsne_data)

class LossPlotingThread(threading.Thread):
    def __init__(self, diagram, loss_data):
        threading.Thread.__init__(self)
        self.diagram = diagram
        self.loss_data = loss_data
    
    def run(self):
        self.diagram.UpdatePlot(self.loss_data)

class GaitPlotingThread(threading.Thread):
    def __init__(self, diagram, foot_model, gait, each_target_times, target_log ,discount_factor,
        PYQTGRAPH_ON=False,
        show_ground=False,
        ground_x_recorder=[],
        ground_y_recorder=[],
    ):
        threading.Thread.__init__(self)
        self.diagram = diagram
        self.foot = foot_model
        self.gait = gait
        self.each_target_times = each_target_times
        self.discount_factor = discount_factor
        self.target_log = target_log
        self.PYQTGRAPH_ON = PYQTGRAPH_ON
        self.show_ground = show_ground
        self.ground_x_recorder = ground_x_recorder
        self.ground_y_recorder = ground_y_recorder

    def run(self):
        self.diagram.clearOld()
        dumy_foot = getattr(robot_leg_model, self.foot.__class__.__name__)()
        # dumy_foot = robot_leg_model.Foot14_14_7_Real()

        if self.gait.ground_info is not None and self.show_ground:
            self.diagram.plotGround(self.ground_x_recorder, self.ground_y_recorder, self.PYQTGRAPH_ON)

        for index in range(self.foot.log_len):
            if index % (self.each_target_times*self.discount_factor) == 0:
                dumy_foot.excu_position_cmd(
                    [
                        np.rad2deg(self.foot.upper_motor.position_log[index]),
                        # np.rad2deg(self.foot.Middle_Motor.position_log[index]),
                        np.rad2deg(self.foot.lower_motor.position_log[index])
                    ],
                    needLog=False, offset= False
                )
                LineX = np.array([dumy_foot.upper_motor_pos[0],
                    # dumy_foot.Mid_Moter_Pos[0],
                    dumy_foot.lower_motor_pos[0],
                    dumy_foot.EndPoint[0]])
                LineY = np.array([dumy_foot.upper_motor_pos[1],
                    # dumy_foot.Mid_Moter_Pos[1],
                    dumy_foot.lower_motor_pos[1],
                    dumy_foot.EndPoint[1]])

                # self.diagram.middl_trace[0].append(dumy_foot.Mid_Moter_Pos[0])
                # self.diagram.middl_trace[1].append(dumy_foot.Mid_Moter_Pos[1])

                self.diagram.lower_trace[0].append(dumy_foot.lower_motor_pos[0])
                self.diagram.lower_trace[1].append(dumy_foot.lower_motor_pos[1])

                self.diagram.poltGait(LineX, LineY,need_repaint=False)
                time.sleep(self.gait.dt)
                dumy_foot.upper_motor_pos[0] = self.target_log[index]*self.gait.Ground_Step

        self.diagram.plotLable(self.gait.__class__.__name__)
        if self.diagram.needTrace:
            self.diagram.plotTrace()

class ActionInfoThread(threading.Thread):
    def __init__(self, ActionInfo):
        super().__init__(daemon=True)
        self.ACTION_INFO = ActionInfo


    def run(self):
        self.ACTION_INFO.updatePlot()



