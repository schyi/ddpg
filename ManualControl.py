from PyQt5 import QtWidgets, QtCore, QtGui
import sys, os
import socket
import numpy as np
import threading
import robot_leg_model as RFModel
import quadruped_robot_controller
import UI.manualControlUI as mcUI
import UI.dynamicdataUI as dyUI
import UI.gaitplotUI as gaUI
import UI.needsaveUI as saUI
import math
import gait
import quadruped_robot_gait
import FootPlot
import time
import datetime
import scipy.io as sio


class App(object):
    def __init__(self):
        #Initialize Model
        self.bool_real_motor = False
        self.bool_motor_armed = False
        self.quadruped_robot = quadruped_robot_controller.QuadrupedRobotController()
        self.default_log_len = 2000
        self.quadruped_robot.resetLoglength(self.default_log_len)
        self.data_counter = 0
        self.runing_Flag = False
        self.GaitRunning = False
        self.monitoring_real_motor = False
        self.obstacle_mode = False
        self.dynamic = True
        self.bool_real_motor_pos = False

        self.bool_has_end_gait = False
        self.bool_has_init_gait = False

        self.gait_speed_up = 1
        self.target = [0,0]
        self.obstacle_info = None
        self.last_run_type = None
        
        self.gait_counter = 0
        self.gait_init_counter = 0
        self.gait_end_counter = 0
        self.test_counter = 0

        #Initialize GUI
        if socket.gethostname() == 'ERIC-PERSONAL':
            os.environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
            appl = QtWidgets.QApplication(sys.argv)
            appl.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling)
        else:
            appl = QtWidgets.QApplication(sys.argv)

        self.GUI = ManualUI()
        self.dynamicLogUI = DynamicLogUI()
        self.gaitGUI = GaitUI()

        self.gait = None
        # gait_names = [
        #     "up_down",
        #     "up_down_out",
        #     "trot_forward",
        #     "trot_rotate_right"
        # ]
        self.gait_dict = {
            "up_down":quadruped_robot_gait.TrotStandingInPlaceGait,
            "up_down_out":quadruped_robot_gait.TrotOutwardStandingInPlaceGait,
            "trot_forward":quadruped_robot_gait.TrotForwardGait,
            "trot_backward":quadruped_robot_gait.TrotBackwardGait,
            "trot_rotate_right":quadruped_robot_gait.TrotRotateRightGait,
            "trot_smooth_rotate_right":quadruped_robot_gait.TrotSmoothRotateRightGait,
            "trot_rotate_left":quadruped_robot_gait.TrotRotateLeftGait,
            "trot_smooth_rotate_left":quadruped_robot_gait.TrotSmoothRotateLeftGait,
            "trot_move_right":quadruped_robot_gait.TrotMoveRightGait,
        }
        for names in self.gait_dict:
            self.GUI.gait_combo.addItem(names)
        self.GUI.device_name_lned.setText("com14")
        #Matplotlib
        # fl : front_left
        # fr : front_right
        # rl : rear_left
        # rr : rear_right
        self.foot_fl_widget = FootPlot.FootWidget(self.GUI.MPLib_front)
        self.foot_fl_diagram = FootPlot.Foot(
            plot_widgets=self.foot_fl_widget.returnPlotWidget(),
            need_ground=True,
            need_trace=True,
            need_target=True,
        )
        self.foot_fr_widget = FootPlot.FootWidget(self.GUI.MPLib_front)
        self.foot_fr_diagram = FootPlot.Foot(
            plot_widgets=self.foot_fr_widget.returnPlotWidget(),
            need_ground=True,
            need_trace=True,
            need_target=True,
        )
        self.foot_rl_widget = FootPlot.FootWidget(self.GUI.MPLib_rear)
        self.foot_rl_diagram = FootPlot.Foot(
            plot_widgets=self.foot_rl_widget.returnPlotWidget(),
            need_ground=True,
            need_trace=True,
            need_target=True,
        )
        self.foot_rr_widget = FootPlot.FootWidget(self.GUI.MPLib_rear)
        self.foot_rr_diagram = FootPlot.Foot(
            plot_widgets=self.foot_rr_widget.returnPlotWidget(),
            need_ground=True,
            need_trace=True,
            need_target=True,
        )

        self.gait_diagram = FootPlot.Gait(self.gaitGUI.gait_plot_Lay)
        self.dynamic_digram = FootPlot.Dynamic(0.1, self.dynamicLogUI, PYQTGRAPH_ON=True, foot_model=self.quadruped_robot.leg_rf)

        #UI mapping
        self.GUI.StartStopBTN.clicked.connect(self.StartStop)
        self.GUI.AngUp.sliderReleased.connect(self.modifyModel)
        self.GUI.AngDown.sliderReleased.connect(self.modifyModel)
        self.GUI.QuitBTN.clicked.connect(self.ExitApp)
        self.GUI.CmdbyIK_BTN.clicked.connect(self.useIK)
        self.GUI.X_Cmd_in.returnPressed.connect(self.updateTarget)
        self.GUI.Y_Cmd_in.returnPressed.connect(self.updateTarget)
        self.GUI.GaitSt_BTN.clicked.connect(self.gaitBtn)
        self.GUI.Dynamic.toggled.connect(self.dynamicState)
        self.GUI.Static.toggled.connect(self.dynamicState)
        self.GUI.dynamic_data.clicked.connect(self.dynamicDataShow)
        self.GUI.activateRM_Btn.clicked.connect(self.activateRealMotor)
        self.GUI.real_monitor_Btn.clicked.connect(self.monitoringRealMotor)
        self.GUI.arm_motor_Btn.clicked.connect(self.armRealMotor)
        self.GUI.with_offset_rb.clicked.connect(self.switchOffset)
        self.GUI.SpeedUpIn.valueChanged.connect(self.speedUP)
        self.GUI.SaveDataBtn.clicked.connect(self.saveToCsv)
        self.GUI.PlotGaitBtn.clicked.connect(lambda: self.gaitGUI.window.show())
        self.GUI.position_ik_rbtn.clicked.connect(self.switchOpMode)
        self.GUI.velocity_ik_rbtn.clicked.connect(self.switchOpMode)
        self.GUI.force_calculate.clicked.connect(self.dynamic_test)
        self.GUI.btn_set_log_level.clicked.connect(self.setLogLevel)
        self.GUI.btn_set_hip_pid.clicked.connect(self.btnSetHipPIDClicked)
        self.GUI.btn_set_leg_pid.clicked.connect(self.btnSetLegPIDClicked)
        self.gaitGUI.start_gait_Btn.clicked.connect(self.plotGlobalGaitGrapth)
        self.gaitGUI.PlotTraceBtn.clicked.connect(self.plotGlobalGaitTrace)


        #Timers
        self.real_motor_timer = QtCore.QTimer()
        self.real_motor_timer.timeout.connect(self.updateRealModel)
        self.plot_foot_timer = QtCore.QTimer()
        self.plot_foot_timer.timeout.connect(self._plotAllFoot)
        self.plot_foot_timer.setInterval(30)

        #Threads
        self.gait_thread = None


        self.initialDynamicTable()
        # self.initialPos()
        self.InitialGait()
        self.Modi_sidebar()

        self._plotAllFoot()


        self.GUI.window.show()
        sys.exit(appl.exec_())

    def activateRealMotor(self):
        if RFModel.HAS_DXL_SDK:
            if not self.quadruped_robot.bool_dxl_connected:
                device_name = self.GUI.device_name_lned.text()
                self.quadruped_robot.activateAllRealMotor(device_name)
                if self.quadruped_robot.bool_real_motor_mode:
                    self.bool_real_motor = True
                    self.quadruped_robot.disarmAllMotor()
                    self.quadruped_robot.switchMotorMode("position")
                    self.plot_foot_timer.start()
                    self.GUI.real_motor_status_Lb.setText("Activated")
                    self.GUI.spin_leg_pid_p.setValue(self.quadruped_robot.leg_lf.upper_motor.real_motor.KP)
                    self.GUI.spin_leg_pid_i.setValue(self.quadruped_robot.leg_lf.upper_motor.real_motor.KI)
                    self.GUI.spin_leg_pid_d.setValue(self.quadruped_robot.leg_lf.upper_motor.real_motor.KD)
                    self.GUI.spin_hip_pid_p.setValue(self.quadruped_robot.leg_lf.hip_motor.real_motor.KP)
                    self.GUI.spin_hip_pid_i.setValue(self.quadruped_robot.leg_lf.hip_motor.real_motor.KI)
                    self.GUI.spin_hip_pid_d.setValue(self.quadruped_robot.leg_lf.hip_motor.real_motor.KD)
                    self.Modi_sidebar()
                else:
                    self.GUI.statusbar.showMessage("Device Error", 2000)
            else:
                if self.bool_motor_armed:
                    self.armRealMotor()
                self.quadruped_robot.deactivateDXLConnection()
                self.plot_foot_timer.stop()
                self.bool_real_motor = False
                self.GUI.real_motor_status_Lb.setText("Deactivated")
        elif not RFModel.HAS_DXL_SDK:
            self.GUI.statusbar.showMessage("SDK Not Installed!!!", 2000)

    def armRealMotor(self):
        if self.bool_real_motor:
            self.bool_motor_armed = (not self.bool_motor_armed)
            if self.bool_motor_armed:
                if not self.monitoring_real_motor:
                    self.quadruped_robot.armAllMotor()
                    self.quadruped_robot.standupFromGround()
                    self.Modi_sidebar()
                    self.GUI.real_motor_status_Lb.setText("Armed")
                    self.GUI.arm_motor_Btn.setText("Disarm Motor")
                    self.GUI.arm_motor_Btn.repaint()
                else:
                    self.GUI.statusbar.showMessage("Stop Monitor Before Arm", 1000)
            else:
                self.quadruped_robot.sitdownFromStand()
                self.quadruped_robot.disarmAllMotor()
                self.GUI.real_motor_status_Lb.setText("Activated")
                self.GUI.arm_motor_Btn.setText("Arm Motor")
                self.GUI.arm_motor_Btn.repaint()

    def btnSetLegPIDClicked(self):
        self.quadruped_robot.setLegPID(
            self.GUI.spin_leg_pid_p.value(),
            self.GUI.spin_leg_pid_i.value(),
            self.GUI.spin_leg_pid_d.value()
        )

    def btnSetHipPIDClicked(self):
        self.quadruped_robot.setHipPID(
            self.GUI.spin_hip_pid_p.value(),
            self.GUI.spin_hip_pid_i.value(),
            self.GUI.spin_hip_pid_d.value()
        )



    def dynamic_test(self):
        #For manual input testing
        # for i, motor in enumerate([self.quadruped_robot.upper_motor, self.quadruped_robot.Middle_Motor, self.quadruped_robot.lower_motor]):
        for i, motor in enumerate([self.quadruped_robot.leg_lf.upper_motor, self.quadruped_robot.leg_lf.lower_motor]):
            try:
                if "-->" not in self.GUI.dynamic_value.item(i*3  , 0).text():
                    motor.pos_override(float(self.GUI.dynamic_value.item(i*3  , 0).text()))
                else:
                    motor.pos_override(float(self.GUI.dynamic_value.item(i*3  , 0).text()[:-10]))
                motor.vel_override(float(self.GUI.dynamic_value.item(i*3+1, 0).text()))
                motor.acc_override(float(self.GUI.dynamic_value.item(i*3+2, 0).text()))
            except ValueError:
                self.GUI.statusbar.showMessage("Input Value Error", 2000)
                return
        F_x = float(self.GUI.dynamic_value.item( 9,0).text())
        F_y = float(self.GUI.dynamic_value.item(10,0).text())
        
        self.quadruped_robot.endPointCalculation()

    
    def dynamicDataShow(self):
        if self.dynamicLogUI.window.isVisible() is not True:
            self.dynamicLogUI.window.show()
            if hasattr(self, "dynamic_digram"):
                self.dynamic_digram.update_data(self.quadruped_robot.leg_lf, show_force=True)
                self.dynamic_digram.qtgraph_update()

    def dynamicState(self):
        if self.GUI.Dynamic.isChecked():
            self.dynamic = True
        else:
            self.dynamic = False

    def ExcuGait(self):
        cycle_passed = False
        if self.runing_Flag == True:
            #Speed Up Function only work in simulation
            if self.bool_has_init_gait and self.init_gait_cycle > 0:
                lf_cmd, rf_cmd, lh_cmd, rh_cmd, lf_pos, rf_pos, lh_pos, rh_pos, cycle_passed = \
                    self.gait.initGaitLookUp(self.gait_init_counter)
                self.gait_init_counter += 1
                if self.gait_init_counter == self.gait.number_of_init_point:
                    self.init_gait_cycle = 0
            else:
                if self.cycle > 0:
                    if not self.bool_real_motor:
                        for _ in range(self.gait_speed_up):
                            lf_cmd, rf_cmd, lh_cmd, rh_cmd, lf_pos, rf_pos, lh_pos, rh_pos, cycle_passed = \
                                self.gait.gaitLookUp(self.gait_counter)
                            self.gait_counter += 1
                            if cycle_passed:
                                self.cycle -= 1
                    else:
                        lf_cmd, rf_cmd, lh_cmd, rh_cmd, lf_pos, rf_pos, lh_pos, rh_pos, cycle_passed = \
                            self.gait.gaitLookUp(self.gait_counter)
                        self.gait_counter += 1
                        if cycle_passed:
                            self.cycle -= 1
                else:
                    if self.bool_has_end_gait:
                        lf_cmd, rf_cmd, lh_cmd, rh_cmd, lf_pos, rf_pos, lh_pos, rh_pos, cycle_passed = \
                            self.gait.endGaitLookUp(self.gait_end_counter)
                        self.gait_end_counter += 1
                        if self.gait_end_counter == self.gait.number_of_end_point:
                            self.end_gait_cycle = 0


            # self.quadruped_robot.executeAllIK(cmd_x, cmd_y, self.gait.dt)
            self.quadruped_robot.executeIK(
                lf_cmd, rf_cmd, lh_cmd, rh_cmd, lf_pos, rf_pos, lh_pos, rh_pos, self.GUI.spin_dt.value()/1000, need_delay=False
            )
            self.IK_count += 1
            if self.IK_count%10 == 0:
                self.dynamic_digram.update_data(self.quadruped_robot.leg_rf, 500)
                self.dynamic_digram.qtgraph_update()
            if not self.bool_real_motor:
                self._plotAllFoot()


            self.GUI.PosX.display(round(self.quadruped_robot.leg_lf.EndPoint[0],2))
            self.GUI.PosY.display(round(self.quadruped_robot.leg_lf.EndPoint[1],2))
            
            self.Modi_sidebar()

            if self.cycle <= 0 and self.end_gait_cycle <= 0:
                self.GaitTimer.stop()
                self.GaitRunning = False
                # if self.bool_real_motor:
                #     time.sleep(0.2)
                self.dynamic_digram.update_data(self.quadruped_robot.leg_rf, 500)
                self.dynamic_digram.qtgraph_update()
                if self.gait_speed_up != 1:
                    self.quadruped_robot.cutOffLog(self.IK_count)


    def ExitApp(self):
        if self.bool_real_motor:
            if self.bool_motor_armed:
                self.armRealMotor()
            self.quadruped_robot.deactivateDXLConnection()
            self.plot_foot_timer.stop()
            time.sleep(0.05)
        sys.exit()

    def gaitBtn(self):
        if not self.GaitRunning or not hasattr(self, "gait"):
            self.InitialGait()
            if self.gait is not None:
                # self.GaitTimer.setInterval(round(self.gait.dt*1000))
                self.GaitRunning = True #change before initialPos to avoid error
                self.quadruped_robot.slowlyPosition(
                        lf_cmd=self.gait.start_point_lf,
                        rf_cmd=self.gait.start_point_rf,
                        lh_cmd=self.gait.start_point_lh,
                        rh_cmd=self.gait.start_point_rh,
                        lf_pos=self.gait.start_point_hip_lf,
                        rf_pos=self.gait.start_point_hip_rf,
                        lh_pos=self.gait.start_point_hip_lh,
                        rh_pos=self.gait.start_point_hip_rh,
                    )
                self.quadruped_robot.resetLog()
                self.GaitTimer.start()
                self.runing_Flag = True
                self.GUI.StartStopBTN.setText("Pause")
                self.GUI.StartStopBTN.repaint()
        else:
            self.resetCounter()
            self.ModifyGait()
            self.GaitRunning = True
            self.quadruped_robot.slowlyPosition(
                lf_cmd=self.gait.start_point_lf,
                rf_cmd=self.gait.start_point_rf,
                lh_cmd=self.gait.start_point_lh,
                rh_cmd=self.gait.start_point_rh,
                lf_pos=self.gait.start_point_hip_lf,
                rf_pos=self.gait.start_point_hip_rf,
                lh_pos=self.gait.start_point_hip_lh,
                rh_pos=self.gait.start_point_hip_rh,
            )
            self.quadruped_robot.resetLog()
            self.GaitTimer.start()
            self.runing_Flag = True
            self.GUI.StartStopBTN.setText("Pause")
            self.GUI.StartStopBTN.repaint()


    def InitialGait(self):
        StepH = float(self.GUI.stepH_input.text())
        StepL = float(self.GUI.stepL_input.text())
        GLevel = float(self.GUI.GLevel_input.text())
        resolution = int(self.GUI.resolution_input.text())
        phase = int(self.GUI.spin_phase.value())
        origin = self.GUI.spin_origin.value()
        dt = self.GUI.spin_dt.value()/1000

        self.gait = self.gait_dict[self.GUI.gait_combo.currentText()](
            GLevel, StepL, resolution,
            height=StepH,
            phase=phase,
            origin=origin,
            dt=dt
        )

        self.dynamic_digram.changeTimeBase(self.gait.dt)
        # if self.quadruped_robot.leg_lf.checkGaitValid(self.gait):
        if True:
            self.cycle = int(self.GUI.cycle_input.text()) + 1
            self.cycle_pass = 0
            self.total_gait_number = int(self.GUI.resolution_input.text())*int(self.GUI.cycle_input.text())
            self.gait_resolution = int(self.GUI.resolution_input.text())
            self.quadruped_robot.resetLoglength(self.gait.number_of_gait_point * (self.cycle-1))
            self.gait_counter = 0
            self.IK_count = 0
            self.GaitTimer = QtCore.QTimer()
            self.GaitTimer.setInterval(round(self.gait.dt * 1000))
            self.GaitTimer.timeout.connect(self.ExcuGait)
            self.GUI.statusbar.showMessage("gait Valid",1000)
            if self.gait.bool_has_init_gait:
                self.bool_has_init_gait = True
                self.init_gait_cycle = 2
                self.gait_init_counter = 0
            else:
                self.bool_has_init_gait = False
                self.init_gait_cycle = 0
            if self.gait.bool_has_end_gait:
                self.bool_has_end_gait = True
                self.end_gait_cycle = 2
                self.gait_end_counter = 0
            else:
                self.bool_has_end_gait = False
                self.end_gait_cycle = 0

        else:
            self.GUI.statusbar.showMessage("Warning: gait Invalid",1000)
            self.gait = None


    def initialPos(self, Cmd_X=0, Cmd_Y=-30):
        path = zip(
            np.linspace(self.quadruped_robot.leg_rf.EndPoint[0], Cmd_X, num=100).tolist(),
            np.linspace(self.quadruped_robot.leg_rf.EndPoint[1], Cmd_Y, num=100).tolist()
        )
        for x, y in path:
            if not self.quadruped_robot.leg_lf.checkWorkingSpace(x, y):
                t_old = time.time()
                self.quadruped_robot.executeAllIK(x, y)
                self.quadruped_robot.endPointCalculation()
                self.GUI.PosX.display(round(self.quadruped_robot.leg_lf.EndPoint[0], 2))
                self.GUI.PosY.display(round(self.quadruped_robot.leg_lf.EndPoint[1], 2))
                if not self.bool_real_motor: self._plotAllFoot()

                self.Modi_sidebar()
                if self.bool_real_motor:
                    while time.time() - t_old < 0.05: pass
            else:
                self.GUI.statusbar.showMessage("Out Space",1000)

    def ModifyGait(self):
        if self.GaitRunning:
            self.GaitTimer.stop()
            self.GaitRunning = False
            Ori_Gait = self.gait
            self.InitialGait()
            if self.gait is None: self.gait = Ori_Gait
        else:
            self.InitialGait()

    def monitoringRealMotor(self):
        if not self.GaitRunning and not self.bool_motor_armed and self.bool_real_motor:
            self.GUI.real_motor_status_Lb.setText("Monitoring")
            self.real_motor_timer.setInterval(100)
            self.real_motor_timer.start()
            self.monitoring_real_motor = True
        elif self.bool_motor_armed:
            self.GUI.statusbar.showMessage("Disarm to activate monitor", 1000)
        elif self.GaitRunning:
            self.GUI.statusbar.showMessage("gait is still running", 1000)
        elif not self.bool_real_motor:
            self.GUI.statusbar.showMessage("Activate real motor first", 1000)
        else:
            pass



    def Modi_sidebar(self):
        AngList = [self.quadruped_robot.leg_lf.upper_motor.getPosition("deg", return_global_angle=False),
                 self.quadruped_robot.leg_lf.lower_motor.getPosition("deg", return_global_angle=False)]
        self.GUI.AngUp.setValue(round(AngList[0]))

        self.GUI.AngDown.setValue(round(AngList[1]))
        for slider in [self.GUI.AngUp, self.GUI.AngDown]:
            slider.repaint()

    def modifyModel(self):
        if self.runing_Flag == True and self.GaitRunning == False:
            cmd = [
                self.GUI.AngUp.value(),
                self.GUI.AngDown.value() - 180
            ]

            if self.quadruped_robot.str_motor_operating_mode == 'p':
                self.quadruped_robot.executePosCmd(foot_position=cmd)

            self.GUI.PosX.display(round(self.quadruped_robot.leg_lf.EndPoint[0],2))
            self.GUI.PosY.display(round(self.quadruped_robot.leg_lf.EndPoint[1],2))
            self.GUI.upp_ang_ledit.setText("{0:03.4f}".format(self.quadruped_robot.leg_lf.upper_motor.getPosition(return_global_angle=False)))
            self.GUI.low_ang_ledit.setText("{0:03.4f}".format(self.quadruped_robot.leg_lf.lower_motor.getPosition(return_global_angle=False)))
            if not self.bool_real_motor: self._plotAllFoot()

    def plotGlobalGaitGrapth(self):
        if not self.GaitRunning and self.gait is not None:
            if self.gait_thread is not None:
                if self.gait_thread.is_alive():
                    return
                else:
                    # When manual mode each target time is 1
                    moving_index = np.arange(self.quadruped_robot.log_len) * self.gait_speed_up
                    # moving_index is for robot moving for how many gait.Ground_step
                    self.gait_thread = FootPlot.GaitPlotingThread(
                        self.gait_diagram, self.quadruped_robot, self.gait, 1,
                        moving_index, discount_factor = self.gaitGUI.DensityInput.value()
                        )
                    self.gait_thread.start()
            else:
                moving_index = np.arange(self.quadruped_robot.log_len) * self.gait_speed_up
                self.gait_thread = FootPlot.GaitPlotingThread(
                    self.gait_diagram, self.quadruped_robot, self.gait, 1,
                    moving_index, discount_factor = self.gaitGUI.DensityInput.value()
                    )
                self.gait_thread.start()
    
    def plotGlobalGaitTrace(self):
        if self.gait_thread is not None:
            if not self.gait_thread.is_alive():
                self.gait_diagram.plotTrace()


    def _plotAllFoot(self):
        '''
            When ever this method is called it will update all plot
        '''
        self.foot_fl_diagram.plotFoot(foot_model=self.quadruped_robot.leg_lf, expect_foot=None)
        self.foot_fr_diagram.plotFoot(foot_model=self.quadruped_robot.leg_rf, expect_foot=None)
        self.foot_rl_diagram.plotFoot(foot_model=self.quadruped_robot.leg_lh, expect_foot=None)
        self.foot_rr_diagram.plotFoot(foot_model=self.quadruped_robot.leg_rh, expect_foot=None)
        self.foot_fl_diagram.plotTrace(foot_model=self.quadruped_robot.leg_lf)
        self.foot_fr_diagram.plotTrace(foot_model=self.quadruped_robot.leg_rf)
        self.foot_rl_diagram.plotTrace(foot_model=self.quadruped_robot.leg_lh)
        self.foot_rr_diagram.plotTrace(foot_model=self.quadruped_robot.leg_rh)

    def resetCounter(self):
        self.gait_counter = 0
        self.test_counter = 0

    def speedUP(self):
        if not self.GaitRunning:
            self.gait_speed_up = self.GUI.SpeedUpIn.value()

    def initialDynamicTable(self):
        for i in range(11):
            self.GUI.dynamic_value.setItem(i, 0, QtWidgets.QTableWidgetItem("0"))



    def saveToCsv(self):
        now = datetime.datetime.now()
        folder_name = f"Manual_{now.month:02d}{now.day:02d}-{now.hour}{now.minute}"
        folder_name = "./excuResult/"+folder_name
        os.mkdir(folder_name)
        type_list = ["pos"        , "vel"        , "acc"        , "toq"]
        attr_list = ["PositionLog", "VelocityLog", "Acceler_Log", "torque_log"]
        data = dict()
        if (not self.gait_counter) or (not self.test_counter):
            data_len = self.test_counter if self.test_counter != 0 else self.gait_counter
            for motor in self.quadruped_robot.leg_lf.motors:
                motor_name = motor.name
                motor_data = dict()
                for d_type, attr in zip(type_list, attr_list):
                    motor_data[d_type] = getattr(motor, attr)[-data_len:]
                motor_data['time_base'] = np.arange(-self.quadruped_robot.log_len,0) * motor.time_step
                data[motor_name] = motor_data
            # data['fbd_output'] = self.quadruped_robot.fbd_log[:,-data_len:]
            sio.savemat("{0}/data.mat".format(folder_name),data)

    def setLogLevel(self):
        log_level = "info"
        log_file_level = "debug"
        if self.GUI.radio_flog_debug.isChecked():
            log_file_level = "debug"
        elif self.GUI.radio_flog_info.isChecked():
            log_file_level = "info"
        elif self.GUI.radio_flog_warning.isChecked():
            log_file_level = "warning"
        elif self.GUI.radio_flog_error.isChecked():
            log_file_level = "error"
        if self.GUI.radio_log_debug.isChecked():
            log_level = "debug"
        elif self.GUI.radio_log_info.isChecked():
            log_level = "info"
        elif self.GUI.radio_log_warning.isChecked():
            log_level = "warning"
        elif self.GUI.radio_log_error.isChecked():
            log_level = "error"
        print(f"Set log level to {log_level} ; file log level to {log_file_level}")
        if hasattr(self, "quadruped_robot"):
            self.quadruped_robot.setLogLevel(log_level, log_file_level)

    def StartStop(self):
        self.runing_Flag = (not self.runing_Flag)
        if self.runing_Flag == True:
            self.GUI.StartStopBTN.setText("Pause")
            self.GUI.StartStopBTN.repaint()
        elif self.runing_Flag == False:
            try:
                self.GaitTimer.stop()
                self.GaitRunning = False
            except:
                pass
            if self.real_motor_timer.isActive():
                self.real_motor_timer.stop()
                self.monitoring_real_motor = False
                if self.bool_real_motor:
                    self.GUI.real_motor_status_Lb.setText("Activated")
            if self.bool_motor_armed:
                self.quadruped_robot.disarmAllMotor()

            self.GUI.StartStopBTN.setText("Start")
            self.GUI.StartStopBTN.repaint()


    def switchOffset(self):
        if self.GUI.real_motor_pos_rb.isChecked():
            self.bool_real_motor_pos = True
        else:
            self.bool_real_motor_pos = False

    def switchOpMode(self):
        if self.bool_real_motor:
            if not self.bool_motor_armed:
                if self.GUI.velocity_ik_rbtn.isChecked():
                    self.quadruped_robot.switchMotorMode('current')
                if self.GUI.position_ik_rbtn.isChecked():
                    self.quadruped_robot.switchMotorMode('position')
                self.bool_motor_armed = False
                self.GUI.real_motor_status_Lb.setText("Activated")
                self.GUI.arm_motor_Btn.setText("Arm Motor")
                self.GUI.arm_motor_Btn.repaint()


    def updateRealModel(self):
        if not self.bool_real_motor:self._plotAllFoot()
        self.Modi_sidebar()

    def updateTarget(self, update_plot = True):
        self.target[0] = float(self.GUI.X_Cmd_in.text())
        self.target[1] = float(self.GUI.Y_Cmd_in.text())
        self.foot_fl_diagram.target = [False, self.target]
        self.foot_fr_diagram.target = [False, self.target]
        self.foot_rl_diagram.target = [False, self.target]
        self.foot_rr_diagram.target = [False, self.target]
        if update_plot:
            self._plotAllFoot()

    def useIK(self):
        self.updateTarget(update_plot=False)
        Cmd_X = self.target[0]
        Cmd_Y = self.target[1]
        if not self.quadruped_robot.leg_lf.checkWorkingSpace(Cmd_X, Cmd_Y):
            self.quadruped_robot.executeAllIK(Cmd_X, Cmd_Y)

            self.quadruped_robot.endPointCalculation()
            self.GUI.PosX.display(round(self.quadruped_robot.leg_lf.EndPoint[0],2))
            self.GUI.PosY.display(round(self.quadruped_robot.leg_lf.EndPoint[1],2))

            if not self.bool_real_motor:self._plotAllFoot()
            self.Modi_sidebar()
        else:
            self.GUI.statusbar.showMessage("Out Space",1000)



class ManualUI(mcUI.Ui_MainWindow):
    def __init__(self):
        self.window = QtWidgets.QMainWindow()
        self.setupUi(self.window)


class DynamicLogUI(dyUI.Ui_MainWindow):
    def __init__(self):
        self.window = QtWidgets.QMainWindow()
        self.setupUi(self.window)

class GaitUI(gaUI.Ui_MainWindow):
    def __init__(self):
        self.window = QtWidgets.QMainWindow()
        self.setupUi(self.window)

class SaveUI(saUI.Ui_MainWindow):
    def __init__(self):
        self.window = QtWidgets.QMainWindow()
        self.setupUi(self.window)

def testGround():
    pass

if __name__ == '__main__':
    CurApp = App()
    #testGround()