from collections import namedtuple
import random
import pickle
import traceback
import numpy as np

import torch
from torch import nn
import torch.nn.functional as F



try:
    from torch2trt import torch2trt
    HAS_TensorRT = True
except ModuleNotFoundError:
    HAS_TensorRT = False




class NetworkModel(nn.Module):
    def __init__(self, list_layer, network_name=None):
        super(NetworkModel, self).__init__()
        self.network_name = network_name
        self.func_in = nn.Linear(*list_layer[0])
        self.hiddn_layers = []
        for layer in range(len(list_layer) - 2 ):
            self.add_module('h_layer_' + str(layer), nn.Linear(*list_layer[layer+1]))
            # getattr(self, 'h_layer_' + str(layer)).weight.data.normal_(0, 0.1)
            self.hiddn_layers.append(getattr(self, 'h_layer_' + str(layer)))
        self.func_out = nn.Linear(*list_layer[-1])

    def forward(self, x):
        if self.network_name == 'actor':
            x = F.softsign(self.func_in(x))
            for layer in self.hiddn_layers:
                x = F.softsign(layer(x))
            x = F.softsign(self.func_out(x))
        elif self.network_name == 'ciritic':
            x = F.softsign(self.func_in(x))
            for layer in self.hiddn_layers:
                x = F.relu(layer(x))
            x = self.func_out(x)
        else:
            print('Network name error')
        return x


class NetworkModelDropOut(nn.Module):
    def __init__(self, list_layer, drop_out_rate=0.3):
        super(NetworkModelDropOut, self).__init__()
        self.func_in = nn.Linear(*list_layer[0])
        self.hiddn_layers = []
        for layer in range(len(list_layer) - 2 ):
            self.add_module('h_layer_' + str(layer), nn.Linear(*list_layer[layer+1]))
            self.hiddn_layers.append(getattr(self, 'h_layer_' + str(layer)))
        self.func_out = nn.Linear(*list_layer[-1])
        self.dropout_rate = drop_out_rate

    def forward(self, x):
        x = F.relu(self.func_in(x))
        for layer in self.hiddn_layers:
            x = F.relu(layer(x))
            x = F.dropout(x, self.dropout_rate)
        x = self.func_out(x)
        return x

    def setDropOutRate(self, dropout_rate):
        self.dropout_rate = dropout_rate





class ReplayMemory(object):
    def __init__(self, capacity, device):
        self.capacity = capacity
        self.memory = []
        self.position = 0

        self.SingleData = namedtuple('SingleData',
                        ('state', 'action', 'next_state', 'reward'))
        self.device = device

    def push(self, currentState, action, newState, reward):
        """Saves a transition."""
        if len(self.memory) < self.capacity:
            self.memory.append(None)

        tensor_transition = self.SingleData(
            torch.tensor([currentState], device=self.device, dtype=torch.float),
            torch.tensor([[action]], device=self.device, dtype=torch.long),
            torch.tensor([newState], device=self.device, dtype=torch.float) if newState is not None else None,
            torch.tensor([reward], device=self.device, dtype=torch.float)
        )

        self.memory[self.position] = tensor_transition
        self.position = (self.position + 1) % self.capacity

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)




class DDPG:
    def __init__(self,
        actor_list_network_shape,
        ciritic_list_network_shape,
        actor_learning_rate=5e-5, 
        ciritic_learning_rate=5e-5, 
        gamma=0.7, 
        var_start=0.9, 
        var_end=0.05, 
        var_decay=500, 
        tau=5e-5
    ):
        self.actor_list_network_shape = actor_list_network_shape
        self.ciritic_list_network_shape = ciritic_list_network_shape
        self.actor_learning_rate = actor_learning_rate
        self.ciritic_learning_rate = ciritic_learning_rate
        self.gamma = gamma
        self.var_start = var_start
        self.var_end = var_end
        self.var_decay = var_decay
        self.tau = tau

        self.filter_data_type = None
        self.actor_policy_network = None
        self.actor_target_network = None
        self.ciritic_policy_network = None
        self.ciritic_target_network = None
        self.device = None
        self.actor_optimizer = None
        self.ciritic_optimizer = None
        self.SingleData = namedtuple('SingleData',
                                ('current_state', 'action', 'new_state', 'reward'))


        self.bool_GPU_available = False
        self.bool_drop_out = False
        self.bool_half_mode = False

        self.actor_list_network_shape = []
        self.ciritic_list_network_shape = []


    def checkDeviceStatus(self):
        if torch.cuda.is_available():
            self.bool_GPU_available = True

    
    def checkPyTorchVersion(self) -> None:
        '''
        create filter_data_type
        based on different version of pytorch
        '''
        if torch.__version__.startswith("1.0"):
            print(f"torch version : {torch.__version__}, filter_data_type use torch.uint8")
            self.filter_data_type = torch.uint8
        else:
            print(f"torch version : {torch.__version__}, filter_data_type use torch.bool")
            self.filter_data_type = torch.bool

    def chooseAction(self, state):
        if self.bool_half_mode:
            tensor_state = torch.tensor(state, device=self.device).half()
        else:
            tensor_state = torch.tensor(state, device=self.device)

        with torch.no_grad():
            return self.actor_policy_network(torch.unsqueeze(tensor_state, 0))[0].detach()



    def generateNetwork(self, actor_list_network_shape, ciritic_list_network_shape) -> None:
        if self.device is not None:
            self.actor_list_network_shape = self.interpretListNetworkShape(actor_list_network_shape)
            self.ciritic_list_network_shape = self.interpretListNetworkShape(ciritic_list_network_shape)
            if self.bool_drop_out:
                self.actor_policy_network = NetworkModelDropOut(self.actor_list_network_shape, getattr(self, "drop_out_rate"))
                self.ciritic_policy_network = NetworkModelDropOut(self.ciritic_list_network_shape, getattr(self, "drop_out_rate"))
            else:
                self.actor_policy_network = NetworkModel(self.actor_list_network_shape, 'actor')
                self.ciritic_policy_network = NetworkModel(self.ciritic_list_network_shape, 'ciritic')
            self.actor_policy_network.to(self.device)
            self.ciritic_policy_network.to(self.device)
        else:
            print("Device has not choosed yet.")


    def generateOptimizer(self):
        self.actor_optimizer = torch.optim.Adam(self.actor_policy_network.parameters(), self.actor_learning_rate)
        self.ciritic_optimizer = torch.optim.Adam(self.ciritic_policy_network.parameters(), self.ciritic_learning_rate)

    def generateTargetNetwork(self):
        self.actor_target_network = NetworkModel(self.actor_list_network_shape, 'actor')
        self.actor_target_network.load_state_dict(self.actor_policy_network.state_dict())
        self.actor_target_network.to(self.device)
        self.actor_target_network.eval()
        self.ciritic_target_network = NetworkModel(self.ciritic_list_network_shape, 'ciritic')
        self.ciritic_target_network.load_state_dict(self.ciritic_policy_network.state_dict())
        self.ciritic_target_network.to(self.device)
        self.ciritic_target_network.eval()

    def initMemory(self, int_memory_size, batch_size):
        self.memory = ReplayMemory(int_memory_size, self.device)
        self.batch_size = batch_size
        return self.memory



    def interpretListNetworkShape(self, list_network_shape) -> list:
        interpreted_network_shape = []
        for index in range(len(list_network_shape) - 1):
            interpreted_network_shape.append((list_network_shape[index], list_network_shape[index + 1]))
        # print(f"{interpreted_network_shape}")
        return interpreted_network_shape

    def learning(self, transitions, var):
        batch = self.SingleData(*zip(*transitions))
        # print(f"length of transitions {len(transitions)}")

        # Compute a mask of non-final states and concatenate the batch elements
        # (a final state would've been the one after which simulation ended)
        non_final_mask = torch.tensor(tuple(map(lambda s: s is not None,
                                            batch.new_state)), device=self.device, dtype=self.filter_data_type)
        non_final_next_states = torch.cat([s for s in batch.new_state
                                                    if s is not None])
        state_batch  = torch.cat(batch.current_state)
        action_batch = torch.cat(batch.action)
        action_batch = action_batch.squeeze()
        reward_batch = torch.cat(batch.reward)
        # print(non_final_mask.shape, non_final_next_states.shape)
        # new_state = torch.cat(batch.new_state)

        # Compute Q(s_t, a) - the model computes Q(s_t), then we select the
        # columns of actions taken. These are the actions which would've been taken
        # for each batch state according to policy_net

        #DDPG
        
        # Compute V(s_{t+1}) for all next states.
        # Expected values of actions for non_final_next_states are computed based
        # on the "older" target_net; selecting their best reward with max(1)[0].
        # This is merged based on the mask, such that we'll have either the expected
        # state value or 0 in case the state was final.

        #ciritic learning
        next_state_values = torch.zeros((len(transitions), self.actor_list_network_shape[-1][1]), device=self.device)
        next_state_action = torch.zeros((len(transitions), self.actor_list_network_shape[-1][1]), device=self.device)
        nosie_tensor = torch.zeros((len(transitions), self.actor_list_network_shape[-1][1]), device=self.device)
        next_state = torch.zeros((len(transitions), self.actor_list_network_shape[0][0]), device=self.device)

        next_state[torch.where(non_final_mask)[0]] = non_final_next_states
        next_predict = self.actor_target_network(non_final_next_states)
        next_state_action[torch.where(non_final_mask)[0]] = next_predict
        nosie = torch.ones_like(next_predict).data.normal_(0, var).to(self.device)
        nosie = nosie.clamp(-0.5, 0.5)
        nosie_tensor[torch.where(non_final_mask)[0]] = nosie

        next_state_values = next_state_action
        next_state_values.clamp(-1, 1)
        next_ciritc_values = self.ciritic_target_network(torch.cat([next_state, next_state_values], 1))

        # Compute the expected Q values
        expected_state_action_values = (next_ciritc_values * self.gamma) + reward_batch.unsqueeze(1)
        # print(f"1",next_ciritc_values[0])
        # Compute Huber loss
        q_eval = self.ciritic_policy_network(torch.cat([state_batch, action_batch], 1))
        # print(q_eval[0])
        loss = F.mse_loss(q_eval, expected_state_action_values)
        loss_return = loss.item()
        # print(f"critic loss = {loss_return}")
        # Optimize the model
        self.ciritic_optimizer.zero_grad()
        loss.backward()
        # resticct between -1~1
        # for param in self.ciritic_policy_network.parameters():
        #     param.grad.data.clamp_(-1, 1)
        self.ciritic_optimizer.step()


        #actor learning
        action_output = self.actor_policy_network(state_batch)
        state_action_values = self.ciritic_policy_network(torch.cat([state_batch, action_output], 1))
        # print(action_output)
        actor_loss = -torch.mean(state_action_values)
        actor_loss_return = actor_loss.item()
        # print(f"actor loss = {actor_loss_return}")

        self.actor_optimizer.zero_grad()
        actor_loss.backward()
        # for param in self.actor_policy_network.parameters():
        #     param.grad.data.clamp_(-1, 1)
        self.actor_optimizer.step()


        for target, param in zip(self.ciritic_target_network.parameters(), self.ciritic_policy_network.parameters()):
            target.data.copy_(target.data*(1.0-self.tau)+param.data*self.tau)
        for target, param in zip(self.actor_target_network.parameters(), self.actor_policy_network.parameters()):
            target.data.copy_(target.data*(1.0-self.tau)+param.data*self.tau)
        # for x in self.actor_target_network.state_dict().keys():
        #     eval('self.actor_target_network.' + x + '.data.mul_((1-self.tau))')
        #     eval('self.actor_target_network.' + x + '.data.add_(self.tau*self.actor_policy_network.' + x + '.data)')
        # for x in self.ciritic_target_network.state_dict().keys():
        #     eval('self.ciritic_target_network.' + x + '.data.mul_((1-self.tau))')
        #     eval('self.ciritic_target_network.' + x + '.data.add_(self.tau*self.ciritic_policy_network.' + x + '.data)')
        
        return [loss_return, actor_loss_return]

    def loadPolicyNetwork(self, model_file_path, shape_file_path) -> bool:
        try:
            shape_file = open(shape_file_path, 'rb')
            model_file = open(model_file_path, 'rb')
            self.list_network_shape = pickle.load(shape_file)
            self.policy_network = NetworkModel(self.list_network_shape)
            self.policy_network.load_state_dict(torch.load(model_file, map_location=self.device))
            return True
        except FileNotFoundError:
            traceback.print_exc()
            return False


    def setDropOut(self, drop_out_rate=0.3) -> None:
        self.bool_drop_out = True
        setattr(self, "drop_out_rate", drop_out_rate)

    def setLearningRate(self, learning_rate) -> None:
        self.learning_rate = learning_rate


    def selectDevice(self, bool_force_cpu=False, int_gpu_id=None) -> None:
        if not bool_force_cpu and self.bool_GPU_available:
            gpu_candidate = list()
            for gpu_index in range(torch.cuda.device_count()):
                print("    device [{0}]: {1}".format(gpu_index, torch.cuda.get_device_name(gpu_index)))
                gpu_candidate.append(gpu_index)
            select = -1
            if int_gpu_id is None:
                while select not in gpu_candidate:
                    select = int(input("Please select your device:"))
            else:
                print(f"Designate GPU ID {int_gpu_id}")
                select = int_gpu_id
            torch.cuda.set_device(select)
            self.device = torch.device("cuda")
            print(f"Device choosed: {torch.cuda.get_device_name(self.device)}")
        else:
            print(f"Device choosed : CPU")
            self.device = torch.device("cpu")

    def setPolicyToEvalMode(self) -> None:
        if self.policy_network is not None:
            self.policy_network.eval()
        else:
            print("No policy network exist in DQN")

    def updateTargetNetwork(self):
        self.target_network.load_state_dict(self.policy_network.state_dict())





