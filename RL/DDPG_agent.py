import os
import sys
import math
import random
import torch
import numpy as np
from collections import namedtuple


from .DDPG import DDPG, ReplayMemory



class DDPGAgent:
    def __init__(
        self, 
        actor_list_network_shape,
        ciritic_list_network_shape,
        actor_learning_rate = 5e-5,
        ciritic_learning_rate = 5e-5,
        var_start = 0.9,
        var_end = 0.05,
        var_decay = 500,
        gamma = 0.7,
        tau = 5e-5,
    ):

        self.actor_list_network_shape = actor_list_network_shape
        self.ciritic_list_network_shape = ciritic_list_network_shape
        self.var_start = var_start
        self.var_decay = var_decay
        self.var_end = var_end
        self.gamma = gamma
        self.tau = tau

        self.DDPG = DDPG(
            self.actor_list_network_shape,
            self.ciritic_list_network_shape,
            actor_learning_rate=actor_learning_rate,
            ciritic_learning_rate = ciritic_learning_rate,
            gamma = self.gamma,
            var_start = self.var_start,
            var_end = self.var_end,
            var_decay = self.var_decay,
            tau = self.tau
        )

        self.bool_execute_mode = False
        self.batch_size = 256
        self.memory = None
        self.var = 0.0


    def chooseAction(self, state, total_execution_time) -> int:
        action = self.DDPG.chooseAction(state)
        self.calulateVar(total_execution_time)
        return action
        #return np.clip(np.random.normal(action, self.var), low_bound, high_bound)



    def chooseModelFile(self, model_file_name=None) -> tuple([str]):
        if model_file_name is None:
            for name in os.listdir("./model"):
                if name.endswith('.model'): print(name[:-6])
            filename = input('input the .parm file or leave blank to creat new Q_Network:')
        model_file_name = "./model/" + filename + ".model"
        network_shape_file_name = "./model/" + filename + ".shape"
        return model_file_name, network_shape_file_name

    def calulateVar(self, total_execute_times) -> bool:
        self.var = self.var_end + (self.var_start - self.var_end)*math.exp(-1*total_execute_times/self.var_decay)



    def initDDPGLearning(self, memory_size=1024, batch_size=256, designate_gpu_id=None):
        self.DDPG.checkPyTorchVersion()
        self.DDPG.checkDeviceStatus()
        self.DDPG.selectDevice(int_gpu_id=designate_gpu_id)
        self.DDPG.generateNetwork(self.actor_list_network_shape, self.ciritic_list_network_shape)
        self.DDPG.generateTargetNetwork()
        self.DDPG.generateOptimizer()

        self.memory = self.DDPG.initMemory(memory_size, batch_size)
        self.batch_size = batch_size
        self.bool_execute_mode = False

    def initDDPGLearningContinue(self, model_name, memory_size=1024, batch_size=256, designate_gpu_id=None):
        self.DDPG.checkPyTorchVersion()
        self.DDPG.checkDeviceStatus()
        self.DDPG.selectDevice(int_gpu_id=designate_gpu_id)
        model_file_name, network_shape_file_name = self.chooseModelFile(model_name)
        self.DDPG.loadPolicyNetwork(model_file_name, network_shape_file_name)
        self.DDPG.generateTargetNetwork()
        self.DDPG.generateOptimizer()

        self.memory = self.DDPG.initMemory(memory_size, batch_size)
        self.batch_size = batch_size
        self.bool_execute_mode = False

    def initDDPGExecute(self, model_file_name=None):
        self.DDPG.checkPyTorchVersion()
        self.DDPG.checkDeviceStatus()
        self.DDPG.selectDevice()
        model_file_name, network_shape_file_name = self.chooseModelFile(model_file_name)
        self.DDPG.loadPolicyNetwork(model_file_name, network_shape_file_name)
        self.DDPG.setPolicyToEvalMode()
        self.bool_execute_mode = True
    
    def initDDPGExecuteTest(self):
        self.DDPG.checkPyTorchVersion()
        self.DDPG.checkDeviceStatus()
        self.DDPG.selectDevice()
        self.DDPG.generateNetwork(self.list_network_shape)
        self.DDPG.setPolicyToEvalMode()
        self.bool_execute_mode = True



    def learning(self, transition):
        self.memory.push(*transition)
        if len(self.memory) < self.batch_size:
            return None
        else:
            transitions = self.memory.sample(self.batch_size)
            # Transpose the batch (see https://stackoverflow.com/a/19343/3343043 for
            # detailed explanation). This converts batch-array of Transitions
            # to Transition of batch-arrays.
            return self.DDPG.learning(transitions, self.var)
    
    def setLearningRate(self, learning_rate) -> None:
        self.DDPG.setLearningRate(learning_rate)



    def updateTargetNetwork(self):
        self.DDPG.updateTargetNetwork()
