import numpy as np


class FunctionDDPG(object):
    @staticmethod
    def getState(quadruped_robot_model, roll, pitch, last_action):
        state = [
            #upper 02 540
            float(round(quadruped_robot_model.leg_lf.motors[0].global_angle, 2)),
            float(round(quadruped_robot_model.leg_rf.motors[0].global_angle, 2)),
            float(round(quadruped_robot_model.leg_lh.motors[0].global_angle, 2)),
            float(round(quadruped_robot_model.leg_rh.motors[0].global_angle, 2)),
            #lower 02 900
            float(round((quadruped_robot_model.leg_lf.motors[1].global_angle), 2)),
            float(round((quadruped_robot_model.leg_rf.motors[1].global_angle), 2)),
            float(round((quadruped_robot_model.leg_lh.motors[1].global_angle), 2)),
            float(round((quadruped_robot_model.leg_rh.motors[1].global_angle), 2)),
            #hip 12 540
            # float(round(quadruped_robot_model.leg_lf.motors[2].global_angle, 2)),
            # float(round(quadruped_robot_model.leg_rf.motors[2].global_angle, 2)),
            # float(round(quadruped_robot_model.leg_lh.motors[2].global_angle, 2)),
            # float(round(quadruped_robot_model.leg_rh.motors[2].global_angle, 2)),

            float(round(quadruped_robot_model.leg_lf.motors[0].velocity, 2)),
            float(round(quadruped_robot_model.leg_rf.motors[0].velocity, 2)),
            float(round(quadruped_robot_model.leg_lh.motors[0].velocity, 2)),
            float(round(quadruped_robot_model.leg_rh.motors[0].velocity, 2)),
            float(round(quadruped_robot_model.leg_lf.motors[1].velocity, 2)),
            float(round(quadruped_robot_model.leg_rf.motors[1].velocity, 2)),
            float(round(quadruped_robot_model.leg_lh.motors[1].velocity, 2)),
            float(round(quadruped_robot_model.leg_rh.motors[1].velocity, 2)),
            # float(round(quadruped_robot_model.leg_lf.motors[2].velocity, 2)),
            # float(round(quadruped_robot_model.leg_rf.motors[2].velocity, 2)),
            # float(round(quadruped_robot_model.leg_lh.motors[2].velocity, 2)),
            # float(round(quadruped_robot_model.leg_rh.motors[2].velocity, 2)),
            float(round(last_action[0], 2)) if last_action is not None else float(round(quadruped_robot_model.leg_lf.motors[0].global_angle, 2)),
            float(round(last_action[1], 2)) if last_action is not None else float(round(quadruped_robot_model.leg_rf.motors[0].global_angle, 2)),
            float(round(last_action[2], 2)) if last_action is not None else float(round(quadruped_robot_model.leg_lh.motors[0].global_angle, 2)),
            float(round(last_action[3], 2)) if last_action is not None else float(round(quadruped_robot_model.leg_rh.motors[0].global_angle, 2)),
            float(round(last_action[4], 2)) if last_action is not None else float(round((quadruped_robot_model.leg_lf.motors[1].global_angle), 2)),
            float(round(last_action[5], 2)) if last_action is not None else float(round((quadruped_robot_model.leg_rf.motors[1].global_angle), 2)),
            float(round(last_action[6], 2)) if last_action is not None else float(round(quadruped_robot_model.leg_lh.motors[1].global_angle, 2)),
            float(round(last_action[7], 2)) if last_action is not None else float(round((quadruped_robot_model.leg_rh.motors[1].global_angle), 2)),
            # float(round(last_action[8], 2)) if last_action is not None else float(round(quadruped_robot_model.leg_lf.motors[2].global_angle, 2)),
            # float(round(last_action[9], 2)) if last_action is not None else float(round(quadruped_robot_model.leg_rf.motors[2].global_angle, 2)),
            # float(round(last_action[10], 2)) if last_action is not None else float(round(quadruped_robot_model.leg_lh.motors[2].global_angle, 2)),
            # float(round(last_action[11], 2)) if last_action is not None else float(round(quadruped_robot_model.leg_rh.motors[2].global_angle, 2)),
            roll,
            pitch,
            # round(quadruped_robot_model.leg_lf.EndPoint[1], 2),
            # round(quadruped_robot_model.leg_rf.EndPoint[1], 2),
            # round(quadruped_robot_model.leg_lh.EndPoint[1], 2),
            # round(quadruped_robot_model.leg_rh.EndPoint[1], 2),

            # quadruped_robot_model.posture_x,
            # quadruped_robot_model.posture_y,
            # quadruped_robot_model.posture_z,
            float(quadruped_robot_model.touch_1),
            float(quadruped_robot_model.touch_2),
            float(quadruped_robot_model.touch_3),
            float(quadruped_robot_model.touch_4),
        ]
        return state

    @staticmethod
    def interpretAction(pnn_action_ID, action_resolution, action_space_x, action_space_y):
        
        x_cmd = action_space_x[pnn_action_ID//action_resolution]
        y_cmd = action_space_y[pnn_action_ID%action_resolution]
        # left = action_1
        # right = -action_1

        return x_cmd, y_cmd

    @staticmethod
    def interpretState(state):
        for i in range(4):
            state[i] = round((state[i]-231)/20-1, 2)
            state[i+4] = round((state[i+4]-435)/40-1, 2)
            state[i+8] = round((state[i+8]-260)/10-1, 2)

        return state

