import sys
import math
import numpy as np


class RewardManager:
    ''' Directly inherit from Eric RewardMachine'''

    def __init__(self, list_activate_rw_name):
        self.total_reward = 0
        defalult_activate = [
            'Fail'
        ]
        list_activate_rw_name = list_activate_rw_name + defalult_activate
        self.dict_activated_reward = dict()
        self.dict_reward_factor = dict()
        self.dict_each_reward = dict()
        
        for reward_name in list_activate_rw_name:
            try:
                rw_func = (globals()['RwBy' + reward_name])
                self.dict_activated_reward[reward_name] = rw_func()
                self.dict_activated_reward[reward_name].name = reward_name
                self.dict_reward_factor[reward_name] = self.dict_activated_reward[reward_name].factor

            except KeyError:
                print("No Such Reward: {0}".format(reward_name))
                print("Please check your reward setting again")
                import sys
                sys.exit()
        
        print("Reward Function Activate:")
        for key in self.dict_activated_reward.keys():
            print("    " + key)
    
    def excuReward(self, c_state, n_state, quadrobot_robot_model, training_parameters):
        self.total_reward = 0
        for reward_func in self.dict_activated_reward.values():
            sub_reward = reward_func.getReward(c_state, n_state, quadrobot_robot_model, training_parameters)
            self.total_reward += sub_reward
            self.dict_each_reward[reward_func.name] = sub_reward
        return self.total_reward

    def chengeFactor(self, factor_dict):
        self.dict_reward_factor = factor_dict
        for reward_name, factor in self.dict_reward_factor.items():
            self.dict_activated_reward[reward_name].factor = factor    




class RewardGeneralForm(object):

    def __init__(self):
        self.reward = 0
        self.factor = 1

    def rewardFunc(self, c_state, n_state, quadrobot_robot_model, training_parameters):
        pass
    
    def getReward(self, c_state, n_state, quadrobot_robot_model, training_parameters):
        self.reward = 0
        self.rewardFunc(c_state, n_state, quadrobot_robot_model, training_parameters)
        return self.reward * self.factor


# ===================== Reward Functions ==============================

class RwByStaticInclineAngle(RewardGeneralForm):
    ''' This reward only care about the absolute static incline angle of quadruped robot'''
    def __init__(self):
        super().__init__()
        self.error_x_factor = 1
        self.error_y_factor = 1
        self.error_z_factor = 1
        self.error_a_factor = 1
        self.max_reward = 0.2
        self.reward_0_degree = 5
    
    def rewardFunc(self, c_state, n_state, quadrobot_robot_model, training_parameters):
        # if all(i < 5e-5 for i in ground_change):
        self.reward = 0
        incline_angle_rad = training_parameters.new_total_incline_angle
        print(f"angle = {np.rad2deg(incline_angle_rad)}")
        # print(incline_angle_rad)
        # I hope the reward trajectory is that max_reward when incline_angle is approach 0
        # and it become zero at certain_sangle and become -max_reward at 2*certain_degree
        # EX: -0.2*tanh(36*(x-0.087266)) for 0 deg max; 5 degree is zero; 10 degree min 
        # self.reward = round(-self.max_reward*math.tanh(180/self.reward_0_degree*(incline_angle_rad - math.radians(self.reward_0_degree))), 2)
        if abs(incline_angle_rad) < math.radians(5):
            self.reward = 0.1

        
class RwByFail(RewardGeneralForm):
    def __init__(self):
        super().__init__()
        self.max_reward = -1.5
        self.reward_fail_degree = 30
        self.fail_roll = 0.4
        self.fail_pitch = 0.2
        self.fail_y = 0.36
    
    def rewardFunc(self, c_state, n_state, quadrobot_robot_model, training_parameters):
        self.reward = 0
        if abs(training_parameters.new_total_incline_angle) > math.radians(self.reward_fail_degree):
            self.reward = self.max_reward
        elif quadrobot_robot_model.wb_robot.getTime() > 60:
            self.reward = self.max_reward
        elif abs(training_parameters.new_error_x) > self.fail_roll:
            self.reward = self.max_reward
        elif abs(training_parameters.new_error_y) > self.fail_pitch:
            self.reward = self.max_reward
        elif training_parameters.robot_y < self.fail_y:
            self.reward = self.max_reward
        elif training_parameters.action_count > 500:
            self.reward = self.max_reward
            print(f"fail by count")
        self.reward *= (1-training_parameters.var)

class RwByGoal(RewardGeneralForm):
    def __init__(self):
        super().__init__()
        self.max_reward = 5
    
    def rewardFunc(self, c_state, n_state, quadrobot_robot_model, training_parameters):
        self.reward = 0
        if training_parameters.new_x > 2.4:
            self.reward = self.max_reward
class RwByForward(RewardGeneralForm):
    def __init__(self):
        super().__init__()
        self.max_reward = 1
    
    def rewardFunc(self, c_state, n_state, quadrobot_robot_model, training_parameters):
        tp = training_parameters
        if tp.new_robot_velocity[0] > 0.05:
            ratio = tp.new_robot_velocity[0]
            self.reward = ratio * self.max_reward
        else:
            self.reward = -self.max_reward*(1-tp.var)
        # print(f"forward reward = {self.reward}")

class RwByGait(RewardGeneralForm):
    def __init__(self):
        super().__init__()
        self.max_reward = 0.05
    
    def rewardFunc(self, c_state, n_state, quadrobot_robot_model, training_parameters):
        tp = training_parameters
        self.reward = 0
        for i in range(12):
            self.reward += abs(tp.target[i]-tp.position[i])
        self.reward *= -self.max_reward

class RwByMovement(RewardGeneralForm):
    def __init__(self):
        super().__init__()
        self.max_reward = 0.05
    
    def rewardFunc(self, c_state, n_state, quadrobot_robot_model, training_parameters):
        tp = training_parameters
        # print(f"move reward = {abs(tp.new_robot_velocity[2])}")
        # self.reward = -np.clip(abs(tp.new_robot_velocity[2]), 0, 1)*self.max_reward
        self.reward = -self.max_reward*(1-tp.var) if abs(tp.new_robot_velocity[2]) > 0.1 else 0.01
        # print(f"move reward = {self.reward}")


class RwByWork(RewardGeneralForm):
    def __init__(self):
        super().__init__()
        self.max_reward = 0.001

    def rewardFunc(self, c_state, n_state, quadrobot_robot_model, training_parameters):
        self.reward = 0
        action_magnitude = 0
        # torque_system = quadrobot_robot_model.torque
        tp = training_parameters
        # print(c_state)
        for leg in range(4):
            for motor in range(3):
                action_magnitude += abs((n_state[leg+motor*4]-c_state[leg+motor*4])*(tp.new_torque[leg][motor]-tp.current_torque[leg][motor]))     
                # print(f"new = {n_state[leg*3+motor]} current = {c_state[leg*3+motor]}")
        # print(action_magnitude/100)
        self.reward = -np.clip(action_magnitude/300, 0, 1)*self.max_reward
        
class RwByJointSpeed(RewardGeneralForm):
    def __init__(self):
        super().__init__()
        self.max_reward = 0.005
        self.motor_max_velocity = 10

    def rewardFunc(self, c_state, n_state, quadrobot_robot_model, training_parameters):
        self.reward = 0
        velocity = 0
        for leg in range(4):
            for motor in range(3):
                if abs(quadrobot_robot_model.lower_system[leg].motors[motor].velocity) > velocity:
                    velocity = abs(quadrobot_robot_model.lower_system[leg].motors[motor].velocity)
        if velocity <= self.motor_max_velocity:
            ratio = velocity/self.motor_max_velocity
            self.reward = (1-ratio) * self.max_reward
        else:
            self.reward = -self.max_reward*(1-training_parameters.var)
        # print(f"joint reward = {velocity}")

class RwByZacc(RewardGeneralForm):
    def __init__(self):
        super().__init__()
        self.max_reward = 0.01

    def rewardFunc(self, c_state, n_state, quadrobot_robot_model, training_parameters):
        self.reward = 0
        self.reward = -self.max_reward if abs(quadrobot_robot_model.acc_y) > 1 else self.max_reward
        # print(f"zacc reward = {self.reward}")

class RwByTouch(RewardGeneralForm):
    def __init__(self):
        super().__init__()
        self.max_reward = 0.01
    
    def rewardFunc(self, c_state, n_state, quadrobot_robot_model, training_parameters):
        self.reward = 0
        tp = training_parameters
        for i in range(4):
            self.reward += tp.touch_ground[i]*tp.end_point_velocity[i]
        # print(self.reward)
        self.reward = -np.clip(abs(self.reward), 0, 1)*self.max_reward
        # print(self.reward)

class RwByEndPoint(RewardGeneralForm):
    def __init__(self):
        super().__init__()
        self.max_reward = 0.01
    
    def rewardFunc(self, c_state, n_state, quadrobot_robot_model, training_parameters):
        self.reward = 0
        for i in range(4):
            if training_parameters.touch_ground[i] == 0:
                # if training_parameters.end_point_velocity[i] > 0.5:
                #     self.reward += self.max_reward
                if training_parameters.end_point_velocity[i] < 0.5:
                    self.reward -= self.max_reward
        # print(self.reward)

class RwByTorque(RewardGeneralForm):
    def __init__(self):
        super().__init__()
        self.max_reward = 0.05
        self.motor_max_torque = 10
    
    def rewardFunc(self, c_state, n_state, quadrobot_robot_model, training_parameters):
        self.reward = 0
        # torque_system = quadrobot_robot_model.torque
        tp = training_parameters
        max_torque = np.max(np.abs(tp.new_torque))
        if max_torque <= 5:
            ratio = max_torque/self.motor_max_torque
            self.reward = (1-ratio) * self.max_reward
        else:
            self.reward = -self.max_reward*(1-tp.var)
        # print(f"torque reward = {max_torque}")
        
class RwByXBalance(RewardGeneralForm):
    def __init__(self):
        super().__init__()
        self.max_reward = 0.15
    
    def rewardFunc(self, c_state, n_state, quadrobot_robot_model, training_parameters):
        self.reward = 0
        tp = training_parameters
        self.reward = -np.clip(abs(tp.new_error_x), 0, 1)*self.max_reward


class RwByYBalance(RewardGeneralForm):
    def __init__(self):
        super().__init__()
        self.max_reward = 0.15
    
    def rewardFunc(self, c_state, n_state, quadrobot_robot_model, training_parameters):
        self.reward = 0
        tp = training_parameters
        self.reward = -np.clip(abs(tp.new_error_y), 0, 1)*self.max_reward