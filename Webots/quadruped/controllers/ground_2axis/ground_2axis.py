"""ground_controller controller."""

# You may need to import some classes of the controller module. Ex:
#  from controller import Robot, Motor, DistanceSensor
from controller import Robot
import numpy as np
import math
import time
from multiprocessing import shared_memory


import sys
import os
import random
# get current file directory
path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, path + "\\..\\..\\..\\..\\")





# create the Robot instance.
robot = Robot()
ground_x = robot.getDevice("ground_x")
ground_y = robot.getDevice("ground_y")
ground_sensor_x = ground_x.getPositionSensor()
ground_sensor_y = ground_y.getPositionSensor()


sm = shared_memory.ShareableList(name="ground_angle", sequence=[0.0, 0.0])


# get the time step of the current world.
timestep = int(robot.getBasicTimeStep())
# print(timestep)
current_time = 0

ground_sensor_x.enable(timestep)
ground_sensor_y.enable(timestep)

current_ground_angle_x, last_ground_angle_x = 0, 0
last_ground_cmd = 0

for i in range(50):
    robot.step(15)

sm_reset_flag = shared_memory.ShareableList(name="reset")
count = 0
ground_incline_dir = 1



max_x_angle = math.radians(5)
max_y_angle = math.radians(5)
max_rotation_velocity = math.radians(5)


ground_change_second = 8
ground_change_times = int(ground_change_second/timestep*1000)
ground_change_counter = 0

while robot.step(timestep) != -1:
    ground_cmd = ground_incline_dir*np.pi*10/180*np.sin(1*current_time/2)

    # if ground_change_counter % ground_change_times == 0:
    #     ground_x.setVelocity(max_rotation_velocity)
    #     ground_y.setVelocity(max_rotation_velocity)
    #     ground_cmd_x = (random.random() - 0.5)*2*max_x_angle
    #     ground_cmd_y = (random.random() - 0.5)*2*max_y_angle


    #     last_ground_cmd = ground_cmd_x
    #     ground_x.setPosition(ground_cmd_x)
    #     ground_y.setPosition(ground_cmd_y)
    #     print(f"ground x cmd {math.degrees(ground_cmd_x)}    ground y cmd {math.degrees(ground_cmd_y)}")

    last_ground_angle_x = current_ground_angle_x
    current_ground_angle_x = ground_sensor_x.getValue()
    current_ground_angle_y = ground_sensor_y.getValue() 
        # print(ground_cmd, current_ground_angle_x, ground.getVelocity(), (current_ground_angle_x-last_ground_angle_x)/15*1000, ground.getAcceleration())
    # print(ground_cmd_y, current_ground_angle_y)

    if count%5:
        sm[0] = current_ground_angle_x - last_ground_angle_x

    # if abs(current_ground_angle_x - last_ground_angle_x) < 5e-6:
        # print(ground_cmd - last_ground_cmd)
        # print("===================")


    # velocity control
    # ground.setPosition(float("inf"))
    # ground.setVelocity(ground_cmd)
    # print(f"{ground_cmd} {ground.getMaxVelocity()}")
    
    # current_time += timestep/1000
    current_time += timestep/1000
    # if current_time%2 > 1.5:
        # for i in range(1000):
            # robot.step(timestep)

    # print(f"ground count {count}")
    count += 1
    ground_change_counter += 1




    if sm_reset_flag[0]:
        # print("ground reset")
        # ground_x.setPosition(0)
        # ground_y.setPosition(0)
        current_time = 0
        last_ground_cmd = 0
        last_ground_angle_x = 0
        ground_change_counter = 0
        # ground_incline_dir = 1 if random.random() >= 0.5 else -1
        robot.step(45)
        # for i in range(100):
        #     robot.step(15)
