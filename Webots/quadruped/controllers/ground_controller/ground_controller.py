"""ground_controller controller."""

# You may need to import some classes of the controller module. Ex:
#  from controller import Robot, Motor, DistanceSensor
from controller import Robot
import numpy as np
import time
from multiprocessing import shared_memory


import sys
import os
import random
# get current file directory
path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, path + "\\..\\..\\..\\..\\")





# create the Robot instance.
robot = Robot()
ground = robot.getDevice("ground_motor")
ground_sensor = ground.getPositionSensor()


sm = shared_memory.ShareableList(name="ground_angle", sequence=[0.0, 0.0])


# get the time step of the current world.
timestep = int(robot.getBasicTimeStep())
print(timestep)
current_time = 0

ground_sensor.enable(timestep)

current_ground_angle_x, last_ground_angle_x = 0, 0
last_ground_cmd = 0

for i in range(50):
    robot.step(15)

sm_reset_flag = shared_memory.ShareableList(name="reset")
count = 0
ground_incline_dir = 1

while robot.step(timestep) != -1:
    ground_cmd = ground_incline_dir*np.pi*10/180*np.sin(1*current_time/2)

    last_ground_cmd = ground_cmd
    ground.setPosition(ground_cmd)
    last_ground_angle_x = current_ground_angle_x
    current_ground_angle_x = ground_sensor.getValue() 
    # print(ground_cmd, current_ground_angle_x, ground.getVelocity(), (current_ground_angle_x-last_ground_angle_x)/15*1000, ground.getAcceleration())
    # print(ground_cmd, current_ground_angle_x)

    if count%5:
        sm[0] = current_ground_angle_x - last_ground_angle_x

    # if abs(current_ground_angle_x - last_ground_angle_x) < 5e-6:
        # print(ground_cmd - last_ground_cmd)
        # print("===================")


    # velocity control
    # ground.setPosition(float("inf"))
    # ground.setVelocity(ground_cmd)
    # print(f"{ground_cmd} {ground.getMaxVelocity()}")
    
    # current_time += timestep/1000
    current_time += timestep/1000
    # if current_time%2 > 1.5:
        # for i in range(1000):
            # robot.step(timestep)

    # print(f"ground count {count}")
    count += 1

    if sm_reset_flag[0]:
        # print("ground reset")
        ground.setPosition(0)
        current_time = 0
        last_ground_cmd = 0
        last_ground_angle_x = 0
        
        ground_incline_dir = 1 if random.random() >= 0.5 else -1

        for i in range(100):
            robot.step(15)
