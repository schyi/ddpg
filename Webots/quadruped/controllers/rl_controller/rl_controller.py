"""robot_controller controller."""

# You may need to import some classes of the controller module. Ex:
#  from controller import Robot, Motor, DistanceSensor
import torch
from torch.functional import Tensor
from controller import Accelerometer
import numpy as np
import socket
import threading
import time
import math
from multiprocessing import shared_memory

import sys
import os
# get current file directory
path = os.path.dirname(os.path.abspath(__file__))
# put log 
sys.path.insert(0, path + "\\..\\..\\..\\..\\")
import log
import robot_leg_model
import quadruped_robot_gait
import quadruped_robot_controller
import ddpg_configuration
from RL import DDPGAgent, RewardManager, FunctionDDPG
from communication.tcp_udp import UDP
import matplotlib.pyplot as plt




GROUND_LEVEL = -39.9


class TrainConfiguration(ddpg_configuration.TrainingConfiguration):
    def __init__(self):
        super().__init__()
        self.hip_motor_init = 270
        self.upper_motor_init = 251
        self.lower_motor_init = 44
        self.init_motor_position = [[self.upper_motor_init, self.lower_motor_init],
                                    [self.upper_motor_init, self.lower_motor_init],
                                    [self.upper_motor_init, self.lower_motor_init],
                                    [self.upper_motor_init, self.lower_motor_init]
                                    ]
        self.init_hip_position = [self.hip_motor_init]*4
        self.action_range = 10
        self.lower_action_range = 20
        self.state_dim = 30
        self.action_motor = 8
        self.actor_learning_rate = 1e-6
        self.ciritic_learning_rate = 5e-6

        self.actor_network_shape = [
            self.state_dim, 128, 512, 1024, 2048, 1536, 512, self.action_motor
        ]
        self.ciritic_network_shape = [
            self.state_dim+self.action_motor, 128, 512, 1024, 2048, 1536, 512, 1
        ]
        
        self.reward_list = [
            # # "StaticInclineAngle",
            # "Gait",
            # "Work",
            "Movement",
            "JointSpeed",
            # "XBalance",
            # "YBalance",
            'Forward',
            # "Touch",
            "Torque",
            # "Zacc",
            # "EndPoint",
            "Goal",
        ]



class TrainParameters:
    def __init__(self):
        self.ground_change = [0, 0]
        self.current_supervise_rotation = [1, 0, 0, -1.57]
        self.new_supervise_rotation = [1, 0, 0, -1.57]
        self.current_total_incline_angle = 0.0
        self.new_total_incline_angle = 0.0
        self.current_robot_velocity = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.new_robot_velocity = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

        self.current_error_x = 0.0
        self.current_error_y = 0.0
        self.new_error_x = 0.0
        self.new_error_y = 0.0
        self.current_x = 0.0
        self.new_x = 0.0
        self.robot_y = 0.0
        self.action_count = 0

        self.touch_ground = 0.0
        self.current_torque = None
        self.new_torque = None
        self.current_action = None
        self.new_action = None
        self.end_point_velocity = None
        self.target = None
        self.position = None
        self.var = None

def socketThread(SharedVariable):
    server_ground = UDP(0)
    server_ground.setBlocking(True)
    server_ground.setTimeout(5)
    server_ground.bind("localhost", 60000)
    while True:
        SharedVariable.ground_pos_change = server_ground.recvObject()

def socketSupervisorGetDataThread(SharedVariable):
    server_super = UDP(1)
    server_super.setBlocking(False)
    server_super.setTimeout(5)
    server_super.bind("localhost", 60001)
    while True:
        SharedVariable.robot_rotation = server_super.recvObject()[0]
        SharedVariable.robot_velocity = server_super.recvObject()[0]
        SharedVariable.end_point_velocity = server_super.recvObject()[0]
        print(SharedVariable.robot_rotation)

def main():

    config = TrainConfiguration()
    train_parameters = TrainParameters()

    wb_robot = quadruped_robot_controller.QuadrupedRobotController()
    wb_robot.activateAllWebotsMotor(imu_timestep=1)
    wb_robot.createIMULog(200)

    agent = DDPGAgent(
        config.actor_network_shape,
        config.ciritic_network_shape,
        actor_learning_rate=config.actor_learning_rate,
        ciritic_learning_rate=config.ciritic_learning_rate,
        var_start=0.5,
        var_end=0.0001,
        var_decay=50000,
        gamma=0.66,
        tau=0.01,
    )
    agent.initDDPGLearning(
        memory_size=500,
        batch_size=64,
        designate_gpu_id=0
    )

    acc_client = UDP(int_data_pack_size=65535)
    acc_client.setBlocking(False)
    gyro_client = UDP(int_data_pack_size=65535)
    gyro_client.setBlocking(False)

    reward_manager = RewardManager(config.reward_list)

    wb_robot.executePosCmd(config.init_motor_position, config.init_hip_position)
    wb_robot.updateAllMotorData()
    for i in range(50):
        wb_robot.wb_robot.step(15)
    sm_rotation = shared_memory.ShareableList(name="robot_rotation")
    sm_ground = shared_memory.ShareableList(name="ground_angle")
    sm_velocity = shared_memory.ShareableList(name="robot_velocity")
    sm_endPoint = shared_memory.ShareableList(name="end_point_velocity")
    sm_reset_super_flag = shared_memory.ShareableList(name="reset")

    time_count = time.monotonic()
    loss = None
    total_actor_loss = 0
    total_ciritic_loss = 0
    total_reward = 0
    success_count = 0
    count = 1
    total_count = 1
    action_count = 1
    last_count = 0

    current_state = None
    new_state = None
    fail_flag = False
    action = last_action = None

    timestep = int(wb_robot.wb_robot.getBasicTimeStep())
    action_timestep = 50
    bool_do_action = True
    bool_in_learning = True

    current_state = FunctionDDPG.getState(wb_robot, sm_rotation[0], sm_rotation[1], last_action)
    
    while wb_robot.wb_robot.step(timestep) != -1:
        wb_robot.updateIMU()
        wb_robot.updateAllMotorData()
        if bool_do_action:
            action_count += 1
            action = agent.chooseAction(current_state, action_count)
            action = np.clip(np.random.normal(action.cpu(), agent.var), -1, 1)
            # print(action)

            upper = action[:4]*config.action_range + config.upper_motor_init
            lower = action[4:8]*config.lower_action_range + config.lower_motor_init
            # hip   = action[8:]*config.action_range + config.hip_motor_init
            motor_action = np.concatenate((upper, lower))
            # motor_action = np.concatenate((motor_action, hip))

            while True:
                try:
                    train_parameters.current_supervise_rotation = [i for i in sm_rotation]
                    train_parameters.ground_change = [i for i in sm_ground]
                    train_parameters.current_error_x = error_x = sm_rotation[0]
                    train_parameters.current_error_y = error_y = sm_rotation[1]
                    train_parameters.current_total_incline_angle = sm_rotation[3]
                    train_parameters.touch_ground = current_state[-4:]
                    train_parameters.current_robot_velocity = sm_velocity
                    train_parameters.current_torque = wb_robot.torque
                    train_parameters.current_action = current_state[:12]
                    train_parameters.current_x = sm_rotation[4]
                    break
                except ValueError:
                    print("Race condition happens when getting sm_rotation")

            joint_position = [
                [motor_action[0], motor_action[4]],
                [motor_action[1], motor_action[5]],
                [motor_action[2], motor_action[6]],
                [motor_action[3], motor_action[7]],
            ]

            # hip_position = [
            #     motor_action[8],
            #     motor_action[9],
            #     motor_action[10],
            #     motor_action[11],
            # ]
            hip_position = config.init_hip_position
            wb_robot.executePosCmd(joint_position, hip_position, dt=0.05)
            bool_do_action = False

        if count % action_timestep == 0:

            if not bool_do_action:
                wb_robot.updateAllMotorData()
                new_state = FunctionDDPG.getState(wb_robot, error_x, error_y, action)
                train_parameters.position = new_state[:12]
                while True:
                    try:
                        train_parameters.new_supervise_rotation = [i for i in sm_rotation]
                        train_parameters.ground_change = [i for i in sm_ground]
                        train_parameters.new_error_x = error_x = sm_rotation[0]
                        train_parameters.new_error_y = error_y = sm_rotation[1]
                        train_parameters.new_total_incline_angle = sm_rotation[3]
                        robot_position = sm_rotation[4]
                        train_parameters.robot_y = sm_rotation[5]
                        train_parameters.new_robot_velocity = sm_velocity
                        train_parameters.new_torque = wb_robot.torque
                        train_parameters.new_action = motor_action
                        train_parameters.new_x = robot_position
                        train_parameters.end_point_velocity = sm_endPoint
                        train_parameters.var = agent.var
                        train_parameters.action_count = action_count - last_count
                        break
                    except ValueError:
                        print("Race condition happens when getting sm_rotation")


                reward = reward_manager.excuReward(current_state, new_state, wb_robot, train_parameters)
                # print(f"reward = {reward}")
                total_reward += reward

                if reward_manager.dict_activated_reward["Fail"].reward != 0 or robot_position > 2.5:
                    if robot_position > 2.5:
                        success_count += 1
                        print(f"reset by goal count = {success_count}")
                    sm_reset_super_flag[0] = True
                    wb_robot.wb_robot.step(45)
                    # new_state = None
                    wb_robot.executePosCmd(config.init_motor_position, config.init_hip_position)
                    fail_flag = True

                if bool_in_learning:
                    transition = (current_state, action.tolist(), new_state, reward)
                    loss = agent.learning(transition)
                    if loss is not None: 
                        total_actor_loss += loss[1]
                        total_ciritic_loss += loss[0]
                else:
                    loss = None

                bool_do_action = True
                current_state = new_state
                
                if fail_flag:
                    current_state = FunctionDDPG.getState(wb_robot, sm_rotation[0], sm_rotation[1], last_action)
                    # print(f"count = {action_count - last_count}")
                    last_count = action_count
                    count = 1
                    bool_do_action = True
                    fail_flag = False

                if action_count%1000 == 0:
                    if loss is None:
                        print(f"Training run at {1000/(time.monotonic() - time_count)} Hz ; total decision: {action_count} ; var {agent.var}")
                    else:
                        print(f"Training run at {1000/(time.monotonic() - time_count):.2f} Hz ; total decision: {action_count:.2f} ; var {agent.var:.2f} ; actor loss {total_actor_loss/1000:.2f} ; ciritic loss {total_ciritic_loss/1000:.2f} ; reward {total_reward/1000:.2f}")
                        total_actor_loss = 0
                        total_ciritic_loss = 0
                        total_reward = 0
                    print(action)
                    time_count = time.monotonic()

        count += 1
        total_count += 1
    sm_rotation.shm.close()
    sm_ground.shm.close()
    sm_velocity.shm.close()
    print("robot memeory close")



if __name__ == "__main__":
    loss = main()