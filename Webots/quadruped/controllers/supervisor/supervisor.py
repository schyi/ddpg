from controller import Supervisor

from multiprocessing import shared_memory
import threading
import sys
import os
import math
# get current file directory
path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, path + "\\..\\..\\..\\..\\")




supervisor = Supervisor()


end_point_dict = ("lf_point", "rf_point", "lh_point", "rh_point")
end_point_node = []

quadruped_robot_node = supervisor.getFromDef("quadruped_robot")
[end_point_node.append(supervisor.getFromDef(i)) for i in end_point_dict]
# qr_velocity = quadruped_robot_node.getField("velocity")
# print(quadruped_robot_node.getId())
qr_translation = quadruped_robot_node.getField("translation")
qr_rotation = quadruped_robot_node.getField("rotation")
initial_rotation = qr_rotation.getSFRotation()
initial_velocity = quadruped_robot_node.getVelocity()

# print(initial_velocity)

sm = shared_memory.ShareableList(name="robot_rotation", sequence=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
sm_1 = shared_memory.ShareableList(name="robot_velocity", sequence=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
sm_2 = shared_memory.ShareableList(name="end_point_velocity", sequence=[0.0, 0.0, 0.0, 0.0])
sm_reset_flag = shared_memory.ShareableList(name="reset", sequence=[False])

for i in range(50):
    supervisor.step(15)
count = 0

timestep = int(supervisor.getBasicTimeStep())

while supervisor.step(timestep) != -1:
# for i in range(10000):
    # values = qr_translation.getSFVec3f()
    # values = qr_rotation.getSFRotation()
    # for i in range(len(sm)):
    #     sm[i] = values[i]

    # # sm.buf[:4] = bytearray(values)

    # # print(f"position: {values[0]} , {values[1]} , {values[2]}")
    # # print(f"rotation: {values[0]} , {values[1]} , {values[2]} , {values[3]}")
    # error_x = 1*abs(1 - values[0])
    # error_y = 1*abs(0 - values[1])
    # error_z = 1*abs(0 - values[2])
    # error_a = 1*abs(-1.57 - values[3])
    # print(error_x + error_y + error_z + error_a)
    # supervisor.step(15)
    if sm_reset_flag[0]:
        # print("supervisor reset")
        supervisor.simulationReset()
        for i in range(4):
            sm[i] = initial_rotation[i]
            sm_2[i] = 0.0
        sm[4] = qr_translation.getSFVec3f()[0]
        sm[5] = qr_translation.getSFVec3f()[1]
        for i in range(6):
            sm_1[i] = initial_velocity[i]

        sm_reset_flag[0] = False
        supervisor.step(45)
        # for i in range(100):
        #     supervisor.step(15)
        #     if i == 10:
        #         sm_reset_flag[0] = False

    if count % 5 == 0:
        o = qr_translation.getSFVec3f()
        v = quadruped_robot_node.getVelocity()
        axis_vector_matrix = quadruped_robot_node.getOrientation()
        axis_x_vector = axis_vector_matrix[0:3] + o
        axis_y_vector = axis_vector_matrix[3:6] + o
        axis_z_vector = axis_vector_matrix[6:] + o
        roll = -math.atan2(axis_z_vector[1], axis_z_vector[2]) - 0.5*math.pi
        pitch = math.atan2(axis_y_vector[0], axis_y_vector[2])
        yaw = -math.asin(axis_z_vector[0])
        # print(math.degrees(math.atan2(axis_z_vector[1], math.hypot(axis_z_vector[0], axis_z_vector[2]))))
        total_incline = math.atan2(math.hypot(axis_y_vector[1], axis_y_vector[0]), axis_y_vector[2])
        
        # roll = math.atan2(axis_y_vector[1], axis_y_vector[2]) # pitch
        # pitch = math.atan2(-axis_x_vector[2], axis_x_vector[0]) # roll
        # yaw = math.atan2(-axis_z_vector[0], -axis_z_vector[1]) # yaw
        # print(math.degrees(roll), math.degrees(pitch), math.degrees(yaw))
        for i in range(6):
            sm_1[i] = v[i]
        # print(sm_1)
        x, y, z = [
            math.cos(yaw)*math.cos(pitch),
            math.sin(yaw)*math.cos(pitch),
            math.hypot(math.sin(pitch) , math.sin(roll))
        ]
        total_angle = math.atan2(z, math.hypot(x, y))
        # print(math.degrees(total_angle))
        # print(math.degrees(roll), math.degrees(pitch), math.degrees(yaw), math.degrees(total_angle))
        # print(math.degrees(total_angle), math.degrees(total_incline))
        sm[0] = roll
        sm[1] = pitch
        sm[2] = yaw
        sm[3] = total_angle

        sm[4] = o[0]
        sm[5] = o[1]
        for i in range(4):
            sm_2[i] = end_point_node[i].getVelocity()[0]
    # print(f"super count {count}")
    count += 1
# print("close")
sm.shm.close()
sm.shm.unlink()
sm_1.shm.close()
sm_1.shm.unlink()
sm_2.shm.close()
sm_2.shm.unlink()
print("robot memeory close")