import socket
import pickle
import time
import traceback
import threading
import sys
import os

log_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, log_path + "\\..\\..\\")
import log



class TCP:
    msg_success = "success"
    def __init__(self, udp_id, log_object, int_data_pack_size=4096, peer_addr=None):
        '''
        udp_id : In upper layer, udp_id is used to identify which server/client is functioning or transmitting data\n
        log_object : the log system made by clement\n
        int_data_pack_size : the interget defince the data_pack size in UDP, please note the size of server and client should be the same\n
        ip_port : the default client/server peer to communicate, can be set later or not set (sendOject must have one)
        '''
        self.ID = udp_id
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.log = log_object
        self.data_pack_size = int(int_data_pack_size)
        self.addr = peer_addr
        self.client_or_server = "tcp_client"
        self.bind_addr = None

        self.timeout_second = None
        self.peer_sock = None
        self.bool_block = False
        self.bool_connect_flag = False
        self.connect_thread = threading.Thread(None)
        self.retry_times = 0
        self.max_retry = 5

    def bind(self, ip, port) -> None:
        '''
        This method will only be used by UDP server
        '''
        self.sock.bind(ip, port)
        self.bind_addr = (ip, port)
        self.sock.listen(1)
        self.client_or_server = "tcp_server"
        self.log.debug(f"{self.__class__.__name__} {self.ID} is now {self.client_or_server} with address {self.bind_addr}")



    def close(self) -> None:
        self.sock.close()
        if self.peer_sock is not None:
            self.peer_sock.close
        self.log.debug(f"{self.__class__.__name__} {self.client_or_server} {self.ID} has been closed")

    def connect(self, ip, port) -> None:
        try:
            self.sock.connect((ip, port))
            self.peer_sock = self.sock
        except TimeoutError:
            self.log.warning(f"{self.__class__.__name__} {self.client_or_server} {self.ID} connect to server has timeout")



    def restartSocket(self) -> None:
        if self.connect_thread.isAlive():
            self.bool_connect_flag = False
            self.connect_thread.join(10)
        self.sock.close()
        if self.peer_sock is not None:
            self.peer_sock.close()
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if self.timeout_second is not None:
            self.setTimeout(self.timeout_second)
        if self.bool_block:
            self.setBlocking(self.bool_block)
        if self.bind_addr is not None:
            self.sock.bind(self.bind_addr)


    def recvObject(self) -> object:

        receive_object_flag = True
        receive_object = b''
        while receive_object_flag:
            try:
                temp_receive_data, source_addr = self.sock.recvfrom(self.data_pack_size)
                receive_object += temp_receive_data
                if self.bool_block:
                    self.sock.sendto(self.msg_success.encode("utf-8"), source_addr)

                if len(temp_receive_data) < self.data_pack_size:
                    receive_object_flag = False
            except TimeoutError:
                receive_object_flag = self.__timeoutError(source_addr)
            except Exception as e:
                print(e)
                receive_object_flag = False
        try:
            if receive_object != b'' and receive_object != None:
                receive_object = pickle.loads(receive_object) 
                return receive_object, source_addr
        except:
            traceback.print_exc()

    def sendObject(self, objects=None, tuple_addr=None) -> bool:
        if objects is not None:
            if tuple_addr is not None:
                target_addr = tuple_addr
            elif self.addr is not None:
                target_addr = self.addr
            else:
                self.log.warning(f"No target specified for {self.__class__.__name__} {self.client_or_server} {self.ID}")
                return False

            temp_objects = pickle.dumps(objects)
            object_queue = []
            total_iteration = len(temp_objects)//self.data_pack_size
            
            for i in range(total_iteration):
                object_queue.append(temp_objects[ i*self.data_pack_size : (i+1)*self.data_pack_size])
            
            iteration = 0
            bool_send_object = True
            while iteration <= total_iteration and bool_send_object:
                try:
                    self.sock.sendto(object_queue[iteration], target_addr)
                    if self.bool_block:
                        self.sock.recv(256)
                    self.retry_times = 0
                except TimeoutError:
                    bool_send_object = self.__timeoutError(target_addr)
                except TypeError as err:
                    print(err)
                    return False
                except:
                    traceback.print_exc()
                    return False

        else:
            return False


    def setBlocking(self, bool_block, float_timeout_second=10.0) -> None:
        self.sock.setblocking(bool_block)
        self.bool_block = bool_block
        # Set a time out when block for fear of infinite block
        if self.bool_block:
            self.setTimeout(float_timeout_second) 

    def setDataPackSize(self, int_size) -> None:
        self.data_pack_size = int(int_size)

    def setDefaultPeerAddr(self, ip, port):
        self.addr = (ip, port)

    def setMaximumRetry(self, int_num):
        self.max_retry = int_num

    def setTimeout(self, float_seconds) -> None :
        self.sock.settimeout(float_seconds)
        self.timeout_second = float_seconds

    def __timeoutError(self, addr):
        self.retry_times += 1
        if self.retry_times >= self.max_retry:
            self.log.warning(f"{self.__class__.__name__} {self.client_or_server} {self.ID} \
                maximum retry reached, source addr : {addr}")
            return False
        else:
            return True

    def waitForConnection(self, max_connection=1):
        def acceptThread():
            self.bool_connect_flag = True
            while self.bool_connect_flag:
                try:
                    self.peer_sock, self.addr = self.sock.accept()
                    self.bool_connect_flag = False
                except socket.timeout:
                    print("timeout")
                except KeyboardInterrupt:
                    self.bool_connect_flag - False
    

        self.sock.listen(max_connection)
        self.connect_thread = threading.Thread(target=acceptThread())

        
