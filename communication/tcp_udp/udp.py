import socket
import pickle
import time
import traceback
import sys
import os

log_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, log_path + "\\..\\..\\")
import log



class UDP:
    msg_success = "success"
    def __init__(self, udp_id=0, log_object=None, int_data_pack_size=4096, peer_addr=None):
        '''
        udp_id : In upper layer, udp_id is used to identify which server/client is functioning or transmitting data\n
        log_object : the log system made by clement\n
        int_data_pack_size : the interget defince the data_pack size in UDP, please note the size of server and client should be the same\n
        ip_port : the default client/server peer to communicate, can be set later or not set (sendOject must have one)
        '''
        self.ID = udp_id
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.log = log_object
        self.data_pack_size = int(int_data_pack_size)
        self.addr = peer_addr
        self.client_or_server = "udp_client"
        self.bind_addr = None

        self.timeout_second = None
        self.bool_block = False
        self.retry_times = 0
        self.max_retry = 5

    def bind(self, ip, port) -> None:
        '''
        This method will only be used by UDP server
        '''
        self.sock.bind((ip, port))
        self.bind_addr = (ip, port)
        self.client_or_server = "udp_server"
        if self.log is not None:
            self.log.debug(f"{self.__class__.__name__} {self.ID} is now {self.client_or_server} with address {self.bind_addr}")



    def close(self) -> None:
        self.sock.close()
        if self.log is not None:
            self.log.debug(f"{self.__class__.__name__} {self.client_or_server} {self.ID} has been closed")

    def restartSocket(self) -> None:
        self.sock.close()
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        if self.timeout_second is not None:
            self.setTimeout(self.timeout_second)
        if self.bool_block:
            self.setBlocking(self.bool_block)
        if self.bind_addr is not None:
            self.sock.bind(self.bind_addr)



    def recvObject(self) -> object:
        source_addr = None
        receive_object_flag = True
        receive_object = b''
        while receive_object_flag:
            try:
                temp_receive_data, source_addr = self.sock.recvfrom(self.data_pack_size)
                receive_object += temp_receive_data
                if self.bool_block:
                    self.sock.sendto(self.msg_success.encode("utf-8"), source_addr)

                if len(temp_receive_data) < self.data_pack_size:
                    receive_object_flag = False
            except socket.timeout:
                if self.bool_block:
                    receive_object_flag = self.__timeoutError(source_addr)
                else:
                    pass
            except:
                traceback.print_exc()
                receive_object_flag = False
        try:
            if receive_object != b'' and receive_object != None:
                receive_object = pickle.loads(receive_object)
                self.addr = source_addr
                return receive_object, source_addr
        except pickle.UnpicklingError:
            traceback.print_exc()
            print(len(receive_object))
        except:
            traceback.print_exc()

    def sendObject(self, objects=None, tuple_addr=None) -> bool:
        if objects is not None:
            if tuple_addr is not None:
                target_addr = tuple_addr
            elif self.addr is not None:
                target_addr = self.addr
            else:
                if self.log is not None:
                    self.log.warning(f"No target specified for {self.__class__.__name__} {self.client_or_server} {self.ID}")
                return False

            temp_objects = pickle.dumps(objects)
            object_queue = []
            total_iteration = len(temp_objects)//self.data_pack_size
            
            for i in range(total_iteration + 1):
                object_queue.append(temp_objects[ i*self.data_pack_size : (i+1)*self.data_pack_size])
            
            iteration = 0
            bool_send_object = True
            while iteration <= total_iteration and bool_send_object:
                try:
                    self.sock.sendto(object_queue[iteration], target_addr)
                    if self.bool_block:
                        self.sock.recv(256)
                    self.retry_times = 0
                except TimeoutError:
                    if self.bool_block:
                        bool_send_object = self.__timeoutError(target_addr)
                except TypeError as err:
                    print(err)
                    return False
                except:
                    traceback.print_exc()
                    return False
                finally:
                    iteration += 1

        else:
            return False


    def setBlocking(self, bool_block, float_timeout_second=10.0) -> None:
        self.sock.setblocking(bool_block)
        self.bool_block = bool_block
        # Set a time out when block for fear of infinite block
        if self.bool_block:
            self.setTimeout(float_timeout_second) 

    def setDataPackSize(self, int_size) -> None:
        self.data_pack_size = int(int_size)

    def setDefaultPeerAddr(self, ip, port):
        self.addr = (ip, port)

    def setMaximumRetry(self, int_num):
        self.max_retry = int_num

    def setTimeout(self, float_seconds) -> None :
        self.sock.settimeout(float_seconds)
        self.timeout_second = float_seconds



    def __timeoutError(self, addr):
        self.retry_times += 1
        if self.retry_times >= self.max_retry:
            if self.log is not None:
                self.log.warning(f"{self.__class__.__name__} {self.client_or_server} {self.ID} \
                    maximum retry reached, source addr : {addr}")
            return False
        else:
            return True


class UDPClient(UDP):
    def __init__(self, ip, port, log=None, timeout=5, blocking=True):
        super().__init__(0, log)
        self.setBlocking(blocking)
        self.setTimeout(timeout)
        self.setDefaultPeerAddr(ip, port)


class UDPServer(UDP):
    def __init__(self, ip, port, log=None, timeout=5, blocking=True):
        super().__init__(0, log)
        self.setBlocking(blocking)
        self.setTimeout(timeout)
        self.bind(ip, port)


