import os, sys

from tcp_udp import UDP

log_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, log_path + "\\..\\")
import log


class UDPHandler:
    def __init__(self, log_level="info", log_file_level="debug"):
        self.log_level = log_level
        self.log_file_level = log_file_level
        self.log = log.LogHandler(self.__class__.__name__, __name__, self.log_level, self.log_file_level)
        
        self.client_dict = {}
        self.client_id_list = []
        self.client_id = 0
        self.server_dict = {}
        self.server_id_list = []
        self.server_id = 0

    def createClient(self, client_id = None, data_pack_size=4096, peer_addr=None) -> None:
        if client_id is not None and client_id in self.client_id_list:
            self.log.warning(f"Duplicate UDP ID {client_id}")
            return
        else:
            while self.client_id in self.client_id_list:
                self.client_id += 1
                client_id = self.client_id

        udp_client = UDP(self.client_id, self.log, data_pack_size, peer_addr)
        self.client_dict[client_id] = udp_client
        
    # def clientSendObject(self,)