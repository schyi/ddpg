import numpy as np

class TrainingConfiguration:
    def __init__(self):
        self.action_range = [-0.5, 0.5]
        self.action_range_x = [-0.5, 0.5]
        self.action_range_y = [-0.5, 0.5]
        self.action_resolution = 11

        self.reward_list = [
            "StaticInclineAngle",
            "TowardBalance",
        ]

        self.network_shape = [
            18, 40, 80, 160, 320, 320, 640, self.action_resolution
        ]

        # self.network_shape = [
        #     1, 5, 25, 100, 400, 400, 800, self.action_resolution
        # ]

        self.initial_learning_rate = 1e-3


        self.ground_level = -35
    

    # def generateActionSpace(self):
    #     self.action_space = np.linspace(self.action_range[0], self.action_range[1], self.action_resolution)
    #     print(self.action_space)

    def generateActionSpace(self):
        self.action_space_x = np.linspace(self.action_range_x[0], self.action_range_x[1], self.action_resolution)
        # print(self.action_space_x)
        self.action_space_y = np.linspace(self.action_range_y[0], self.action_range_y[1], self.action_resolution)
        # print(self.action_space_y)