import numpy as np
import sys
import math, random

class Gait():

    def __init__(self):
        self.numberOfPoint = 1
        self.GAIT = np.array([[0., -30.]])
        self.dt = 0.1 #0.1
        self.startPoint = self.GAIT[0]
        self.gait_name = None
        self.whole_gait_time = len(self.GAIT) * self.dt
        self.current_index = 0
        self.current_target = (0, -35)
        self.ground_info = None
        self.obstacle_info = None
        #Foot Moving Speed, should be calculated!!!
        self.Ground_Step = 0
        self.gait_on_ground = np.array([True])
        self.target_on_ground = False
        self.current_ground_angle = 0


    def generateGate(self, **kwargs):
        print("Gait not defined")
        sys.exit()

    def shiftGaitPoint(self, curPoint):
        x = curPoint[0] + self.startPoint[0]
        y = curPoint[1] + self.startPoint[1]
        return [x, y]

    def GaitLookUp(self, index, checking=False):
        """return (x(float) ,y(float) ,new_cycle(bool))"""
        self.current_index = index % self.numberOfPoint
        self.current_target = (
            self.GAIT[self.current_index, 0],
            self.GAIT[self.current_index, 1]
        )
        if self.ground_info is not None:self.target_on_ground = self.gait_on_ground[self.current_index]

        if self.ground_info is not None and self.target_on_ground:
            ground_index = np.argmin(np.hypot(
                self.ground_info.ground_x - self.current_target[0],
                self.ground_info.ground_y - self.current_target[1]
            ))
            self.current_ground_angle = self.ground_info.ground_angle_rad[ground_index]

        if self.current_index == 0:
            return self.current_target[0], self.current_target[1], True
        else:
            return self.current_target[0], self.current_target[1], False

    def generateVelocityLog(self):
        current_x = self.GAIT[:, 0]
        next_x = np.roll(current_x, -1)
        delta_x = next_x - current_x
        self.velocity_log = -delta_x / self.dt
    
    def getVelocity(self):
        if hasattr(self, "velocity_log"):
            return self.velocity_log[self.current_index]
        else:
            return 0
    
    def changeDt(self, dt):
        self.dt = dt

################################################

class Cycloid_Gait(Gait):

    def __init__(self, GLevel, StepL, resolution):
        super(Cycloid_Gait, self).__init__()
        self.generateGate(
            GLevel = GLevel,
            StepL = StepL,
            resolution = resolution
        )
        self.ground_info = Ground(GLevel, StepL, resolution)
        self.generateVelocityLog()
        self.whole_gait_time = len(self.GAIT) * self.dt

    def generateGate(self, **kwargs):
        GLevel = kwargs['GLevel']
        StepL = kwargs['StepL']
        resolution = kwargs['resolution']

        self.GAIT = np.zeros((resolution*4, 2), dtype=np.float)
        self.gait_on_ground = np.full(len(self.GAIT), 0, dtype=bool)

        Cy_Radius = StepL / (2*np.pi)
        X_Start = -StepL/2
        Y_Start = 0
        
        swipe_step = (2*np.pi)/(resolution)
        for i in range(0, resolution + 1):
            angle = i * swipe_step
            X = X_Start + Cy_Radius*(angle - math.sin(angle))
            Y = Y_Start + Cy_Radius*(1 - math.cos(angle))
            self.GAIT[i] = np.array([X,Y])

        Ground_Range = [self.GAIT[0, 0], self.GAIT[resolution, 0]]
        Ground_Step = (Ground_Range[1] - Ground_Range[0])/(3*resolution)
        for i in range(resolution + 1, 4*resolution):
            #X_Ground = Ground_Range[0,0] + (i-resolution)*Ground_Step
            X_Ground = self.GAIT[i - 1, 0] - Ground_Step
            self.GAIT[i] = np.array([X_Ground, self.GAIT[0, 1]])
            self.gait_on_ground[i] = True
        Gait_Shift = GLevel - self.GAIT[0, 1]
        self.GAIT[:, 1] = self.GAIT[:, 1] + Gait_Shift
        self.numberOfPoint = len(self.GAIT)
        self.Ground_Step = Ground_Step

class UpDownGait(Gait):
    def __init__(self, ground_level, step_long, resolution):
        super().__init__()
        self.ground_level = ground_level
        self.step_long = step_long
        self.resolution = resolution
        self.height = 3
        self.generateGate(ground_level, resolution)

    def generateGate(self, ground_level, resolution):
        x = np.zeros(resolution)-2
        y = np.append(
            np.linspace(ground_level, ground_level + self.height, int(resolution/2)),
            np.linspace(self.height + ground_level, ground_level, int(resolution/2))
        )
        self.GAIT = np.stack((x, y), axis=-1)
        self.numberOfPoint = len(self.GAIT)



class Ground(object):
    ''' Baseic flat ground'''
    def __init__(self, ground_level, step_long, resolution):
        self.ground_level = ground_level
        self.x_increase   = step_long / (3*resolution)
        self.ground_x = np.arange(-30, 30, self.x_increase)
        self.ground_y = np.ones(len(self.ground_x)) * self.ground_level
        self.ground_angle_rad = np.full(len(self.ground_x), 0, dtype=float)
        # self.hovering_shift = hovering_shift

    def getGroundInfo(self):
        return self.ground_x, self.ground_y

class Ground_Line(Ground):
    def __init__(self, ground_level, step_long, resolution, gait_object):
        super().__init__(ground_level, step_long, resolution)
        self.start_point = gait_object.start_point
        self.end_point = gait_object.end_point
        self.GAIT = gait_object
        self.ground_x = np.linspace(self.start_point[0], self.end_point[0], resolution)
        self.ground_y = np.linspace(self.start_point[1], self.end_point[1], resolution)
        slope = (self.end_point - self.start_point)[0] / (self.end_point - self.start_point)[1]
        self.ground_angle_rad = np.full(
            len(self.ground_x),
            (0.5*np.pi - np.arctan(slope))%(2*np.pi)
        )




def plotGAIT():
    Gait = Cycloid_Gait(GLevel = -30, StepL = 20, resolution = 30)
    print(Gait.whole_gait_time)
    XL = list()
    YL = list()
    for index in range(Gait.numberOfPoint):
        X, Y = Gait.GaitLookUp(index)
        XL.append(X)
        YL.append(Y)

    import matplotlib.pyplot as plt
    gait_axis = (
        min(XL) - 3,
        max(XL) + 3,
        min(YL) - 3,
        max(YL) + 3
    )
    plt.figure()
    for index in range(len(XL)):
        plt.cla()
        plt.axis(gait_axis)
        plt.plot(XL[:index], YL[:index])
        plt.ion()
        plt.show()
        plt.pause(Gait.dt)


def testGround():
    G = Ground(-30,10,30)
    print(G.__class__)


if __name__ == "__main__":
    # test_ground = Ground_Line()
    # print(test_ground.getGroundInfo())
    # gait = Line_Gait(-30,10,30)
    # print(gait.ground_info.getGroundInfo())
    # gait = Cycloid_Gait(-30, 15, 30)
    # print(gait.ground_info.ground_x)
    # g_info = gait.ground_info
    # index = np.where(g_info == gait.GAIT)
    # print(gait.GAIT[:, 0])
    # print(index)
    # gait.GaitLookUp(50)
    # print(gait.target_on_ground)
    # print(gait.current_ground_angle)
    pl = plotGAIT()