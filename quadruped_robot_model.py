import sys
import numpy as np
import time
import threading
import multiprocessing

from numpy.core.records import array



import robot_leg_model
import log
try:
    import DXL_motor_control as DXL
    from DXL_motor_control import DXL_Conmunication
    HAS_DXL_SDK = True
except ModuleNotFoundError:
    HAS_DXL_SDK = False

try:
    from controller import Robot
    BOOL_HAS_WEBOTS = True
except ModuleNotFoundError:
    BOOL_HAS_WEBOTS = False


try:
    from IMU import lsm9ds1
    BOOL_HAS_IMU = True
except ModuleNotFoundError:
    BOOL_HAS_IMU = False

class QuadrupedRobot:
    '''
    This class manage the operation of individual foot and hip motor
    of quadruped robot
    '''
    def __init__(self, log_level="info", log_file_level="debug"):
        '''
            Initial parameters
        '''
        self.log_level = log_level
        self.log_file_level = log_file_level
        self.log = log.LogHandler(self.__class__.__name__, __name__, self.log_level, log_file_level)
        self.log_len = 500
        self.bool_dxl_connected = False
        self.bool_real_motor_mode = False
        self.bool_webots_motor_mode = False
        self.bool_need_write = False
        self.str_motor_operating_mode = "p"

        self.acc_x = 0
        self.acc_y = 0
        self.acc_z = 0
        self.gyro_x = 0
        self.gyro_y = 0
        self.gyro_z = 0
        self.posture_x = 0
        self.posture_y = 0
        self.posture_z = 0
        self.touch_1 = 0
        self.touch_2 = 0
        self.touch_3 = 0
        self.touch_4 = 0

        if BOOL_HAS_WEBOTS:
            self.wb_robot = Robot()
        else:
            self.wb_robot = None
        self.wb_acc = None
        self.wb_gyro = None
        self.wb_touch_1 = None
        self.wb_touch_2 = None
        self.wb_touch_3 = None
        self.wb_touch_4 = None
        self.torque = None
        self.velocity = None
        self.gait_target = None

        self.imu = None
        self.thread_update_imu = None

        self.leg_lf = robot_leg_model.LegLF()
        self.leg_rf = robot_leg_model.LegRF()
        self.leg_lh = robot_leg_model.LegLH()
        self.leg_rh = robot_leg_model.LegRH()


        self.leg_system = [self.leg_lf, self.leg_rf, self.leg_lh, self.leg_rh]
        self.lower_system = [self.leg_lf, self.leg_rf, self.leg_lh, self.leg_rh]

        self.event_read = threading.Event()
        self.event_write = threading.Event()
        self.event_terminate_thread = threading.Event()
        self.event_terminate_imu = threading.Event()

    def activateAllRealMotor(self, device_name='/dev/ttyUSB0') -> None:
        '''
            Activate all dxl motor
            No return
        '''
        if not self.bool_dxl_connected:
            self.activateDXLConnection(device_name)
        
        if self.bool_dxl_connected:
            for system in self.lower_system:
                if not system.bool_real_motor_mode:
                    system.activateRealMotor(device_name, dxl_communicator=self.dxl_communicator, need_reboot=False)
                    self.bool_real_motor_mode = True
            self.rebootAllMotor()
            self.switchMotorMode("position")
            self.setHipPID(5000, 200, 0)
            # self.setLegPID(1000, 50, 0)
            self.dxl_communicator.activateIndirectMode()
            self.event_read.set()
            self.event_terminate_thread.clear()
            self.event_write.clear()
            self.thread_update_motor_data = threading.Thread(
                target=self._threadUpdateAllMotorData,
                daemon=True
            )
            self.thread_update_motor_data.start()
            time.sleep(1)

    def activateDXLConnection(self, device_name='/dev/ttyUSB0'):
        '''
            Establish connection to DXL controller
        '''
        if HAS_DXL_SDK:
            if self.bool_dxl_connected:
                pass
            else:
                if not hasattr(self, "dxl_communicator"):
                    self.dxl_communicator = DXL.DXL_Conmunication(device_name, log_level=self.log_level, log_file_level=self.log_file_level)
                self.dxl_communicator.activateDXLConnection()
                self.bool_dxl_connected = True
        else:
            self.log.error("\nPlease Install Dynamixel SDK first to activate DXL motor")
            self.log.error("Follow the instruction : https://github.com/ROBOTIS-GIT/DynamixelSDK\n")
        
        if BOOL_HAS_IMU:
            self.imu = lsm9ds1.Lsm9ds1()
            if self.imu.testDevice():
                self.imu.initialize()

    def activateIMUThread(self):
        self.event_terminate_imu.clear()
        self.thread_update_imu = threading.Thread(target=self._threadUpdateIMUData, daemon=True)
        self.thread_update_imu.start()


    def activateAllWebotsMotor(self, webots_timestep=15, imu_timestep=15):
        if BOOL_HAS_WEBOTS:
            if not self.bool_webots_motor_mode:
                self.wb_acc = self.wb_robot.getDevice("acc_1")
                self.wb_acc.enable(imu_timestep)
                self.wb_gyro = self.wb_robot.getDevice("gyro_1")
                self.wb_gyro.enable(imu_timestep)
                self.wb_touch_1 = self.wb_robot.getDevice("touch_1")
                self.wb_touch_1.enable(imu_timestep)
                self.wb_touch_2 = self.wb_robot.getDevice("touch_2")
                self.wb_touch_2.enable(imu_timestep)
                self.wb_touch_3 = self.wb_robot.getDevice("touch_3")
                self.wb_touch_3.enable(imu_timestep)
                self.wb_touch_4 = self.wb_robot.getDevice("touch_4")
                self.wb_touch_4.enable(imu_timestep)

                self.webots_timestep = webots_timestep
                for leg in self.lower_system:
                    success = leg.activateWebotsMotor(self.wb_robot, webots_timestep)
                    if success:
                        self.log.debug(f"{leg.__class__.__name__} webots motor successfully activated")
                    else:
                        self.log.warning(f"{leg.__class__.__name__} webots could not activate.")
                        self.log.warning(f"All activated process halt, deactivate all webots motor.")
                        self.deactivateAllWebotsMotor()
                        break
                if success:
                    self.bool_webots_motor_mode = True
                    self.updateAllMotorData()
        else:
            print("This can only be used in Webots simulator")


    def armAllMotor(self):
        '''
            Power all motors
            process : check connection >>> check activate >>> true: arm motor
        '''
        if self.bool_dxl_connected:
            self.eventWaitUntilWrite()
            for system in self.lower_system:
                if system.bool_real_motor_mode:
                    system.armAllMotor()
            self.eventStartRead()

    def createIMULog(self, length=None):
        if length is not None:
            self.acc_x_log = np.zeros(length, dtype=np.double)
            self.acc_y_log = np.zeros(length, dtype=np.double)
            self.acc_z_log = np.zeros(length, dtype=np.double)
            self.gyro_x_log = np.zeros(length, dtype=np.double)
            self.gyro_y_log = np.zeros(length, dtype=np.double)
            self.gyro_z_log = np.zeros(length, dtype=np.double)
        else:
            self.acc_x_log = np.zeros(200, dtype=np.double)
            self.acc_y_log = np.zeros(200, dtype=np.double)
            self.acc_z_log = np.zeros(200, dtype=np.double)
            self.gyro_x_log = np.zeros(200, dtype=np.double)
            self.gyro_y_log = np.zeros(200, dtype=np.double)
            self.gyro_z_log = np.zeros(200, dtype=np.double)


    def cutOffLog(self, num):
        for system in self.lower_system:
            system.cutOffLog(num)


    def disarmAllMotor(self):
        '''
            Cut the power for motor
            process : check connection >>> check if arm >>>true: disarm
        '''
        if self.bool_dxl_connected:
            self.eventWaitUntilWrite()
            for system in self.lower_system:
                if system.bool_motor_armed:
                    system.disarmAllMotor()
            self.eventStartRead()

    def deactivateDXLConnection(self):
        '''
            Cut connection of DXL controller
            process : check connection >>> true: disarm all motor, close port, change bool_real_motor_mode tage
        '''
        if self.bool_dxl_connected:
            self.event_terminate_thread.set()
            if self.thread_update_motor_data.is_alive():
                time.sleep(0.05)
            self.disarmAllMotor()
            for system in self.lower_system:
                system.deactivateRealMotor()
            time.sleep(0.2)
            self.dxl_communicator.closeHandler()
            time.sleep(0.1)
            self.bool_dxl_connected = False
        
        if BOOL_HAS_IMU:
            if self.imu is not None:
                self.imu.powerOff()

    def deactivateAllWebotsMotor(self):
        for leg in self.lower_system:
            leg.deactivateWebotsMotor()
        self.bool_webots_motor_mode = False


    def deactivateIMUThread(self):
        self.event_terminate_imu.set()



    def endPointCalculation(self):
        '''
            Calculate all end point on all foot and hip
        '''
        for system in self.lower_system:
            system.endPointCalculation()

    def estimatePosture(self, length=6):
        if hasattr(self, "acc_x_log"):
            length = length if not length > len(self.acc_x_log) else len(self.acc_x_log)
            average_acc_x = np.average(self.acc_x_log[:length])
            average_acc_y = np.average(self.acc_y_log[:length])
            average_acc_z = np.average(self.acc_z_log[:length])
            std_acc_x = np.std(self.acc_x_log[:length])
            std_acc_y = np.std(self.acc_y_log[:length])
            std_acc_z = np.std(self.acc_z_log[:length])
            filtered_acc_x = np.array([i for i in self.acc_x_log[:length] if i <= average_acc_x + std_acc_x and i >= average_acc_x - std_acc_x])
            filtered_acc_y = np.array([i for i in self.acc_y_log[:length] if i <= average_acc_y + std_acc_y and i >= average_acc_y - std_acc_y])
            filtered_acc_z = np.array([i for i in self.acc_z_log[:length] if i <= average_acc_z + std_acc_z and i >= average_acc_z - std_acc_z])
            self.posture_x = float(np.average(filtered_acc_x))
            self.posture_y = float(np.average(filtered_acc_y))
            self.posture_z = float(np.average(filtered_acc_z))
            # print(filtered_acc_y)


    def executeAllIK(self, cmd_x, cmd_y, dt=0.05, bool_dynamic=False, bool_update_log=True):
        '''
            Control 4 feet with IK
        '''
        for leg in self.leg_system:
            leg.inverseKinematic(cmd_x, cmd_y, dt, dynamic=bool_dynamic, send_command=False)

        if self.bool_dxl_connected:
            self.writeRealMotorCmd(dt)
        elif self.bool_webots_motor_mode:
            self.wb_robot.step(self.webots_timestep)
            self.updateAllMotorData(bool_update_log)


    def executeIK(self,
        lf_cmd=None, rf_cmd=None, lh_cmd=None, rh_cmd=None,
        lf_pos=None, rf_pos=None, lh_pos=None, rh_pos=None,
        dt=0.05, need_log=True, need_delay=True, **kwargs):
        '''
            For safty operation, please use keyword "lf_cmd", "rf_cmd", "lh_cmd", "rh_cmd", "hip_cmd"\n
            as keyword to use this method. Each keyword value should be a list of [X_cmd, Y_cmd]\n
            For should_cmd should be a "lf_pos", "rf_pos", "lh_pos", "rh_pos" \n
            If not intend to move some hip joint, then just leave it as None. For example [None, 250, None, 290]\n
            then only fr, rr hip will move.
        '''
        leg_cmd = [lf_cmd, rf_cmd, lh_cmd, rh_cmd]
        hip_cmd = [lf_pos, rf_pos, lh_pos, rh_pos]
        for leg, cmd in zip(self.leg_system, leg_cmd):
            if cmd is not None:
                leg.inverseKinematic(cmd[0], cmd[1], dt, need_log, send_command=False)

        for leg, cmd in zip(self.leg_system, hip_cmd):
            if cmd is not None:
                leg.setHipPositionCmd(cmd, dt, need_log, send_command=False)


        self.log.debug(f"leg command {[lf_cmd, rf_cmd, lh_cmd, rh_cmd, lf_pos, rf_pos, lh_pos, rh_pos]} dt={dt}", self.executeIK)

        if self.bool_dxl_connected:
            self.writeRealMotorCmd(dt, need_delay)
        elif self.bool_webots_motor_mode:
            if need_delay:
                # self.wb_robot.step(int(dt*1000))
                self.wb_robot.step(self.webots_timestep)
            self.updateAllMotorData(need_log)

    def executePosCmd(self, joint_position, hip_position=[0, 0, 0, 0], dt=0.05):
        for i, leg in enumerate(self.leg_system):
            leg.setPositionCmd(p_cmd=joint_position[i], dt=dt, send_command=False, offset=False)
            leg.setHipPositionCmd(hip_position[i], dt=dt, send_command=False)

        if self.bool_dxl_connected:
            self.writeRealMotorCmd(dt)

    def getFootEndPointPath(self, x_cmd, y_cmd, end_point, resolution) -> array:
        '''
            Return path from current EndPoint to target cmd, with resolution (2D [[x...], [y...]])
        '''
        path = np.stack([
            np.linspace(end_point[0], x_cmd, num=resolution),
            np.linspace(end_point[1], y_cmd, num=resolution)
        ])
        return path
    
    def getHipPath(self, cmd, current_pos, resolution) -> array:
        '''
            Return path from current pos to target pos with resolution (1D array)
        '''
        path = np.linspace(current_pos, cmd, num=resolution)
        return path

    def rebootAllMotor(self):
        '''
            Reboot all DXL motor
        '''
        if self.bool_dxl_connected:
            self.eventWaitUntilWrite()
            self.dxl_communicator.rebootAllMotor()
            self.eventStartRead()

    def reinitialAllMotor(self):
        if self.bool_dxl_connected:
            self.eventWaitUntilWrite()
        for system in self.lower_system:
            system.reinitial()
        self.eventStartRead()

    def resetLog(self):
        for system in self.lower_system:
            system.resetLog()

    def resetAccLog(self):
        if hasattr(self, "acc_x_log"):
            self.acc_x_log = np.zeros(len(self.acc_x_log), dtype=np.double)
            self.acc_y_log = np.zeros(len(self.acc_y_log), dtype=np.double)
            self.acc_z_log = np.zeros(len(self.acc_z_log), dtype=np.double)

    def resetLoglength(self, log_len):
        '''
            Reset data log length in all motors
        '''
        for system in self.lower_system:
            system.resetLoglength(log_len)

    def resetToStandup(self):
        self.slowlySetAllMotorPositionIK(0, -35, 270)
    
    def resetToSitDown(self):
        self.slowlySetAllMotorPositionIK(8, -16)

    def setLogLevel(self, log_level="info", log_file_level="debug"):
        '''
            Set current log level to target level
        '''
        self.log.setLogLevel(log_level, log_file_level)
        self.log_level = log_level
        self.log_file_level = log_file_level
        if hasattr(self, "dxl_communicator"):
            self.dxl_communicator.setLogLevel(log_level, log_file_level)

    def setLegPID(self, p, i, d):
        if self.bool_real_motor_mode:
            self.eventWaitUntilWrite()
            for leg in self.leg_system:
                leg.setLegPID(p, i, d)
            self.eventStartRead()

    def setHipPID(self, p, i, d):
        if self.bool_real_motor_mode:
            self.eventWaitUntilWrite()
            for leg in self.leg_system:
                leg.setHipPID(p, i, d)
            self.eventStartRead()


    def slowlyPosition(self,
        lf_cmd=None, rf_cmd=None, lh_cmd=None, rh_cmd=None,
        lf_pos=None, rf_pos=None, lh_pos=None, rh_pos=None,
        resolution=50, dt=0.05, need_log=True, **kwargs):
        '''
            For safty operation, please use keyword "lf_cmd", "rf_cmd", "lh_cmd", "rh_cmd", "hip_cmd"\n
            as keyword to use this method. Each keyword value should be a list of [X_cmd, Y_cmd]\n
            For should_cmd should be a "lf_pos", "rf_pos", "lh_pos", "rh_pos" \n
            If not intend to move some hip joint, then just leave it as None. For example [None, 250, None, 290]\n
            then only fr, rr hip will move.
        '''
        leg_cmd = [lf_cmd, rf_cmd, lh_cmd, rh_cmd]
        hip_cmd = [lf_pos, rf_pos, lh_pos, rh_pos]
        endpoint_trajectory = [None, None, None, None] # [lf_path, rf_path, lh_path, rh_path]
        hip_trajectory = [None, None, None, None] # [lf_hip_path, rf_hip_path, lh_hip_path, rh_hip_path]
        if self.bool_webots_motor_mode:
            self.updateAllMotorData()
            for i in range(100):
                self.wb_robot.step(15)
        else: self.endPointCalculation()
        for i, cmd, leg in zip(range(len(leg_cmd)), leg_cmd, self.leg_system):
            if cmd is not None:
                endpoint_trajectory[i] = self.getFootEndPointPath(*cmd, leg.EndPoint, resolution)

        for i, cmd, leg in zip(range(len(hip_cmd)), hip_cmd, self.leg_system):
            if cmd is not None:
                hip_trajectory[i] = self.getHipPath(cmd, leg.hip_motor.getPosition("deg"), resolution)

        self.log.debug(f"leg command {[lf_cmd, rf_cmd, lh_cmd, rh_cmd, lf_pos, rf_pos, lh_pos, rh_pos]}", self.slowlyPosition)

        for i in range(resolution):
            for cmd, trajectory, leg in zip(leg_cmd, endpoint_trajectory, self.leg_system):
                if cmd is not None:
                    if not leg.checkWorkingSpace(trajectory[0][i], trajectory[1][i]):
                        leg.inverseKinematic(trajectory[0][i], trajectory[1][i], dt, need_log, send_command=False)

            for cmd, trajectory, leg in zip(hip_cmd, hip_trajectory, self.leg_system):
                if cmd is not None:
                    leg.setHipPositionCmd(trajectory[i], dt, need_log, send_command=False)

            if self.bool_dxl_connected:
                self.writeRealMotorCmd(dt)
            elif self.bool_webots_motor_mode:
                self.wb_robot.step(self.webots_timestep)
                self.updateAllMotorData(need_log)

    def slowlySetAllMotorPositionIK(self, x_cmd=0, y_cmd=-32, hip_cmd=None, resolution=100, dt=0.05, need_log=True):
        '''
            Reset all motor position to its initial pos\n
            argv : x_cmd , y_cmd : X,Y position of the foot end point\n
                hip cmd (deg) global angle of hip (270 is downward)
        '''
        self.endPointCalculation()
        endpoint_trajectory = [None, None, None, None]
        hip_trajectory = [None, None, None, None]
        for i, leg in zip(range(len(self.leg_system)), self.leg_system):
            endpoint_trajectory[i] = self.getFootEndPointPath(x_cmd, y_cmd, leg.EndPoint, resolution)
            if hip_cmd is not None:
                hip_trajectory[i] = self.getHipPath(hip_cmd, leg.hip_motor.getPosition("deg"), resolution)

        for i in range(resolution):
            for trajectory, h_trajectory, leg in zip(endpoint_trajectory, hip_trajectory, self.leg_system):
                if not leg.checkWorkingSpace(trajectory[0][i], trajectory[1][i]):
                    leg.inverseKinematic(trajectory[0][i], trajectory[1][i], dt, need_log, send_command=False)
                if h_trajectory is not None:
                    leg.setHipPositionCmd(h_trajectory[i], dt, need_log, send_command=False)

            if self.bool_dxl_connected:
                self.writeRealMotorCmd(dt)
            elif self.bool_webots_motor_mode:
                self.wb_robot.step(self.webots_timestep)
                self.updateAllMotorData(need_log)

    def switchMotorMode(self, mode='position'):
        '''
            Switch motor command mode : "position" or "velocity"
        '''
        if self.bool_dxl_connected:
            self.eventWaitUntilWrite()
            for system in self.lower_system:
                if system.bool_real_motor_mode:
                    system.switchMotorMode(mode)
            self.eventStartRead()

        if mode == "position":
            self.str_motor_operating_mode = "p"
        else:
            self.str_motor_operating_mode = "v"


    def updateTorque(self):
        self.torque = []
        # self.velocity = []
        for system in self.lower_system:
            self.torque.append(system.getTorque())
            # [self.velocity.append(system.getVelocity())]
        #print(np.max(np.abs(self.torque)))

    def updateIMU(self):
        success = False
        if BOOL_HAS_WEBOTS:
            if self.wb_acc is not None:
                acc = self.wb_acc.getValues()
                self.acc_x = acc[0]
                self.acc_y = acc[1]
                self.acc_z = acc[2]
                gyro = self.wb_gyro.getValues()
                self.gyro_x = gyro[0]
                self.gyro_y = gyro[1]
                self.gyro_z = gyro[2]
                self.touch_1 = self.wb_touch_1.getValue()
                self.touch_2 = self.wb_touch_2.getValue()
                self.touch_3 = self.wb_touch_3.getValue()
                self.touch_4 = self.wb_touch_4.getValue()

                # print(self.touch_1, self.touch_2, self.touch_3, self.touch_4)
                success = True
            else:
                self.log.warning("Webos accelerometer did not initialized")
        elif BOOL_HAS_IMU:
            if self.imu is not None:
                acc = self.imu.readAccData()
                self.acc_x = acc[0]
                self.acc_y = acc[1]
                self.acc_z = acc[2]
                gyro = self.imu.readGyroData()
                self.gyro_x = gyro[0]
                self.gyro_y = gyro[1]
                self.gyro_z = gyro[2]
                self.touch_1 = self.wb_touch_1.getValue()
                self.touch_2 = self.wb_touch_2.getValue()
                self.touch_3 = self.wb_touch_3.getValue()
                self.touch_4 = self.wb_touch_4.getValue()
                success = True
            else:
                self.log.warning("Lsm9ds1 did not initialized")

        if success:
            if hasattr(self, "acc_x_log"):
                self.acc_x_log[1:] = self.acc_x_log[:-1]
                self.acc_x_log[0] = self.acc_x
                self.acc_y_log[1:] = self.acc_y_log[:-1]
                self.acc_y_log[0] = self.acc_y
                self.acc_z_log[1:] = self.acc_z_log[:-1]
                self.acc_z_log[0] = self.acc_z
                self.gyro_x_log[1:] = self.gyro_x_log[:-1]
                self.gyro_x_log[0] = self.gyro_x
                self.gyro_y_log[1:] = self.gyro_y_log[:-1]
                self.gyro_y_log[0] = self.gyro_y
                self.gyro_z_log[1:] = self.gyro_z_log[:-1]
                self.gyro_z_log[0] = self.gyro_z
                # self.updateTorque()



    def _threadUpdateIMUData(self, real_delay=0.005):
        '''
            Keep updating IMU data with desire delay (real_delay only works in real world)
        '''
        while not self.event_terminate_imu.is_set():
            self.updateIMU()
            
            if self.bool_real_motor_mode:
                time.sleep(real_delay)
            elif self.bool_webots_motor_mode:
                # self.wb_robot.step(self.webots_timestep)
                print("imu update")
                pass
            else:
                time.sleep(0.1)


    def _threadUpdateAllMotorData(self):
        '''
            This handler will keep getting real motor data when activated.\n
            And will block if there is other things to do. \n
            The control method is based on event driven mode for efficiency. \n

            control logic:
            if not write & read is true -> read
            if want to write: clear read -> write will set and  real will wait until read is set
        '''
        event_trigger = threading.Event()
        count_time = time.time()
        iteration = 0
        while not self.event_terminate_thread.is_set():
            if not self.event_write.is_set() and self.event_read.is_set():
                event_trigger.wait(0.005)
                if self.bool_need_write:
                    self.dxl_communicator.sentAllCmd()
                    self.bool_need_write = False
                self.updateAllMotorData()
                iteration += 1
                event_trigger.clear()
                
            else:
                self.event_write.set()
                self.event_read.wait()
                self.event_write.clear()
            

            if time.time() - count_time >= 1:
                print("freq : {:0.2f}".format(iteration/(time.time()-count_time)))
                iteration = 0
                count_time = time.time()






    def updateAllMotorData(self, delay=-1, bool_update_log=True):
        '''
        delay = Extra delay for communication, -1 means no delay.
        Update real motor info and check for interference and calculate end point location
        
            Modified by Clement, add update_noew option. if false means we do update data manually.
        '''
        if self.bool_dxl_connected:
            self.dxl_communicator.updateMotorData(delay=delay)
            for leg in self.lower_system:
                if leg.bool_real_motor_mode:
                    leg.updateRealMotorsInfo(update_now=False)
            # self.endPointCalculation()
        elif self.bool_webots_motor_mode:
            for leg in self.lower_system:
                leg.updateMotorData(bool_update_log)
                # pass
            # self.endPointCalculation()
            self.updateTorque()
        else:
            # self.log.warning("Not connect to DXL or Webots yet", self.updateAllMotorData)
            # self.log.warning("Use activateDXLConnection() or activateWebotsMotor() first", self.updateAllMotorData)
            pass

    def eventWaitUntilWrite(self):
        if hasattr(self, "thread_update_motor_data"):
            if self.thread_update_motor_data.is_alive():
                self.event_read.clear()
                self.event_write.wait(0.5)


    def eventStartRead(self):
        self.event_write.clear()
        self.event_read.set()

    def writeRealMotorCmd(self, dt, need_delay=True):
        self.bool_need_write = True
        if need_delay: time.sleep(dt)


