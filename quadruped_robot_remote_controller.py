import numpy as np
import time
import threading
import traceback

import inputs

import quadruped_robot_model
import quadruped_robot_gait
import quadruped_robot_controller
from communication.tcp_udp import UDPServer, UDPClient
import xbox_one
import log

GROUND_LEVEL = -37


class QuadrupedRobotRemoteController(quadruped_robot_controller.QuadrupedRobotController):
    def __init__(self, log_level="info", log_file_level="debug"):
        self.log_level = log_level
        self.log_file_level = log_file_level
        self.log = log.LogHandler(self.__class__.__name__, __name__, self.log_level, self.log_file_level)
        self.gait_phase = 0.5 # 50%
        self.gait = None
        self.xbox_one = xbox_one.XboxOne()
        self.xbox_command = ""
        self.udp_command = ""
        super().__init__(self.log_level, self.log_file_level)

        self.bool_keep_execute_gait = False
        self.bool_robot_stand = False
        self.run_flag = False

        self.udp_server = UDPServer("10.10.10.2", 60005, self.log, blocking=False)

        self.hip = [272, 272, 272, 272]
        self.dt = 0.015
        self.resolution = 14

        self.gait_forward = quadruped_robot_gait.TrotForwardGait(
            ground_level = -37,
            step_long = 6,
            resolution = self.resolution,
            origin = -3,
            height = 4,
            hip = self.hip,
            dt = self.dt
        )
        self.gait_backward = quadruped_robot_gait.TrotBackwardGait(
            ground_level = -37,
            step_long = 4,
            resolution = self.resolution,
            origin = -2,
            height = 3,
            hip = self.hip,
            dt = self.dt
        )
        self.gait_turn_left = quadruped_robot_gait.TrotSmoothRotateLeftGait(
            ground_level=-37,
            step_long = 3.5,
            resolution=self.resolution,
            origin = -2,
            height = 3,
            hip = self.hip,
            dt = self.dt
        )

        self.gait_turn_right = quadruped_robot_gait.TrotSmoothRotateRightGait(
            ground_level = -37,
            step_long = 3,
            resolution = self.resolution,
            origin = -2,
            height = 3,
            hip = self.hip,
            dt = self.dt
        )

    def startRemoteControlProcess(self):
        self.run_flag = True
        self.xbox_one.startThread()
        self.get_command_thread = self.GetCommandThread(self, self.xbox_one)
        self.get_command_thread.start()
        all_command ={
            "forward":self.gait_forward,
            "backward":self.gait_backward,
            "left":self.gait_turn_left,
            "right":self.gait_turn_right,
        }
        while self.run_flag:
            try:
                if self.xbox_command == "stand":
                    if not self.bool_robot_stand:
                        self.armAllMotor()
                        self.standupFromGround()
                        self.slowlySetAllMotorPositionIK(-3, -37, resolution=50, dt=0.015)
                        self.bool_robot_stand = True
                elif self.xbox_command == "sit":
                    if self.bool_robot_stand:
                        self.sitdownFromStand()
                        self.disarmAllMotor()
                        self.bool_robot_stand = False
                else:
                    if self.bool_robot_stand:
                        gait = all_command[self.xbox_command]
                        self.executeGaitContinuous(gait)
                        # print("wait for command")
                time.sleep(0.1)
            except KeyError:
                time.sleep(0.1)
            except KeyboardInterrupt:
                self.run_flag = False


    def startUDPControlProcess(self):
        self.run_flag = True
        self.xbox_one.startThread()
        self.get_command_thread = self.GetUDPCommandThread(self)
        self.get_command_thread.start()
        all_command ={
            "forward":self.gait_forward,
            "backward":self.gait_backward,
            "left":self.gait_turn_left,
            "right":self.gait_turn_right,
        }
        while self.run_flag:
            try:
                if self.udp_command == "stand":
                    if not self.bool_robot_stand:
                        # self.armAllMotor()
                        # self.standupFromGround()
                        # self.slowlySetAllMotorPositionIK(-3, -37, resolution=50, dt=0.015)
                        self.bool_robot_stand = True
                elif self.udp_command == "sit":
                    if self.bool_robot_stand:
                        # self.sitdownFromStand()
                        # self.disarmAllMotor()
                        self.bool_robot_stand = False
                else:
                    if self.bool_robot_stand:
                        gait = all_command[self.xbox_command]
                        self.executeGaitContinuous(gait)
                        # print("wait for command")
                time.sleep(0.1)
            except KeyError:
                time.sleep(0.1)
            except KeyboardInterrupt:
                self.run_flag = False

    def executeGaitContinuous(self, gait):
        gait_count = 0
        self.bool_keep_execute_gait = True
        if gait.bool_has_init_gait:
            while gait_count != gait.number_of_init_point:
                lf_cmd, rf_cmd, lh_cmd, rh_cmd, lf_pos, rf_pos, lh_pos, rh_pos, cycle_passed = \
                    gait.initGaitLookUp(gait_count)
                self.executeIK(
                    lf_cmd, rf_cmd, lh_cmd, rh_cmd, lf_pos, rf_pos, lh_pos, rh_pos, gait.dt
                )
                gait_count+=1

        keep_running = True
        gait_count = 0
        while keep_running:
            lf_cmd, rf_cmd, lh_cmd, rh_cmd, lf_pos, rf_pos, lh_pos, rh_pos, cycle_passed = \
                gait.gaitLookUp(gait_count)
            self.executeIK(
                lf_cmd, rf_cmd, lh_cmd, rh_cmd, lf_pos, rf_pos, lh_pos, rh_pos, gait.dt
            )
            if cycle_passed:
                if not self.bool_keep_execute_gait:
                    keep_running = False
            gait_count+=1

        gait_count = 0
        if gait.bool_has_end_gait:
            while gait_count != gait.number_of_end_point:
                lf_cmd, rf_cmd, lh_cmd, rh_cmd, lf_pos, rf_pos, lh_pos, rh_pos, cycle_passed = \
                    gait.endGaitLookUp(gait_count)
                self.executeIK(
                    lf_cmd, rf_cmd, lh_cmd, rh_cmd, lf_pos, rf_pos, lh_pos, rh_pos, gait.dt
                )
                gait_count+=1

    class GetCommandThread(threading.Thread):
        def __init__(self, controller, xbox_one):
            self.controller = controller
            self.xbox_one = xbox_one
            super().__init__(daemon=True)
        
        def run(self):
            while self.controller.run_flag:
                last_command = self.controller.xbox_command
                if self.xbox_one.abs_state["HX"] == 1:
                    self.controller.xbox_command = "right"
                    # print("turn right")
                else:
                    if last_command == "right":
                        self.controller.bool_keep_execute_gait = False
                        self.controller.xbox_command = ""
                        # print("stop")

                if self.xbox_one.abs_state["HX"] == -1:
                    self.controller.xbox_command = "left"
                    # print("turn left")
                else:
                    if last_command == "left":
                        self.controller.bool_keep_execute_gait = False
                        self.controller.xbox_command = ""
                        # print("stop")


                if self.xbox_one.abs_state["HY"] == -1:
                    self.controller.xbox_command = "forward"
                    # print("forward")
                else:
                    if last_command == "forward":
                        self.controller.bool_keep_execute_gait = False
                        self.controller.xbox_command = ""
                        # print("stop")

                if self.xbox_one.abs_state["HY"] == 1:
                    self.controller.xbox_command = "backward"
                    # print("backward")
                else:
                    if last_command == "backward":
                        self.controller.bool_keep_execute_gait = False
                        self.controller.xbox_command = ""
                        # print("stop")
                
                if self.xbox_one.btn_state["S"] == 1:
                    self.controller.xbox_command = "stand"
                    print("stand")
                else:
                    if last_command == "stand":
                        self.controller.xbox_command = ""

                if self.xbox_one.btn_state["E"] == 1:
                    self.controller.xbox_command = "sit"
                    print("sit")
                else:
                    if last_command == "stand":
                        self.controller.xbox_command = ""

                time.sleep(0.1)

    class GetUDPCommandThread(threading.Thread):
        def __init__(self, controller):
            self.controller = controller
            super().__init__(daemon=True)
        
        def run(self):
            while self.controller.run_flag:
                last_command = self.controller.udp_command
                self.controller.udp_command = self.controller.udp_server.recvObject()
                print(self.controller.udp_command)
                if last_command != self.controller.udp_command and self.controller.udp_command is None:
                    self.controller.bool_keep_execute_gait = False
                    print("stop")
                time.sleep(0.1)


if __name__ == "__main__":
    # device = "com14"
    device = "/dev/ttyUSB0"
    robot = QuadrupedRobotRemoteController()
    robot.activateDXLConnection(device)
    robot.activateAllRealMotor()
    # robot.armAllMotor()
    # robot.standupFromGround()

    command_dict = {
        "standup":robot.standupFromGround,
        "sitdown":robot.sitdownFromStand,
        "trot":robot.trotStandingInPlace,
        "trot2":robot.trotOutwardStandingInPlace,

    }

    while True:
        try:
            cmd = input("CMD : ")
            if cmd in command_dict:
                command_dict[cmd]()
            elif cmd == "s":
                robot.startRemoteControlProcess()
            elif cmd == "c":
                robot.startUDPControlProcess()
            elif cmd == "exit":
                break
        except KeyboardInterrupt:
            pass
        except Exception as e:
            traceback.print_exc()
            break
    
    if robot.bool_robot_stand:
        robot.sitdownFromStand()
    robot.disarmAllMotor()
    robot.deactivateDXLConnection()