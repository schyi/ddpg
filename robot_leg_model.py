from math import cos,sin,atan2,sqrt

import torch
try:
    import DXL_motor_control as DXL
    HAS_DXL_SDK = True
except ModuleNotFoundError:
    HAS_DXL_SDK = False

# When used in webots, module below will be import
try:
    from controller import Robot
    BOOL_HAS_WEBOTS = True
except ModuleNotFoundError:
    BOOL_HAS_WEBOTS = False

import math
import numpy as np
import time
class MotorInfo():
    '''
    Input : angle with offset , velocity , acceleration , torque
    '''
    def __init__(self, angle, velocity, acceleration, torque):
        self.angle = angle
        self.velocity = velocity
        self.acceleration = acceleration
        self.torque = torque

class Motor(object):
    """Use Degrees as initial degree input!!"""

    def __init__(
            self, 
            motor_name, 
            simulated_initial_position_deg, 
            real_angle_offset_deg = 0,
            parent_moter = None, 
            log_len = 200
        ):
        self.name = motor_name
        self.real_angle_offset_rad = 0.
        self.setting_offset(real_angle_offset_deg)
        self.__angular_p_cmd = 0
        self.__angular_position = np.deg2rad(simulated_initial_position_deg) + self.real_angle_offset_rad
        self.__angular_velocity = 0
        self.__angular_acceleration = 0
        self.velocity = 0
        self.log_length = log_len
        self.p_cmd_log = np.zeros(log_len)
        self.position_log = np.ones(log_len) * self.__angular_position
        self.velocity_log = np.zeros(log_len)
        self.acc_log = np.zeros(log_len)
        self.torque_log = np.zeros(log_len)
        self.current_log = np.zeros(log_len)
        self.temperature_log = np.zeros(log_len)
        self.real_motor = None
        self.webots_motor = None
        self.bool_webtos_motor_inverse_flag = False

        self.base_moter = parent_moter
        self.global_angle = 0
        self.updateGlobalAngle()

    def activateRealMotor(self, motor):
        self.real_motor = motor
    
    def activateWebotsMotor(self, webots_motor, webots_timestep=15, bool_reverse_flag=False):
        '''
        webots_motor : Webots motor controller object\n
        webots_timestep : Data sampling period (ms) in webots\n\n
        Put Webots motor controller into Motor object, to share some variables and liberaries.\n
        '''
        self.webots_motor = webots_motor
        self.webots_position_sensor = self.webots_motor.getPositionSensor()
        self.webots_position_sensor.enable(webots_timestep)
        self.webots_motor.enableTorqueFeedback(webots_timestep)
        self.webots_motor.enableForceFeedback(webots_timestep)
        self.webots_timestep = webots_timestep
        self.bool_webtos_motor_inverse_flag = bool_reverse_flag

    def calculateVelAcc(self, mode, dt):
        self.dt = dt
        if mode == "position":
            self.__angular_velocity = (self.__angular_position - self.position_log[-1])/dt
            self.__angular_acceleration = (self.__angular_velocity - self.velocity_log[-1])/dt
            
        if mode == "velocity":
            self.__angular_acceleration = (self.__angular_velocity - self.velocity_log[-1])/dt

    def cutOffLog(self, num):
        self.position_log = self.position_log[-num:]
        self.velocity_log = self.velocity_log[-num:]
        self.acc_log = self.acc_log[-num:]
        self.torque_log = self.torque_log[-num:]

    def deactivateRealMotor(self):
        self.real_motor = None

    def deactivateWebotsMotor(self):
        self.webots_motor = None

    def getAcceleration(self, unit='rad'):
        if unit == 'rad':
            return self.__angular_acceleration
        elif unit == 'deg':
            return np.rad2deg(self.__angular_acceleration)
        else:
            return None

    def getCurrent(self):
        return self.__current_data if self.real_motor is not None else None
    
    def getPosition(self, unit='rad', return_global_angle=True):
        """Use "deg" or "rad" to select output formate"""
        if return_global_angle:
            if self.webots_motor is not None:
                ang = self.__angular_position + self.real_angle_offset_rad
            else:
                ang = self.__angular_position - self.real_angle_offset_rad
                ang = ang if ang > 0 else (ang + 2*np.pi)
            if unit == "deg":
                return np.rad2deg(ang)
            elif unit == "rad":
                return ang
        else:
            if unit == "deg":
                return np.rad2deg(self.__angular_position)
            elif unit == "rad":
                return self.__angular_position

    def getVelocity(self, unit='rad'):
        if unit == 'rad':
            return self.__angular_velocity
        elif unit == 'deg':
            return np.rad2deg(self.__angular_velocity)
        else:
            return None

    def getMotorInfo(self):
        info = MotorInfo(
            self.getPosition(return_global_angle=False),
            self.velocity_log[-1],
            self.acc_log[-1],
            self.torque_log[-1]
        )
        return info

    def getTemperture(self):
        return self.__temperture if self.real_motor is not None else None

    def moveMotorP(self, angle, dt=1, need_log=True, bool_input_global=True):
        """angle Unit as degree ; angle should be local p_cmd if bool_input_global is True"""
        # angle = np.deg2rad(angle) + self.real_angle_offset_rad if bool_input_global else np.deg2rad(angle)
        temp_angle = np.deg2rad(angle) + self.real_angle_offset_rad if bool_input_global else np.deg2rad(angle)
        if temp_angle >= 2 * math.pi:
            temp_angle = temp_angle - 2*math.pi
        elif temp_angle < 0:
            temp_angle = temp_angle + 2*math.pi
        self.__angular_p_cmd = temp_angle

        if self.real_motor is not None:
            self.real_motor.writePosition(int(temp_angle/math.pi*2048))

        elif self.webots_motor is not None:
            temp_angle = np.deg2rad(angle)
            if self.bool_webtos_motor_inverse_flag:
                temp_angle = -temp_angle
                temp_angle -= self.real_angle_offset_rad
            else:
                temp_angle += self.real_angle_offset_rad

            while temp_angle >= 2*np.pi:
                temp_angle -= 2*np.pi
            while temp_angle <= -2*np.pi:
                temp_angle += 2*np.pi

            if self.bool_webtos_motor_inverse_flag:
                temp_angle += np.pi
            else:
                temp_angle -= np.pi
            self.__angular_p_cmd = temp_angle
            self.webots_motor.setPosition(temp_angle)
        else:
            self.__angular_position = self.__angular_p_cmd
            self.calculateVelAcc("position", dt)
            if need_log == True:
                self.updateData()


    def moveMotorV(self, velocity, dt=1, need_log=True, profile_type='T'):
        """
        profile_type -- S stands for steps profile, T stands for trapezoid profile
        """
        if self.real_motor is not None:
            velocity = int(velocity/0.024)
            self.real_motor.writeVelocity(velocity)

        elif self.webots_motor is not None:
            if velocity >= 0:
                self.webots_motor.setPosition(float("inf"))
            else:
                self.webots_motor.setPosition(-float("inf"))
            self.webots_motor.setVelocity(abs(velocity))

        else:
            self.__angular_velocity = velocity
            
            if profile_type == 'T':
                self.__angular_position += (dt * (self.__angular_velocity + self.velocity_log[-1]))*0.5
            elif profile_type == 'S':
                self.__angular_position += (dt * self.__angular_velocity)
            else:
                self.__angular_position += (dt * self.__angular_velocity)
            
            if self.__angular_position >= 2*math.pi:
                self.__angular_position = self.__angular_position - 2*math.pi
            if self.__angular_position < 0:
                self.__angular_position = self.__angular_position + 2*math.pi
            self.calculateVelAcc("velocity", dt)
            if need_log:
                self.updateData()

    def updateTorque(self, torque):
        self.torque_log[:-1] = self.torque_log[1:]
        self.torque_log[-1] = torque
    
    def updatePosition(self, position=None):
        self.position_log[:-1] = self.position_log[1:]
        self.position_log[-1] = position if position is not None else self.__angular_position
    
    def updateVelocity(self, velocity=None):
        self.velocity_log[:-1] = self.velocity_log[1:]
        self.velocity_log[-1] = velocity if velocity is not None else self.__angular_velocity

    def setting_offset(self, offset):
        """in degrees"""
        self.real_angle_offset_rad = np.deg2rad(offset)
    
    def updateGlobalAngle(self, unit='deg'):
        if self.base_moter is not None:
            self.global_angle = self.getPosition(unit=unit) + self.base_moter.global_angle
        else:
            self.global_angle = self.getPosition(unit=unit)

    def setByMotorInfo(self, motor_info):
        for log in [self.position_log, self.velocity_log, self.acc_log, self.torque_log]:
            log[:-1] = log[1:]
        self.position_log[-1] = motor_info.angle
        self.velocity_log[-1] = motor_info.velocity
        self.acc_log[-1] = motor_info.acceleration
        self.torque_log[-1] = motor_info.torque
        self.__angular_position = motor_info.angle
        self.__angular_velocity = motor_info.velocity
        self.__angular_acceleration = motor_info.acceleration

    def setPID(self, p, i, d):
        self.real_motor.setPID(p, i, d)

    def resetLoglength(self, lengeth):
        self.log_length = lengeth
        self.position_log = np.zeros(lengeth)
        self.velocity_log = np.zeros(lengeth)
        self.acc_log = np.zeros(lengeth)
        self.torque_log  = np.zeros(lengeth)
        self.p_cmd_log = np.zeros(lengeth)
        self.current_log = np.zeros(lengeth)
        self.temperature_log = np.zeros(lengeth)

    def resetLog(self):
        self.position_log[0:-1] = np.ones(self.log_length-1)*self.position_log[-1]
        self.velocity_log[0:-1] = np.ones(self.log_length-1)*self.velocity_log[-1]
        self.acc_log[0:-1] = np.ones(self.log_length-1)*self.acc_log[-1]
        self.torque_log[0:-1]  = np.ones(self.log_length-1)*self.torque_log[-1]
        self.p_cmd_log[0:-1] = np.ones(self.log_length-1)*self.p_cmd_log[-1]
        self.current_log[0:-1] = np.ones(self.log_length-1)*self.current_log[-1]
        self.temperature_log[0:-1] = np.ones(self.log_length-1)*self.temperature_log[-1]


    def updateData(self, bool_update_log=True):
        if self.real_motor is not None:
            self.__angular_position = self.real_motor.PRESENT_POSITION_value*DXL.POSITION_RATIO
            if self.__angular_position >= 2*math.pi: self.__angular_position -= 2*math.pi
            self.__angular_velocity    = self.real_motor.PRESENT_VELOCITY_value*DXL.VELOCITY_RATIO
            self.__current_data     = self.real_motor.PRESENT_CURRENT_value*DXL.CURRENT_RATIO
            self.__temperture      = self.real_motor.PRESENT_TEMPERTURE_value
        elif self.webots_motor is not None:
            self.updateGlobalAngle()
            last_position = self.__angular_position
            last_velocity = self.__angular_velocity
            self.__angular_position = self.webots_position_sensor.getValue()
            self.__angular_velocity = (self.__angular_position - last_position) / (self.webots_timestep/1000)
            self.__angular_acceleration = (self.__angular_velocity - last_velocity) / (self.webots_timestep/1000)
        else:
            pass

        if bool_update_log:
            self.p_cmd_log[:-1] = self.p_cmd_log[1:]
            self.p_cmd_log[-1] = self.__angular_p_cmd
            self.position_log[:-1] = self.position_log[1:]
            self.position_log[-1] = self.__angular_position
            self.velocity_log[:-1] = self.velocity_log[1:]
            self.velocity_log[-1] = self.__angular_velocity
            self.velocity = self.velocity_log[np.where(self.velocity_log!=0.0)[0][-1]]
            self.acc_log[:-1] = self.acc_log[1:]
            self.acc_log[-1] = self.__angular_acceleration
            if self.real_motor is not None:
                self.current_log[:-1] = self.current_log[1:]
                self.current_log[-1] = self.__current_data
                self.temperature_log[:-1] = self.temperature_log[1:]
                self.temperature_log[-1] = self.__temperture
            if self.webots_motor is not None:
                self.torque_log[:-1] = self.torque_log[1:]
                self.torque_log[-1] = self.webots_motor.getTorqueFeedback()

    def acc_override(self, acc):
        self.resetLog()
        self.__angular_acceleration = acc

    def vel_override(self, vel):
        self.resetLog()
        self.__angular_velocity = vel

    def pos_override(self, pos):
        self.resetLog()
        self.__angular_position = pos*math.pi/180

class Leg(object):
    def __init__(self):
        
        self.BaseLocation = [0, 0]  #initial value(IV)
        self.EndPoint     = [0, 0]
        # self.working_space_warning = False
        
        self.upper_motor_pos = self.BaseLocation 
        self.lower_motor_pos  = [0, 0]

        self.AngleU_C = 270       #Unit: Deg
        self.AngleM_C = 270
        self.AngleL_C = 270

        self.log_len = 500
        self.upper_motor = Motor("upper_motor", 270 , real_angle_offset_deg=270, log_len=self.log_len)
        self.lower_motor = Motor("lower_motor", 0, real_angle_offset_deg= 180,  parent_moter=self.upper_motor, log_len=self.log_len)
        self.hip_motor = Motor("hip_motor", 270, real_angle_offset_deg=270, log_len=self.log_len)
        self.u_l_motors = [self.upper_motor, self.lower_motor]
        self.motors = [self.upper_motor, self.lower_motor, self.hip_motor]

        self.upper_link_length  = 20   #Unit: Cm
        self.lower_link_length = 20
        
        self.InterferenceFlag = False
        self.inverse_coordinate = False
        self.checkInterference()

        self.UpperWeight  = 0.4  #Unit: Kg
        self.MiddleWeight = 0.4
        self.LowerWeight  = 0.6

        self.torque = [0,0,0]
        self.torque_diff_log = np.zeros(200)
        self.fbd_output = np.zeros(9)
        # self.Gait_Shape = "Cycle"
        self.Gait_Shape = "Cycloid"
        self.bool_real_motor_mode = False
        self.bool_webots_motor_mode = False
        self.bool_motor_armed = False
        self.motor_operating_mode = 'p'
        self.profile_type = "S"

        self.webots_motor_name_list = ["", "", ""]
        self.webots_motor_reverse_flag_list = [False, False, False]

        self.endPointCalculation()

    def checkGaitValid(self, Gait):
        Check_Flag = True
        for index in range(Gait.number_of_gait_point):
            Check_Flag = Check_Flag and not(self.checkWorkingSpace(*Gait.GaitLookUp(index, checking=True)))
        return Check_Flag

    def checkInterference(self):
        clearacne = [190, 350]
        upper_pos = self.upper_motor.getPosition("deg")
        lower_pos = self.lower_motor.getPosition("deg")
        self.InterferenceFlag = False

    def checkWorkingSpace(self, *args):
        X_Cmd, Y_Cmd = args[0], args[1]
        working_space_warning = False
        if Y_Cmd >= 0:
            working_space_warning = True
        if sqrt(pow(X_Cmd,2) + pow(Y_Cmd,2)) > (self.upper_link_length + self.lower_link_length):
            working_space_warning = True

        return working_space_warning

    def cutOffLog(self, num):
        self.log_len = num
        for motor in self.motors: motor.cutOffLog(num)

    def dynamicCalculateManually(self, mode, dt):
        '''
        Execute DynamicCalculate in each motors.
        This method calcualte and update velocityLog, accelerationLog of each motors by differential.\n\n
        mode is either "position" or "velocity". Use "position" if you just updated position manually\n
        dt (second) is the time step for estimation.\n
        mode == "position" : update velocity and acceleration\n
        mode == "velocity" : only update acceleration
        '''
        if mode in ["position", "velocity"]:
            self.upper_motor.calculateVelAcc(mode, dt)
            self.lower_motor.calculateVelAcc(mode, dt)
        else:
            print("Please specify mode in 'position' or 'velocity' in robot_foot_model.dynamicCaluateManually")

    def endPointCalculation(self):
        ang = self.upper_motor.getPosition()
        L1X = self.upper_link_length * cos(ang) * (-1 if self.upper_motor.bool_webtos_motor_inverse_flag else 1)
        L1Y = self.upper_link_length * sin(ang)

        ang = ang + self.lower_motor.getPosition() + (np.pi if self.bool_webots_motor_mode else 0)
        L3X = self.lower_link_length * cos(ang) * (-1 if self.lower_motor.bool_webtos_motor_inverse_flag else 1)
        L3Y = self.lower_link_length * sin(ang)

        self.lower_motor_pos = [self.upper_motor_pos[0] + L1X, self.upper_motor_pos[1] + L1Y]
        self.EndPoint = [self.lower_motor_pos[0] + L3X, self.lower_motor_pos[1] + L3Y]
        # print(f"{self.EndPoint} upper : {self.upper_motor.getPosition('deg', False)} lower : {self.lower_motor.getPosition('deg', False)} upper_global : {self.upper_motor.getPosition('deg', True)} lower_global : {self.lower_motor.getPosition('deg', True)}")
        self.upper_motor.updateGlobalAngle()
        self.lower_motor.updateGlobalAngle()
        self.hip_motor.updateGlobalAngle()

    def excuByInfo(self, motor_infos, f_cmd=None, dynamic=True, update_torque=False):
        '''
        Move simulative motors with motor_infos. Used for monitor data from training.\n\n
        motor_infos         = get from robot_foot_model.getMotorInfo()\n
        update_torque = Only True if it is sured to recalcuate torque, and it will have buggy torque log*** This code leave this just in case.\n
        f_cmd         = loading force in simulation\n
        dynamic       = True if using dynamic model to estimate torque.
        '''
        self.upper_motor.setByMotorInfo(motor_infos[0])
        self.lower_motor.setByMotorInfo(motor_infos[1])
        self.endPointCalculation()
        mode = 'static' if not dynamic else 'dynamic'
        if update_torque:
            if f_cmd is not None:
                self.torqueUpdate(force_output=f_cmd, need_log=True, mode=mode)
            else:
                self.torqueUpdate(need_log=True, mode = mode)
        else:
            self.torque = [motor_infos[0].torque, motor_infos[1].torque]
        self.checkInterference()

    def getMotorInfo(self):
        infos = [
            self.upper_motor.getMotorInfo(),
            self.lower_motor.getMotorInfo()
        ]
        return infos

    def getTorque(self):
        torque = []
        [torque.append(i.webots_motor.getTorqueFeedback())for i in self.motors]
        return torque

    def getVelocity(self):
        velocity = []
        for motor in self.motors:
            velocity.append(motor.velocity_log[np.where(motor.velocity_log != 0)[-1]])
        return velocity

    def inverseKinematic(self, X_Cmd, Y_Cmd, timeStep = 1, need_log = True, dynamic=False, f_cmd=None, move_motor=True, send_command=True):
        Motor_Ang = [0, 0]
        if not self.checkWorkingSpace(X_Cmd, Y_Cmd):
            cos_2 = (X_Cmd**2 + Y_Cmd**2 - self.upper_link_length**2 - self.lower_link_length**2)/2/self.upper_link_length/self.lower_link_length
            beta = np.arctan2(Y_Cmd, X_Cmd)
            cos_phai = (X_Cmd**2 + Y_Cmd**2 + self.upper_link_length**2 - self.lower_link_length**2)/(2*self.upper_link_length*(X_Cmd**2 + Y_Cmd**2)**0.5)
            phai = np.arccos(cos_phai)
            Motor_Ang[0] = beta + (phai if self.inverse_coordinate else -phai)
            Motor_Ang[1] = np.arccos(cos_2) * (-1 if self.inverse_coordinate else 1)

            AngleU_C = np.rad2deg(Motor_Ang[0])
            AngleL_C = np.rad2deg(Motor_Ang[1])
            cmd_list = [AngleU_C, AngleL_C]

            if move_motor:
                if not self.bool_real_motor_mode:
                    self.setPositionCmd(cmd_list, timeStep, need_log = need_log, dynamic=dynamic, f_cmd=f_cmd)
                else:
                    if self.motor_operating_mode == 'p':
                        self.setPositionCmd(cmd_list, timeStep, need_log = need_log, dynamic=dynamic, send_command=send_command)
                    elif self.motor_operating_mode == 'v':
                        self.softVelocityToTarget(cmd_list)

    def positionUpdateManually(self, position=None):
        '''
        position = [upper_motor_pos, lower_motor_pos]\n
        Update positionLog in each motor by Input or current value(__angular_position)
        '''
        self.upper_motor.updatePosition(None if position is None else position[0])
        self.lower_motor.updatePosition(None if position is None else position[1])

    def resetLoglength(self, log_len):
        self.log_len = log_len
        for motor in self.motors:
            motor.resetLoglength(log_len)

    def resetLog(self):
        for motor in self.motors: motor.resetLog()

    def setHipPositionCmd(self, hip_cmd_degree, dt=0.015, need_log=True, bool_input_global=True,
            send_command=False, update_torque=False
        ):
        self.hip_motor.moveMotorP(hip_cmd_degree, dt, need_log, bool_input_global)
        if self.bool_real_motor_mode:
            if send_command:
                self.dxl_conmunicator.sentAllCmd()

    def setPositionCmd(self, p_cmd, dt=1, need_log=True, offset=True, 
            update_torque=True, f_cmd=None, dynamic=False, send_command=True
        ):
        '''
        Move simulative motors or real motors with position command.\n\n
        p_cmd         = [ upper_motor_DEGREE , lower_motor_DEGREE]\n
        dt            = time for execution. Use for calculation velcity and acceleration.\n
        need_log       = True if need to update Simulate data in positionLog, velocityLog, accLog in each motor\n
        offset        = False if p_cmd already have offset ***\n
        update_torque = False if getting torque value from other source. Recommand False when
                        bool_real_motor_mode is enable\n
        f_cmd         = loading force in simulation\n
        dynamic       = True if using dynamic model to estimate torque.
        '''
        # print(p_cmd)
        self.upper_motor.moveMotorP(p_cmd[0], dt, need_log, offset)
        self.lower_motor.moveMotorP(p_cmd[1], dt, need_log, offset)
        self.endPointCalculation()
        if update_torque:
            mode = 'static' if not dynamic else 'dynamic'
            if f_cmd is not None:
                self.torqueUpdate(force_output=f_cmd, need_log=need_log, mode=mode)
            else:
                self.torqueUpdate(need_log=need_log, mode=mode)
        self.checkInterference()
        if self.bool_real_motor_mode:
            if send_command:
                self.dxl_conmunicator.sentAllCmd()

    def setVelocityCmd(self, v_cmd, dt, need_log=True, update_torque=True,
            f_cmd=None, dynamic=True, send_command=True
        ):
        """
        Move simulative motors or real motors with velocity command.\n\n

        v_cmd = [upper_motor_speed, lower_motor_speed] *** Use rad/s as input unit\n
        dt            = time for execution. Use for calculation velcity and acceleration.\n
        need_log       = True if need to update Simulate data in positionLog, velocityLog, accLog in each motor\n
        update_torque = False if getting torque value from other source. Recommand False when
                        bool_real_motor_mode is enable\n
        f_cmd         = loading force in simulation\n
        dynamic       = True if using dynamic model to estimate torque.
        """
        self.upper_motor.moveMotorV(v_cmd[0], dt, profile_type=self.profile_type)
        self.lower_motor.moveMotorV(v_cmd[2], dt, profile_type=self.profile_type)
        mode = 'static' if not dynamic else 'dynamic'
        if update_torque:
            if f_cmd is not None:
                self.torqueUpdate(force_output = f_cmd, need_log=need_log, mode=mode)
            else:
                self.torqueUpdate(need_log=need_log, mode=mode)
        if not self.bool_real_motor_mode:
            self.endPointCalculation()
            self.checkInterference()
        else:
            if send_command:
                self.dxl_conmunicator.sentAllCmd()


    def setVelocityProfile(self, profile):
        '''
        profile = "S" or "T"
        Change simulative velocity calculation. See exec_velocity_command and moveMotorV
        '''
        if profile in ["S", "T"]:
            self.profile_type = profile
        else:
            print("profile should be either 'S' or 'T'")

    def torqueUpdate(self, force_output=[0, 0], need_log=False, mode='static'):
        """
        force_output
        +y
        ^
        |
        |
        end_point ____>+x
        """
        G = -9.8

        if self.bool_real_motor_mode:
            self.torque = [
                self.upper_motor.getCurrent(),
                self.lower_motor.getCurrent(),
            ]
        else:
            self.torque = [0, 0]

        if need_log:
            self.upper_motor.updateTorque(self.torque[0])
            self.lower_motor.updateTorque(self.torque[1])

    def torqueUpdateManually(self, torque):
        '''
        torque = [upper_motor_torque, lower_motor_torque]\n
        Update the torque in Leg and update the torqueLog in each motors.
        '''
        self.torque = torque
        self.upper_motor.updateTorque(torque[0])
        self.lower_motor.updateTorque(torque[1])

    def updateMotorData(self, bool_update_log=True):
        for motor in self.motors:
            motor.updateData(bool_update_log=True)
        # print(f"{self.__class__.__name__} : {self.hip_motor.getPosition('deg')} cmd: {np.rad2deg(self.hip_motor.p_cmd_log[-1])}")
        # self.endPointCalculation()
        # self.checkInterference()

    def velocityUpdateManually(self, velocity=None):
        '''
        velocity = [upper_motor_V, lower_motor_V]\n
        Leave velocity as None will use __angular_velocity to update log\n
        Update the velocity in Leg and update the velocityLog in each motors.\n
        '''
        self.upper_motor.updatePosition(None if velocity is None else velocity[0])
        self.lower_motor.updatePosition(None if velocity is None else velocity[1])

    ########  Real Motor Related Methods ###########
    def activateRealMotor(self, device_name = '/dev/ttyUSB0', indirect_mode=True, dxl_communicator=None, need_reboot=True):
        '''
            Modified by Clement : I left device_name for fear of someone is not using a global communicator.
        '''
        motor_id = [4, 5, 12] if not hasattr(self, "motor_id") else getattr(self, "motor_id")
        self.dxl_conmunicator = DXL.DXL_Conmunication(device_name) if dxl_communicator is None else dxl_communicator
        if self.dxl_conmunicator.portHandler_Check_Pass:
            self.upper_motor.activateRealMotor(
                self.dxl_conmunicator.createMotor('upper_motor', motor_id[0])
            )
            self.lower_motor.activateRealMotor(
                self.dxl_conmunicator.createMotor('lower_motor', motor_id[1])
            )
            self.hip_motor.activateRealMotor(
                self.dxl_conmunicator.createMotor('hip_motor', motor_id[2])
            )
            if None not in [motor.real_motor for motor in self.motors]:
                if need_reboot:
                    self.dxl_conmunicator.rebootAllMotor()
                    if indirect_mode: self.dxl_conmunicator.activateIndirectMode()
                    self.updateRealMotorsInfo()
                self.bool_real_motor_mode = True
            else:
                self.bool_real_motor_mode = False
        else:
            self.bool_real_motor_mode = False

    def armAllMotor(self):
        for motor in self.motors:
            motor.real_motor.enableMotor()
        self.bool_motor_armed = True

    def deactivateRealMotor(self):
        '''
        Close serial communication with DXL motors
        '''
        for motor in self.motors:
            motor.deactivateRealMotor()
        self.bool_real_motor_mode = False

    def disarmAllMotor(self):
        '''
        Disarm all motors (stop providing current to motors)
        '''
        for motor in self.motors:
            motor.real_motor.disableMotor()
        self.bool_motor_armed = False

    def reinitial(self):
        self.softVelocityToTarget([270, 270])
        self.endPointCalculation()
        self.checkInterference()
    
    def switchMotorMode(self, mode='position', rearm=False):
        if self.bool_motor_armed:
            self.disarmAllMotor()
        switched = True
        for motor in self.motors:
            switched = switched and motor.real_motor.switchMode(mode)
        if switched:
            if mode == 'position':
                self.motor_operating_mode = 'p'
            elif mode == 'velocity':
                self.motor_operating_mode = 'v'
        if rearm:
            self.armAllMotor()

    def setMotorsAccProfile(self, profile: int) -> None:
        for motor in self.motors:
            motor.real_motor.setAccelerationProfile(profile)

    def setLegPID(self, p, i, d) -> None:
        if self.bool_real_motor_mode:
            self.upper_motor.setPID(p, i, d)
            self.lower_motor.setPID(p, i, d)

    def setHipPID(self, p, i, d) -> None:
        if self.bool_real_motor_mode:
            self.hip_motor.setPID(p, i, d)

    def softVelocityToTarget(self, cmd_list):
        self.disarmAllMotor()
        if self.motor_operating_mode == 'p':
            self.switchMotorMode("velocity")
        velocity_cmd = 30
        for motor, tar_ang in zip(self.u_l_motors, cmd_list):
            motor.real_motor.enableMotor()
            tar_ang = np.deg2rad(tar_ang)
            tar_ang = tar_ang + motor.angle_offset
            tar_ang = tar_ang + 2*math.pi if tar_ang < 0 else tar_ang
            tar_ang = tar_ang - 2*math.pi if tar_ang >= 2*math.pi else tar_ang
            tar_ang = abs(tar_ang)
            direction = 1 if tar_ang - motor.getPosition(return_global_angle=False) >= 0 else -1
            sign_str  = "+" if direction == 1 else "-"

            # if direction > 0:
            motor.real_motor.setVelocity(int(velocity_cmd * direction))
            value_error = 0
            self.updateRealMotorsInfo(delay=10)
            old_pos_val = pos_val = motor.getPosition(return_global_angle=False)
            while True:
                end = "\r" if self.dxl_conmunicator.checkErrorCount() == 0 else "\n"
                print("ID {ID}: {c_pos:.3f} ={sign}=> {target:.3f} Err: {err}".format(
                    ID = motor.real_motor.DXL_ID,
                    c_pos = motor.getPosition(return_global_angle=False), 
                    sign = sign_str,
                    target = tar_ang,
                    err = self.dxl_conmunicator.checkErrorCount()
                    ), end = end
                )
                self.updateRealMotorsInfo(delay=10)
                pos_val = motor.getPosition(return_global_angle=False)
                if pos_val == old_pos_val: value_error += 1
                if self.dxl_conmunicator.checkErrorCount()>= 40:# or value_error >= 20: #Dealing Reading Error
                    print("\nError Reboot")
                    motor.real_motor.rebootMotor()
                    time.sleep(3)
                    motor.real_motor.setAccelerationProfile(motor.real_motor.acc_profile)
                    motor.real_motor.switchMode('velocity')
                    motor.real_motor.enableMotor()
                    self.updateRealMotorsInfo(delay=10)
                    direction = 1 if tar_ang - motor.getPosition(return_global_angle=False) >= 0 else -1
                    sign_str  = "+" if direction == 1 else "-"
                    motor.real_motor.setVelocity(int(velocity_cmd * direction))
                    pos_val = motor.getPosition(return_global_angle=False)
                    value_error = 0
                old_pos_val = pos_val
                if (direction>0 and pos_val>tar_ang) or (direction<0 and pos_val<tar_ang): break
            print("ID {0}: {1:.3f} ={2}=> {3:.3f}".format(motor.real_motor.DXL_ID, motor.getPosition(return_global_angle=False), sign_str, tar_ang))
            motor.real_motor.setVelocity(0)
        self.updateRealMotorsInfo()

    def updateRealMotorsInfo(self, delay=-1, update_now=True):
        '''
        delay = Extra delay for communication, -1 means no delay.
        Update real motor info and check for interference and calculate end point location
        
            Modified by Clement, add update_noew option. if false means we do update data manually.
        '''
        if update_now:
            self.dxl_conmunicator.updateMotorData(delay=delay)
        self.updateMotorData(bool_update_log=True)

############# Webots motor ######################
    def activateWebotsMotor(self, webots_controller_robot_module, timestep=15, webots_motor_name_list=None) -> bool:
        '''
            webots_motor_name_list = [str(upper motor name), str(lower motor name), str(hip motor name)]
        '''
        if BOOL_HAS_WEBOTS:
            if webots_motor_name_list is not None:
                self.webots_motor_name_list = webots_motor_name_list

            if len(self.webots_motor_name_list) != 3:
                print("webots_motor_name_list mismatch (should be 3)")
            else:
                for name in self.webots_motor_name_list:
                    if not isinstance(name, str):
                        print("Error : webots_motor_name_list contain non string")
                        return None
            for i, motor in enumerate(self.motors):
                webots_motor = webots_controller_robot_module.getDevice(self.webots_motor_name_list[i])
                if webots_motor is None:
                    print(f"Error : webots_motor_name_list[{i}] {self.webots_motor_name_list[i]} mismatch")
                    self.deactivateWebotsMotor()
                    return False
                else:
                    motor.activateWebotsMotor(webots_motor, timestep, self.webots_motor_reverse_flag_list[i])
            self.bool_webots_motor_mode = True
            return True
        else:
            print("No webots controller imported, plese used in webots simulation")
            return False

    def deactivateWebotsMotor(self):
        '''
            This is the method can be used to deactivate webots mode
        '''
        for motor in self.motors:
            motor.deactivateWebotsMotor()
        self.bool_webots_motor_mode = False

class Vector2D(object):
    def __init__(self,x,y,l = None):
        self.x = x
        self.y = y
        self.l = l if l is not None else math.sqrt(x**2+y**2)

        self.unit_x = self.x/self.l if self.l != 0 else self.x
        self.unit_y = self.y/self.l if self.l != 0 else self.y

    def __mul__(self, value):
        x = self.x*value
        y = self.y*value
        l = math.sqrt(x**2+y**2)
        return Vector2D(x,y,l)

    def __add__(self, value):
        if type(value) == Vector2D:
            x = self.x + value.x
            y = self.y + value.y
            l = math.sqrt(x**2+y**2)
            return Vector2D(x,y,l)
        elif type(value) == np.ndarray:
            if value.shape == (2,):
                x = self.x + value[0]
                y = self.y + value[1]
                l = math.sqrt(x**2+y**2)
                return Vector2D(x,y,l)
            else:
                print("np array dimention missmatch")
                raise TypeError
        else:
            raise TypeError
    


class Leg10_10_15_Real(Leg):

    def __init__(self):
        super(Leg10_10_15_Real, self).__init__()
    

class Leg14_14_7_Real(Leg):
    
    def __init__(self):
        super(Leg14_14_7_Real, self).__init__()
        self.upper_link_length = 20
        self.lower_link_length = 20

class LegLF(Leg):
    def __init__(self):
        super(LegLF, self).__init__()
        self.motor_id = [4, 5, 12]
        self.webots_motor_name_list = ["LF_leg_joint", "LF_knee_joint", "LF_hip_joint"]
        self.webots_motor_reverse_flag_list = [True, True, False]
        self.inverse_coordinate = False

class LegRF(Leg):
    def __init__(self):
        super(LegRF, self).__init__()
        self.motor_id = [6, 7, 13]
        self.webots_motor_name_list = ["RF_leg_joint", "RF_knee_joint", "RF_hip_joint"]
        self.webots_motor_reverse_flag_list = [False, False, True]

class LegLH(Leg):
    def __init__(self):
        super(LegLH, self).__init__()
        self.motor_id = [8, 9, 14]
        self.webots_motor_name_list = ["LH_leg_joint", "LH_knee_joint", "LH_hip_joint"]
        self.webots_motor_reverse_flag_list = [True, True, True]
        self.inverse_coordinate = False

class LegRH(Leg):
    def __init__(self):
        super(LegRH, self).__init__()
        self.motor_id = [10, 11, 15]
        self.webots_motor_name_list = ["RH_leg_joint", "RH_knee_joint", "RH_hip_joint"]
        self.webots_motor_reverse_flag_list = [False, False, False]




def testGround():
    pass

def realLegSpeedTest():

    import time
    foot = Leg14_14_7_Real()
    foot.activateRealMotor(indirect_mode=False)
    foot.switchMotorMode('position')

    cmd_number = 1000
    t0 = time.time()
    for _ in range(cmd_number):
        foot.setPositionCmd([1.5,1.5,1.5],dt=0.01,need_log=False)
        foot.updateRealMotorsInfo()
    t1 = time.time()
    print("average freq is {0}Hz".format(1000/(t1-t0)))


if __name__ == "__main__":
    realLegSpeedTest()
    # testGround()





